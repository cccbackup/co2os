# fork2

虽然fork复制了文件描述符表，但每个底层文件的偏移量是父子共享的。考虑这个例子。

```
user@DESKTOP-96FRN6B MSYS /d/ccc109/sp/11-os/xv6-riscv-win/lab/01/04-fork2
$ gcc fork2.c -o fork2

user@DESKTOP-96FRN6B MSYS /d/ccc109/sp/11-os/xv6-riscv-win/lab/01/04-fork2
$ ./fork2
hello world

```