#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
  int p[2];
  char *argv[2];
  argv[0] = "wc";
  argv[1] = 0;
  pipe(p);
  if(fork() == 0) {
    close(0);
    dup(p[0]); // 複製 p[0] 到 fd:0
    close(p[0]);
    close(p[1]);
    exec("/bin/wc", argv); // child: 這樣 wc 會由 stdin (p[0], fd:0) 讀入，於是讀到 hello world
  } else {
    close(p[0]);
    write(p[1], "hello world\n", 12); // parent: 寫入 hello world 到 p[1]
    close(p[1]);
  }
}
