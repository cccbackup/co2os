#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
  int fd = dup(1);
  write(1, "hello ", 6);
  write(fd, "world\n", 6);
}
