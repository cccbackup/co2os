# cat

## cat1.c

下面的程序片段（它构成了程序cat的本质）将数据从标准输入复制到标准输出。如果发生错误，它就会向标准错误写入一条信息。

```
user@DESKTOP-96FRN6B MSYS /d/ccc109/sp/11-os/xv6-riscv-win/lab/01/03-cat
$ gcc cat1.c -o cat1

user@DESKTOP-96FRN6B MSYS /d/ccc109/sp/11-os/xv6-riscv-win/lab/01/03-cat
$ ./cat1 < cat1.c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main() {
  char buf[512];
  int n;
  for(;;){
    n = read(0, buf, sizeof buf);
    if(n == 0)
      break;
    if(n < 0){
      printf("read error\n");
      exit(1);
    }
    if(write(1, buf, n) != n) {
      printf("write error\n");
      exit(1);
    }
  }
}

```

## cat2.c

文件描述符和fork相互作用，使I/O重定向易于实现。Fork将父进程的文件描述符表和它的内存一起复制，这样子进程开始时打开的文件和父进程完全一样。系统调用exec替换了调用进程的内存，但保留了它的文件表。这种行为允许shell通过分叉实现I/O重定向，在子进程中重新打开选定的文件描述符，然后调用exec运行新的程序。下面是shell运行`cat < input.txt`命令的简化版代码。

```
user@DESKTOP-96FRN6B MSYS /d/ccc109/sp/11-os/xv6-riscv-win/lab/01/03-cat
$ ./cat2

user@DESKTOP-96FRN6B MSYS /d/ccc109/sp/11-os/xv6-riscv-win/lab/01/03-cat
$ This is input.txt
```