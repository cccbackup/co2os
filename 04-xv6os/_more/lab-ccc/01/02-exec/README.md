# exec

## exec1.c

exec系统调用用从文件系统中存储的文件加载的新内存映像替换调用进程的内存。该文件必须有特定的格式，它规定了文件中哪部分存放指令，哪部分是数据，在哪条指令启动等，xv6使用ELF格式，第3章将详细讨论。当exec成功时，它并不返回到调用程序；相反，从文件中加载的指令在ELF头声明的入口点开始执行。exec需要两个参数：包含可执行文件的文件名和一个字符串参数数组。例如

```
user@DESKTOP-96FRN6B MSYS /d/ccc109/sp/11-os/xv6-riscv-win/lab/01/02-exec
$ gcc exec1.c -o exec1

user@DESKTOP-96FRN6B MSYS /d/ccc109/sp/11-os/xv6-riscv-win/lab/01/02-exec
$ ./exec1
hello
```

