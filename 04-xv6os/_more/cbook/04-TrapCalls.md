# Chapter 4 -- Traps and system calls

There are three kinds of event which cause the CPU to set aside ordinary execution of instructions and force a transfer of control to special code that handles the event. One situation is a system call, when a user program executes the ecall instruction to ask the kernel to do something for it. Another situation is an exception: an instruction (user or kernel) does something illegal, such as divide by zero or use an invalid virtual address. The third situation is a device interrupt, when a device signals that it needs attention, for example when the disk hardware finishes a read or write request.

有三种事件会导致CPU搁置普通指令的执行，强制将控制权转移给处理该事件的特殊代码。一种情况是系统调用，当用户程序执行ecall指令要求内核为其做某事时。另一种情况是异常：一条指令（用户或内核）做了一些非法的事情，如除以零或使用无效的虚拟地址。第三种情况是设备中断，当一个设备发出需要注意的信号时，例如当磁盘硬件完成一个读写请求时。

This book uses trap as a generic term for these situations. Typically whatever code was executing at the time of the trap will later need to resume, and shouldn’t need to be aware that anything special happened. That is, we often want traps to be transparent; this is particularly important for interrupts, which the interrupted code typically doesn’t expect. The usual sequence is that a trap forces a transfer of control into the kernel; the kernel saves registers and other state so that execution can be resumed; the kernel executes appropriate handler code (e.g., a system call implementation or device driver); the kernel restores the saved state and returns from the trap; and the original code resumes where it left off.

本书使用陷阱作为这些情况的通用术语。通常不管在陷阱发生时正在执行的代码以后都需要恢复，不应该需要意识到发生了什么特别的事情。也就是说，我们通常希望陷阱是透明的；这一点对于中断来说尤其重要，被中断的代码通常不会想到会发生这种情况。通常的顺序是：陷阱迫使控制权转移到内核；内核保存寄存器和其他状态，以便恢复执行；内核执行适当的处理程序代码（例如，系统调用实现或设备驱动程序）；内核恢复保存的状态，并从陷阱中返回；原始代码从原来的地方恢复。

The xv6 kernel handles all traps. This is natural for system calls. It makes sense for interrupts since isolation demands that user processes not directly use devices, and because only the kernel has the state needed for device handling. It also makes sense for exceptions since xv6 responds to all exceptions from user space by killing the offending program.

xv6内核会处理所有的陷阱。这对于系统调用来说是很自然的。这对中断是有意义的，因为隔离要求用户进程不能直接使用设备，而且只有内核才有设备处理所需的状态。这对异常也是有意义的，因为xv6对所有来自用户空间的异常的响应是杀死违规程序。

Xv6 trap handling proceeds in four stages: hardware actions taken by the RISC-V CPU, an assembly “vector” that prepares the way for kernel C code, a C trap handler that decides what to do with the trap, and the system call or device-driver service routine. While commonality among the three trap types suggests that a kernel could handle all traps with a single code path, it turns out to be convenient to have separate assembly vectors and C trap handlers for three distinct cases: traps from user space, traps from kernel space, and timer interrupts.

Xv6的陷阱处理分为四个阶段：RISC-V CPU的硬件操作，为内核C代码准备的汇编 "向量"，决定如何处理陷阱的C陷阱处理程序，以及系统调用或设备驱动服务例程。虽然这三种陷阱类型的共性表明，内核可以用一个单一的代码路径来处理所有的陷阱，但事实证明，为三种不同的情况，即来自用户空间的陷阱、来自内核空间的陷阱和定时器中断，分别设置不同的汇编向量和C陷阱处理程序是很方便的。

## 4.1 RISC-V trap machinery

Each RISC-V CPU has a set of control registers that the kernel writes to tell the CPU how to handle traps, and that the kernel can read to find out about a trap that has occured. The RISC-V documents contain the full story [1]. riscv.h (kernel/riscv.h:1) contains definitions that xv6 uses. Here’s an outline of the most important registers:

每个RISC-V CPU都有一组控制寄存器，内核写入这些寄存器来告诉CPU如何处理陷阱，内核可以通过读取这些寄存器来发现已经发生的陷阱。RISC-V文档包含了完整的故事[1]。riscv.h（kernel/riscv.h:1）包含了xv6使用的定义。这里是最重要的寄存器的概要。

* stvec: The kernel writes the address of its trap handler here; the RISC-V jumps here to handle a trap.
* sepc: When a trap occurs, RISC-V saves the program counter here (since the pc is then overwritten with stvec). The sret (return from trap) instruction copies sepc to the pc. The kernel can write to sepc to control where sret goes.
* scause: The RISC-V puts a number here that describes the reason for the trap.
* sscratch: The kernel places a value here that comes in handy at the very start of a trap handler.
* sstatus: The SIE bit in sstatus controls whether device interrupts are enabled. If the kernel clears SIE, the RISC-V will defer device interrupts until the kernel sets SIE. The SPP bit indicates whether a trap came from user mode or supervisor mode, and controls to what mode sret returns.

* stvec。内核在这里写下陷阱处理程序的地址；RISC -V在这里跳转来处理陷阱。
* sepc: 当一个陷阱发生时，RISC-V将程序计数器保存在这里（因为pc会被stvec覆盖）。sret (ret from trap)指令将sepc复制到pc中。内核可以写入sepc来控制sret的去向。
* 吓。RISC -V在这里写了一个数字来描述陷阱的原因.
* scause: RISC-V在这里放一个数字来描述陷阱的原因。内核在这里放了一个值，在陷阱处理程序开始的时候很方便。
* sstatus: sstatus中的SIE位用来描述陷阱的原因。sstatus: sstatus中的SIE位控制设备中断是否被激活. 如果内核清除SIE，RISC-V将推迟设备中断，直到内核设置SIE。SPP位表示一个陷阱是来自用户模式还是上级模式，并控制sret返回到什么模式

The above registers relate to traps handled in supervisor mode, and they cannot be read or written in user mode. There is an equivalent set of control registers for traps handled in machine mode; xv6 uses them only for the special case of timer interrupts.

Each CPU on a multi-core chip has its own set of these registers, and more than one CPU may be handling a trap at any given time.

上述寄存器与在主管模式下处理的陷阱有关，在用户模式下不能读或写。对于机器模式下处理的陷阱，有一组等效的控制寄存器，xv6只在定时器中断的特殊情况下使用它们。

多核芯片上的每个CPU都有自己的一组这些寄存器，而且任何时候都可能有多个CPU在处理一个陷阱。

When it needs to force a trap, the RISC-V hardware does the following for all trap types (other than timer interrupts):

1. If the trap is a device interrupt, and the sstatus SIE bit is clear, don’t do any of the following.
2. Disable interrupts by clearing SIE.
3. Copy the pc to sepc.
4. Save the current mode (user or supervisor) in the SPP bit in sstatus.
5. Set scause to reflect the trap’s cause.
6. Set the mode to supervisor.
7. Copy stvec to the pc.

Note that the CPU doesn’t switch to the kernel page table, doesn’t switch to a stack in the kernel, and doesn’t save any registers other than the pc. Kernel software must perform these tasks.

当需要强制陷阱时，RISC-V硬件对所有的陷阱类型（除定时器中断外）都会进行以下操作。

1. 如果陷阱是设备中断，且sstatus SIE位为清，不要做以下任何操作。
2. 通过清除SIE禁用中断。
3. 将pc复制到sepc。
4. 在sstatus的SPP位中保存当前模式（用户或主管）。
5. 设置scause来反映陷阱的原因。
6. 将模式设置为supervisor。
7. 将stvec复制到pc上。

注意，CPU不会切换到内核页表，不会切换到内核中的堆栈，也不会保存pc以外的任何寄存器。内核软件必须执行这些任务。

One reason that the CPU does minimal work during a trap is to provide flexibility to software; for example, some operating systems don’t require a page table switch in some situations, which can increase performance.

You might wonder whether the CPU hardware’s trap handling sequence could be further simplified. For example, suppose that the CPU didn’t switch program counters. Then a trap could switch to supervisor mode while still running user instructions. Those user instructions could break the user/kernel isolation, for example by modifying the satp register to point to a page table that allowed accessing all of physical memory. It is thus important that the CPU switch to a kernel specified instruction address, namely stvec.

CPU在陷阱期间做最少的工作的原因之一是为了给软件提供灵活性；例如，一些操作系统在某些情况下不需要切换页表，这可以提高性能。

你可能会想，CPU硬件的陷阱处理顺序是否可以进一步简化。例如，假设CPU没有切换程序计数器。那么陷阱可能会切换到主管模式，同时还在运行用户指令。这些用户指令可以打破用户/内核的隔离，例如通过修改satp寄存器指向一个允许访问所有物理内存的页表。因此，CPU必须切换到内核指定的指令地址，即stvec。

## 4.2 Traps from user space

A trap may occur while executing in user space if the user program makes a system call (ecall instruction), or does something illegal, or if a device interrupts. The high-level path of a trap from user space is uservec (kernel/trampoline.S:16), then usertrap (kernel/trap.c:37); and when returning, usertrapret (kernel/trap.c:90) and then userret (kernel/trampoline.S:16).

在用户空间执行时，如果用户程序进行了系统调用(ecall指令)，或者做了一些非法的事情，或者设备中断，都可能发生陷阱。从用户空间发出陷阱的高级路径是uservec(kernel/trampoline.S:16)，然后是usertrap(kernel/trap.c:37)；返回时是usertrapret(kernel/trap.c:90)，然后是userret(kernel/trampoline.S:16)。

Traps from user code are more challenging than from the kernel, since satp points to a user page table that doesn’t map the kernel, and the stack pointer may contain an invalid or even malicious value.

Because the RISC-V hardware doesn’t switch page tables during a trap, the user page table must include a mapping for uservec, the trap vector instructions that stvec points to. uservec must switch satp to point to the kernel page table; in order to continue executing instructions after the switch, uservec must be mapped at the same address in the kernel page table as in the user page table.

来自用户代码的陷阱比来自内核的陷阱更具挑战性，因为satp指向的用户页表并不映射内核，而且堆栈指针可能包含一个无效甚至恶意的值。

由于RISC-V硬件在陷阱过程中不会切换页表，所以用户页表必须包含uservec的映射，即stvec指向的陷阱向量指令，uservec必须切换satp指向内核页表；为了在切换后继续执行指令，uservec必须在内核页表中映射到与用户页表相同的地址。

Xv6 satisfies these constraints with a trampoline page that contains uservec. Xv6 maps the trampoline page at the same virtual address in the kernel page table and in every user page table.

This virtual address is TRAMPOLINE (as we saw in Figure 2.3 and in Figure 3.3). The trampoline contents are set in trampoline.S, and (when executing user code) stvec is set to uservec (kernel/trampoline.S:16).

When uservec starts, all 32 registers contain values owned by the interrupted code. But uservec needs to be able to modify some registers in order to set satp and generate addresses at which to save the registers. RISC-V provides a helping hand in the form of the sscratch register.

Xv6用一个包含uservec的trampoline页来满足这些约束。Xv6在内核页表和每个用户页表中的同一个虚拟地址上映射了trampoline页。

这个虚拟地址就是trampoline（如我们在图2.3和图3.3中看到的）。trampoline内容设置在trampoline.S中，（执行用户代码时）stvec设置为uservec（kernel/trampoline.S:16）。

当uservec启动时，所有32个寄存器都包含被中断的代码所拥有的值。但是uservec需要能够修改一些寄存器，以便设置satp和生成保存寄存器的地址。RISC-V以sscratch寄存器的形式提供了帮助。

The csrrw instruction at the start of uservec swaps the contents of a0 and sscratch. Now the user code’s a0 is saved; uservec has one register (a0) to play with; and a0 contains the value the kernel previously placed in sscratch.

uservec’s next task is to save the user registers. Before entering user space, the kernel previously set sscratch to point to a per-process trapframe that (among other things) has space to save all the user registers (kernel/proc.h:44). Because satp still refers to the user page table, uservec needs the trapframe to be mapped in the user address space. When creating each process, xv6 allocates a page for the process’s trapframe, and arranges for it always to be mapped at user virtual address TRAPFRAME, which is just below TRAMPOLINE. The process’s p->trapframe also points to the trapframe, though at its physical address so the kernel can use it through the kernel page table.

uservec开头的csrrw指令将a0和sscratch的内容互换。现在用户代码的a0被保存了；uservec有一个寄存器（a0）可以玩；a0包含了内核之前放在sscratch中的值。

uservec的下一个任务是保存用户寄存器。在进入用户空间之前，内核先前设置sscratch指向一个每进程的trapframe，这个trapframe（除其他外）有保存所有用户寄存器的空间（kernel/proc.h:44）。因为satp仍然是指用户页表，所以uservec需要将trapframe映射到用户地址空间中。在创建每个进程时，xv6为进程的trapframe分配一个页面，并安排它始终映射在用户虚拟地址TRAPFRAME，也就是TRAMPOLINE的下方。进程的p->trapframe也指向trapframe，不过是在它的物理地址，这样内核就可以通过内核页表来使用它。

Thus after swapping a0 and sscratch, a0 holds a pointer to the current process’s trapframe.

uservec now saves all user registers there, including the user’s a0, read from sscratch.

The trapframe contains pointers to the current process’s kernel stack, the current CPU’s hartid, the address of usertrap, and the address of the kernel page table. uservec retrieves these values, switches satp to the kernel page table, and calls usertrap.

因此在交换a0和sscratch后，a0持有指向当前进程的trapframe的指针。

uservec现在将所有用户寄存器保存在那里，包括从sscratch读取的用户a0。

trapframe包含指向当前进程的内核栈、当前CPU的hartid、usertrap的地址和内核页表的地址的指针，uservec检索这些值，将satp切换到内核页表，然后调用usertrap。

The job of usertrap is to determine the cause of the trap, process it, and return (kernel/trap.c:37). As mentioned above, it first changes stvec so that a trap while in the kernel will be handled by kernelvec. It saves the sepc (the saved user program counter), again because there might be a process switch in usertrap that could cause sepc to be overwritten. If the trap is a system call, syscall handles it; if a device interrupt, devintr; otherwise it’s an exception, and the kernel kills the faulting process. The system call path adds four to the saved user pc because RISC-V, in the case of a system call, leaves the program pointer pointing to the ecall instruction.

On the way out, usertrap checks if the process has been killed or should yield the CPU (if this trap is a timer interrupt).

usertrap的工作是确定陷阱的原因，处理它，然后返回(kernel/trap.c:37)。如上所述，它首先改变stvec，这样在内核中的陷阱将由kernelvec处理。它保存了sepc(保存的用户程序计数器)，这也是因为usertrap中可能会有一个进程切换，导致sepc被覆盖。如果陷阱是系统调用，则syscall处理；如果是设备中断，则devintr处理；否则就是异常，内核会杀死故障进程。系统调用路径在保存的用户pc上增加了4个，因为RISC-V在系统调用的情况下，会留下指向ecall指令的程序指针。

在退出时，usertrap检查进程是否已经被杀死或应该让出CPU（如果这个陷阱是一个定时器中断）。


The first step in returning to user space is the call to usertrapret (kernel/trap.c:90). This function sets up the RISC-V control registers to prepare for a future trap from user space. This involves changing stvec to refer to uservec, preparing the trapframe fields that uservec relies on, and setting sepc to the previously saved user program counter. At the end, usertrapret calls userret on the trampoline page that is mapped in both user and kernel page tables; the reason is that assembly code in userret will switch page tables.

回到用户空间的第一步是调用usertrapret(kernel/trap.c:90)。这个函数设置RISC-V控制寄存器，为将来从用户空间的陷阱做准备。这包括改变stvec来引用uservec，准备uservec所依赖的trapframe字段，并将sepc设置为先前保存的用户程序计数器。最后，usertrapret在用户页表和内核页表中映射的trampoline页上调用userret，原因是userret中的汇编代码会切换页表。

usertrapret’s call to userret passes a pointer to the process’s user page table in a0 and TRAPFRAME in a1 (kernel/trampoline.S:88). userret switches satp to the process’s user page table. Recall that the user page table maps both the trampoline page and TRAPFRAME, but nothing else from the kernel. Again, the fact that the trampoline page is mapped at the same virtual address in user and kernel page tables is what allows uservec to keep executing after changing satp.

usertrapret对userret的调用在a0中传递一个指向进程用户页表的指针，在a1中传递一个指向TRAPFRAME的指针(kernel/trampoline.S:88)，userret将satp切换到进程的用户页表。回顾一下，用户页表同时映射了trampoline页和TRAPFRAME，但没有映射内核的其他内容。同样，事实上，trampoline页在用户页表和内核页表中映射在同一个虚拟地址上，这也是允许uservec在改变satp后继续执行的原因。

userret copies the trapframe’s saved user a0 to sscratch in preparation for a later swap with TRAPFRAME. From this point on, the only data userret can use is the register contents and the content of the trapframe. Next userret restores saved user registers from the trapframe, does a final swap of a0 and sscratch to restore the user a0 and save TRAPFRAME for the next trap, and uses sret to return to user space.

userret将trapframe保存的用户a0复制到sscratch中，为以后与TRAPFRAME交换做准备。从这时开始，userret能使用的数据只有寄存器内容和trapframe的内容。接下来userret从trapframe中恢复保存的用户寄存器，对a0和sscratch做最后的交换，恢复用户a0并保存TRAPFRAME，为下一次的陷阱做准备，并使用sret返回用户空间。

## 4.3 Code: Calling system calls

Chapter 2 ended with initcode.S invoking the exec system call (user/initcode.S:11). Let’s look at how the user call makes its way to the exec system call’s implementation in the kernel.

The user code places the arguments for exec in registers a0 and a1, and puts the system call number in a7. System call numbers match the entries in the syscalls array, a table of function pointers (kernel/syscall.c:108). The ecall instruction traps into the kernel and executes uservec, usertrap, and then syscall, as we saw above.

第2章以initcode.S调用exec系统调用结束（user/initcode.S:11）。我们来看看用户调用是如何在内核中实现exec系统调用的。

用户代码将exec的参数放在寄存器a0和a1中，并将系统调用号放在a7中。系统调用号与函数指针表syscalls数组（kernel/syscall.c:108）中的条目相匹配。ecall指令陷阱进入内核，并执行uservec、usertrap，然后执行syscall，就像我们上面看到的那样。

syscall (kernel/syscall.c:133) retrieves the system call number from the saved a7 in the trapframe and uses it to index into syscalls. For the first system call, a7 contains SYS_exec (kernel/syscall.h:8), resulting in a call to the system call implementation function sys_exec.

When the system call implementation function returns, syscall records its return value in p->trapframe->a0. This will cause the original user-space call to exec() to return that value, since the C calling convention on RISC-V places return values in a0. System calls conventionally return negative numbers to indicate errors, and zero or positive numbers for success. If the system call number is invalid, syscall prints an error and returns −1.

syscall (kernel/syscall.c:133)从trapframe中保存的a7中检索系统调用号，并将其用于索引到syscalls中。对于第一次系统调用，a7包含SYS_exec（kernel/syscall.h:8），结果调用系统调用实现函数sys_exec。

当系统调用实现函数返回时，syscall将其返回值记录在p->trapframe->a0中。这将使原来的用户空间调用exec()返回该值，因为RISC-V上的C调用惯例将返回值放在a0中。系统调用惯例返回负数表示错误，0或正数表示成功。如果系统调用号无效，syscall会打印一个错误并返回-1。

## 4.4 Code: System call arguments

System call implementations in the kernel need to find the arguments passed by user code. Because user code calls system call wrapper functions, the arguments are initially where the RISC-V C calling convention places them: in registers. The kernel trap code saves user registers to the current process’s trap frame, where kernel code can find them. The functions argint, argaddr, and argfd retrieve the n ’th system call argument from the trap frame as an integer, pointer, or a file descriptor. They all call argraw to retrieve the appropriate saved user register (kernel/syscall.c:35).

内核中的系统调用实现需要找到用户代码传递的参数。因为用户代码调用系统调用封装函数，所以参数最初在RISC-V C调用惯例放置的地方：寄存器中。内核陷阱代码将用户寄存器保存到当前进程的陷阱帧中，内核代码可以在那里找到它们。函数argint、argaddr和argfd从陷阱帧中以整数、指针或文件描述符的形式检索第n个系统调用参数。它们都调用argraw来检索保存的用户寄存器(kernel/syscall.c:35)。

Some system calls pass pointers as arguments, and the kernel must use those pointers to read or write user memory. The exec system call, for example, passes the kernel an array of pointers referring to string arguments in user space. These pointers pose two challenges. First, the user program may be buggy or malicious, and may pass the kernel an invalid pointer or a pointer intended to trick the kernel into accessing kernel memory instead of user memory. Second, the xv6 kernel page table mappings are not the same as the user page table mappings, so the kernel cannot use ordinary instructions to load or store from user-supplied addresses.

一些系统调用传递指针作为参数，而内核必须使用这些指针来读取或写入用户内存。例如，exec系统调用会向内核传递一个指向用户空间中的字符串参数的指针数组。这些指针带来了两个挑战。首先，用户程序可能是错误的或恶意的，可能会传递给内核一个无效的指针或一个旨在欺骗内核访问内核内存而不是用户内存的指针。第二，xv6内核页表映射与用户页表映射不一样，所以内核不能使用普通指令从用户提供的地址加载或存储。

The kernel implements functions that safely transfer data to and from user-supplied addresses.

fetchstr is an example (kernel/syscall.c:25). File system calls such as exec use fetchstr to retrieve string file-name arguments from user space. fetchstr calls copyinstr to do the hard work.

copyinstr (kernel/vm.c:406) copies up to max bytes to dst from virtual address srcva in the user page table pagetable. It uses walkaddr (which calls walk) to walk the page table in software to determine the physical address pa0 for srcva. Since the kernel maps all physical
RAM addresses to the same kernel virtual address, copyinstr can directly copy string bytes from pa0 to dst. walkaddr (kernel/vm.c:95) checks that the user-supplied virtual address is part of the process’s user address space, so programs cannot trick the kernel into reading other memory.

A similar function, copyout, copies data from the kernel to a user-supplied address.

内核实现了一些函数，可以安全地将数据传输到用户提供的地址，或者从用户提供的地址传输数据。

fetchstr是一个例子(kernel/syscall.c:25)。文件系统调用，如exec，使用fetchstr从用户空间中获取字符串文件名参数，fetchstr调用copyinstr来完成这项艰难的工作。

copyinstr (kernel/vm.c:406) 将用户页表 pagetable 中的虚拟地址 srcva 复制到 dst，最多可复制最大字节。它使用 walkaddr (调用 walk) 在软件中走页表来确定 srcva 的物理地址 pa0。由于内核将所有的物理地址
walkaddr(kernel/vm.c:95)检查用户提供的虚拟地址是否是进程的用户地址空间的一部分，所以程序不能欺骗内核读取其他内存。

类似的函数copyout，可以将数据从内核复制到用户提供的地址。

## 4.5 Traps from kernel space

Xv6 configures the CPU trap registers somewhat differently depending on whether user or kernel code is executing. When the kernel is executing on a CPU, the kernel points stvec to the assembly code at kernelvec (kernel/kernelvec.S:10). Since xv6 is already in the kernel, kernelvec can rely on satp being set to the kernel page table, and on the stack pointer referring to a valid kernel stack. kernelvec saves all registers so that the interrupted code can eventually resume without disturbance.

Xv6根据用户代码还是内核代码在执行，对CPU陷阱寄存器的配置有些不同。当内核在CPU上执行时，内核将stvec指向kernelvec处的汇编代码（kernel/kernelvec.S:10）。由于xv6已经在内核中，kernelvec可以依靠satp被设置为内核页表，以及堆栈指针指向一个有效的内核堆栈，kernelvec保存了所有的寄存器，这样被中断的代码最终可以不受干扰地恢复。

kernelvec saves the registers on the stack of the interrupted kernel thread, which makes sense because the register values belong to that thread. This is particularly important if the trap causes a switch to a different thread – in that case the trap will actually return on the stack of the new thread, leaving the interrupted thread’s saved registers safely on its stack.

kernelvec将寄存器保存在被中断的内核线程的栈上，这很有意义，因为寄存器的值属于那个线程。如果陷阱导致切换到不同的线程，这一点尤其重要--在这种情况下，陷阱实际上会在新线程的堆栈上返回，将被中断线程保存的寄存器安全地留在其堆栈上。

kernelvec jumps to kerneltrap (kernel/trap.c:134) after saving registers. kerneltrap is prepared for two types of traps: device interrrupts and exceptions. It calls devintr (kernel/trap.c:177) to check for and handle the former. If the trap isn’t a device interrupt, it must be an exception, and that is always a fatal error if it occurs in the xv6 kernel; the kernel calls panic and stops executing.

If kerneltrap was called due to a timer interrupt, and a process’s kernel thread is running (rather than a scheduler thread), kerneltrap calls yield to give other threads a chance to run.

At some point one of those threads will yield, and let our thread and its kerneltrap resume again. Chapter 7 explains what happens in yield.

如果kerneltrap是由于定时器中断而被调用的，并且一个进程的内核线程正在运行（而不是调度器线程），kerneltrap调用屈服来给其他线程一个运行的机会。

在某些时候，这些线程中的一个线程会屈服，让我们的线程和它的kerneltrap再次恢复。第7章解释了yield中发生的事情。

When kerneltrap’s work is done, it needs to return to whatever code was interrupted by the trap. Because a yield may have disturbed the saved sepc and the saved previous mode in sstatus, kerneltrap saves them when it starts. It now restores those control registers and returns to kernelvec (kernel/kernelvec.S:48). kernelvec pops the saved registers from the stack and executes sret, which copies sepc to pc and resumes the interrupted kernel code.

当kerneltrap的工作完成后，它需要返回到被陷阱打断的任何代码。因为yield可能会干扰sstatus中保存的sepc和保存的前一个模式，所以kerneltrap在启动时保存了它们。kernelvec从堆栈中弹出保存的寄存器并执行sret，sret将sepc复制到pc中并恢复被中断的内核代码。


It’s worth thinking through how the trap return happens if kerneltrap called yield due to a timer interrupt.

Xv6 sets a CPU’s stvec to kernelvec when that CPU enters the kernel from user space; you can see this in usertrap (kernel/trap.c:29). There’s a window of time when the kernel is executing but stvec is set to uservec, and it’s crucial that device interrupts be disabled during that window. Luckily the RISC-V always disables interrupts when it starts to take a trap, and xv6 doesn’t enable them again until after it sets stvec.

值得思考的是，如果kerneltrap由于定时器中断而调用yield，陷阱返回是如何发生的。

当一个CPU从用户空间进入内核时，Xv6会将该CPU的stvec设置为kernelvec；你可以在usertrap（kernel/trap.c:29）中看到这一点。有一个时间窗口，当内核在执行时，stvec被设置为uservec，在这个窗口中禁用设备中断是非常重要的。幸运的是RISC-V在开始采取陷阱时总是禁用中断，xv6在设置stvec之后才会再次启用中断。

## 4.6 Page-fault exceptions

Xv6’s response to exceptions is quite boring: if an exception happens in user space, the kernel kills the faulting process. If an exception happens in the kernel, the kernel panics. Real operating systems often respond in much more interesting ways.

Xv6对异常的响应相当无聊：如果异常发生在用户空间，内核就会杀死故障进程。如果一个异常发生在内核中，内核就会恐慌。真正的操作系统通常会以更有趣的方式进行响应。

As an example, many kernels use page faults to implement copy-on-write (COW) fork. To explain copy-on-write fork, consider xv6’s fork, described in Chapter 3. fork causes the child to have the same memory content as the parent, by calling uvmcopy (kernel/vm.c:309) to allocate physical memory for the child and copy the parent’s memory into it. It would be more efficient if the child and parent could share the parent’s physical memory. A straightforward implementation of this would not work, however, since it would cause the parent and child to disrupt each other’s execution with their writes to the shared stack and heap.

举个例子，许多内核使用页面故障来实现写后复制（COW）fork。要解释copy-on-write fork，可以考虑xv6的fork，在第3章中介绍过。fork通过调用uvmcopy(kernel/vm.c:309)为子程序分配物理内存，并将父程序的内存复制到子程序中，使子程序拥有与父程序相同的内存内容。如果子代和父代能够共享父代的物理内存，效率会更高。然而，直接实现这个方法是行不通的，因为这会导致父代和子代对共享栈和堆的写入，从而扰乱对方的执行。

Parent and child can safely share phyical memory using copy-on-write fork, driven by page faults. When a CPU cannot translate a virtual address to a physical address, the CPU generates a page-fault exception. RISC-V has three different kinds of page fault: load page faults (when a load instruction cannot translate its virtual address), store page faults (when a store instruction cannot translate its virtual address), and instruction page faults (when the address for an instruction doesn’t translate). The value in the scause register indicates the type of the page fault and the stval register contains the address that couldn’t be translated.

父母和子女可以安全地共享物理内存，使用copy-on-write fork，由page faults驱动。当CPU不能将虚拟地址翻译成物理地址时，CPU会产生一个页故障异常。RISC-V有三种不同的页故障：加载页故障（当加载指令不能翻译其虚拟地址时）、存储页故障（当存储指令不能翻译其虚拟地址时）和指令页故障（当指令的地址不能翻译时）。scause寄存器中的值表示页面故障的类型，stval寄存器中包含无法翻译的地址。

The basic plan in COW fork is for the parent and child to initially share all physical pages, but to map them read-only. Thus, when the child or parent executes a store instruction, the RISC-V CPU raises a page-fault exception. In response to this exception, the kernel makes a copy of the page that contains the faulted address. It maps one copy read/write in the child’s address space and the other copy read/write in the parent’s address space. After updating the page tables, the kernel resumes the faulting process at the instruction that caused the fault. Because the kernel has updated the relevant PTE to allow writes, the faulting instruction will now execute without a fault.

COW fork中的基本计划是父级和子级最初共享所有的物理页面，但将它们映射为只读。因此，当子代或父代执行存储指令时，RISC-V CPU会引发一个页面故障异常。作为对这个异常的响应，内核会对包含故障地址的页面做一个拷贝。它将一个副本读/写映射在子地址空间，另一个副本读/写映射在父地址空间。更新页表后，内核在引起故障的指令处恢复故障处理。因为内核已经更新了相关的PTE，允许写入，所以现在故障指令将无故障执行。

This COW plan works well for fork, because often the child calls exec immediately after the fork, replacing its address space with a new address space. In that common case, the child will experience only a few page faults, and the kernel can avoid making a complete copy. Furthermore, COW fork is transparent: no modifications to applications are necessary for them to benefit.

这个COW计划对fork很有效，因为往往子程序在fork后立即调用exec，用新的地址空间替换其地址空间。在这种常见的情况下，子程序只会遇到一些页面故障，而内核可以避免进行完整的复制。此外，COW fork是透明的：不需要对应用程序进行修改，应用程序就能受益。

The combination of page tables and page faults opens up a wide-range of interesting possibilities other than COW fork. Another widely-used feature is called lazy allocation, which has two parts. First, when an application calls sbrk, the kernel grows the address space, but marks the new addresses as not valid in the page table. Second, on a page fault on one of those new addresses, the kernel allocates physical memory and maps it into the page table. Since applications often ask for more memory than they need, lazy allocation is a win: the kernel allocates memory only when the application actually uses it. Like COW fork, the kernel can implement this feature transparently to applications.

页表和页故障的结合，开启了除COW fork之外的多种有趣的可能性。另一个被广泛使用的特性叫做懒惰分配，它有两个部分。首先，当一个应用程序调用sbrk时，内核会增长地址空间，但在页表中把新的地址标记为无效。第二，当这些新地址中的一个出现页面故障时，内核分配物理内存并将其映射到页表中。由于应用程序经常要求获得比他们需要的更多的内存，所以懒惰分配是一个胜利：内核只在应用程序实际使用时才分配内存。像COW fork一样，内核可以对应用程序透明地实现这个功能。

Yet another widely-used feature that exploits page faults is paging from disk. If applications need more memory than the available physical RAM, the kernel can evict some pages: write them to a storage device such as a disk and mark their PTEs as not valid. If an application reads or writes an evicted page, the CPU will experience a page fault. The kernel can then inspect the faulting address. If the address belongs to a page that is on disk, the kernel allocates a page of physical memory, reads the page from disk to that memory, updates the PTE to be valid and refer to that memory, and resumes the application. To make room for the page, the kernel may have to evict another page. This feature requires no changes to applications, and works well if applications have locality of reference (i.e., they use only a subset of their memory at any given time).

然而，另一个被广泛使用的利用页面故障的功能是从磁盘上分页。如果应用程序需要的内存超过了可用的物理RAM，内核可以驱逐一些页面：将它们写入一个存储设备，比如磁盘，并将其PTE标记为无效。如果一个应用程序读取或写入一个被驱逐的页面，CPU将遇到一个页面故障。内核就可以检查故障地址。如果该地址属于磁盘上的页面，内核就会分配一个物理内存的页面，从磁盘上读取页面到该内存，更新PTE为有效并引用该内存，然后恢复应用程序。为了给该页腾出空间，内核可能要驱逐另一个页。这个特性不需要对应用程序进行任何修改，如果应用程序具有引用的位置性（即它们在任何时候都只使用其内存的一个子集），这个特性就能很好地发挥作用。

Other features that combine paging and page-fault exceptions include automatically extending stacks and memory-mapped files.

其他结合分页和分页错误异常的功能包括自动扩展堆栈和内存映射文件。

## 4.7 Real world

The need for special trampoline pages could be eliminated if kernel memory were mapped into every process’s user page table (with appropriate PTE permission flags). That would also eliminate the need for a page table switch when trapping from user space into the kernel. That in turn would allow system call implementations in the kernel to take advantage of the current process’s user memory being mapped, allowing kernel code to directly dereference user pointers. Many operating systems have used these ideas to increase efficiency. Xv6 avoids them in order to reduce the chances of security bugs in the kernel due to inadvertent use of user pointers, and to reduce some complexity that would be required to ensure that user and kernel virtual addresses don’t overlap.

如果将内核内存映射到每个进程的用户页表中（使用适当的PTE权限标志），就可以消除对特殊的蹦床页的需求。这也将消除从用户空间捕捉到内核时对页表切换的需求。这又可以让内核中的系统调用实现利用当前进程的用户内存被映射的优势，让内核代码直接去引用用户指针。很多操作系统都采用了这些思路来提高效率。Xv6避免了这些想法，一是为了减少内核中因无意中使用用户指针而出现安全漏洞的几率，二是为了减少一些复杂性，以保证用户和内核虚拟地址不重叠。

## 4.8 Exercises

1- The functions copyin and copyinstr walk the user page table in software. Set up the kernel page table so that the kernel has the user program mapped, and copyin and copyinstr can use memcpy to copy system call arguments into kernel space, relying on the hardware to do the page table walk.
2- Implement lazy memory allocation
3- Implement COW fork

1- 函数copyin和copyinstr在软件中走用户页表。设置内核页表，使内核有用户程序映射，copyin和copyinstr可以使用memcpy将系统调用参数复制到内核空间，依靠硬件来完成页表的行走。
2- 实现懒惰的内存分配
3-实施COW分叉