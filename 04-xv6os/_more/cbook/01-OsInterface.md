# Chapter 1 -- Operating system interfaces

The job of an operating system is to share a computer among multiple programs and to provide a more useful set of services than the hardware alone supports. An operating system manages and abstracts the low-level hardware, so that, for example, a word processor need not concern itself with which type of disk hardware is being used. An operating system shares the hardware among multiple programs so that they run (or appear to run) at the same time. Finally, operating systems provide controlled ways for programs to interact, so that they can share data or work together.

操作系统的工作是在多个程序之间共享一台计算机，并提供一套比硬件单独支持更有用的服务。操作系统管理和抽象低级硬件，因此，例如，文字处理机不需要关心正在使用哪种类型的磁盘硬件。操作系统在多个程序之间共享硬件，使它们能同时运行（或看起来是运行）。最后，操作系统为程序提供了可控的交互方式，使它们能够共享数据或共同工作。

An operating system provides services to user programs through an interface. Designing a good interface turns out to be difficult. On the one hand, we would like the interface to be simple and narrow because that makes it easier to get the implementation right. On the other hand, we may be tempted to offer many sophisticated features to applications. The trick in resolving this tension is to design interfaces that rely on a few mechanisms that can be combined to provide much generality.

一个操作系统通过一个界面为用户程序提供服务。设计一个好的界面原来是很困难的。一方面，我们希望界面简单而狭窄，因为这样更容易得到正确的实现。另一方面，我们可能会想为应用程序提供许多复杂的功能。解决这种紧张关系的诀窍是设计出依靠一些机制的接口，这些机制可以结合起来提供很多通用性。

This book uses a single operating system as a concrete example to illustrate operating system concepts. That operating system, xv6, provides the basic interfaces introduced by Ken Thompson and Dennis Ritchie’s Unix operating system [14], as well as mimicking Unix’s internal design.

本书以一个单一的操作系统作为具体的例子来说明操作系统的概念。该操作系统xv6提供了Ken Thompson和Dennis Ritchie的Unix操作系统[14]所介绍的基本接口，同时也模仿了Unix的内部设计。

Unix provides a narrow interface whose mechanisms combine well, offering a surprising degree of generality. This interface has been so successful that modern operating systems—BSD, Linux, Mac OS X, Solaris, and even, to a lesser extent, Microsoft Windows—have Unix-like interfaces.

Unix提供了一个狭窄的接口，其机制结合得很好，提供了惊人的通用性。这种接口非常成功，以至于现代操作系统--BSD、Linux、Mac OS X、Solaris，甚至在较小的程度上，微软的Windows也有类似Unix的接口。

Understanding xv6 is a good start toward understanding any of these systems and many others.

As Figure 1.1 shows, xv6 takes the traditional form of a kernel, a special program that provides services to running programs. Each running program, called a process, has memory containing instructions, data, and a stack. The instructions implement the program’s computation. The data are the variables on which the computation acts. The stack organizes the program’s procedure calls.

了解xv6是了解这些系统和其他许多系统的一个良好开端。

如图1.1所示，xv6采用了传统的内核形式，一个为运行中的程序提供服务的特殊程序。每个正在运行的程序，称为进程，有包含指令、数据和堆栈的内存。指令实现了程序的计算。数据是计算作用的变量。栈组织了程序的过程调用。

A given computer typically has many processes but only a single kernel.

When a process needs to invoke a kernel service, it invokes a system call, one of the calls in the operating system’s interface. The system call enters the kernel; the kernel performs the service and returns. Thus a process alternates between executing in user space and kernel space.

一台特定的计算机通常有许多进程，但只有一个内核。

当一个进程需要调用一个内核服务时，它就会调用一个系统调用，也就是操作系统接口中的一个调用。系统调用进入内核，内核执行服务并返回。这样一个进程就在用户空间和内核空间中交替执行。

![](../img/Figure1.1.png)

The kernel uses the hardware protection mechanisms provided by a CPU [1] to ensure that each process executing in user space can access only its own memory. The kernel executes with the hardware privileges required to implement these protections; user programs execute without those privileges. When a user program invokes a system call, the hardware raises the privilege level and starts executing a pre-arranged function in the kernel.

内核使用CPU[1]提供的硬件保护机制来确保在用户空间执行的每个进程只能访问自己的内存。内核以实现这些保护所需的硬件权限来执行，而用户程序的执行则没有这些权限。当用户程序调用系统调用时，硬件会提高特权级别，并开始执行内核中预先安排好的函数。

The collection of system calls that a kernel provides is the interface that user programs see. The xv6 kernel provides a subset of the services and system calls that Unix kernels traditionally offer.

Figure 1.2 lists all of xv6’s system calls.

内核提供的系统调用集合是用户程序看到的界面。xv6内核提供了Unix内核传统上提供的服务和系统调用的子集。

图1.2列出了xv6的所有系统调用。

![](../img/Figure1.2.png)

The rest of this chapter outlines xv6’s services—processes, memory, file descriptors, pipes, and a file system—and illustrates them with code snippets and discussions of how the shell, Unix’s command-line user interface, uses them. The shell’s use of system calls illustrates how carefully they have been designed.

本章的其余部分概述了xv6的服务--处理、内存、文件描述符、管道和文件系统，并通过代码片段和讨论shell（Unix的命令行用户界面）如何使用它们来说明。shell对系统调用的使用说明了它们是如何被精心设计的。

The shell is an ordinary program that reads commands from the user and executes them. The fact that the shell is a user program, and not part of the kernel, illustrates the power of the system call interface: there is nothing special about the shell. It also means that the shell is easy to replace;as a result, modern Unix systems have a variety of shells to choose from, each with its own user interface and scripting features. The xv6 shell is a simple implementation of the essence of the Unix Bourne shell. Its implementation can be found at (user/sh.c:1).

shell是一个普通的程序，它从用户那里读取命令并执行它们。事实上，shell是一个用户程序，而不是内核的一部分，这说明了系统调用接口的力量：shell没有什么特别之处。这也意味着shell很容易替换；因此，现代Unix系统有多种shell可供选择，每种shell都有自己的用户界面和脚本功能。xv6 shell是Unix Bourne shell本质的简单实现。它的实现可以在(user/sh.c:1)找到。

## 1.1 Processes and memory

An xv6 process consists of user-space memory (instructions, data, and stack) and per-process state private to the kernel. Xv6 time-shares processes: it transparently switches the available CPUs among the set of processes waiting to execute. When a process is not executing, xv6 saves its CPU registers, restoring them when it next runs the process. The kernel associates a process identifier, or PID, with each process.

一个xv6进程由用户空间内存（指令、数据和堆栈）和内核私有的每个进程状态组成。Xv6共享进程的时间：它透明地在等待执行的进程集之间切换可用的CPU。当一个进程不执行时，xv6会保存它的CPU寄存器，在下次运行进程时恢复它们。内核为每个进程关联一个进程标识符，或PID。

A process may create a new process using the fork system call. Fork creates a new process, called the child process, with exactly the same memory contents as the calling process, called the parent process. Fork returns in both the parent and the child. In the parent, fork returns the child’s PID; in the child, fork returns zero. For example, consider the following program fragment written in the C programming language [6]:

一个进程可以使用fork系统调用创建一个新进程。Fork创建一个新的进程，称为子进程，其内存内容与调用的进程，称为父进程完全相同。在父进程和子进程中，fork都会返回。在父进程中，fork返回子进程的PID；在子进程中，fork返回0。例如，考虑以下用C程序语言编写的程序片段[6]。

```cpp
int pid = fork();
if(pid > 0){
  printf("parent: child=%d\n", pid);
  pid = wait((int *) 0);
  printf("child %d is done\n", pid);
} else if(pid == 0){
  printf("child: exiting\n");
  exit(0);
} else {
  printf("fork error\n");
}
```

The exit system call causes the calling process to stop executing and to release resources such as memory and open files. Exit takes an integer status argument, conventionally 0 to indicate success and 1 to indicate failure. The wait system call returns the PID of an exited (or killed) child of the current process and copies the exit status of the child to the address passed to wait; if none of the caller’s children has exited, wait waits for one to do so. If the caller has no children, wait immediately returns -1. If the parent doesn’t care about the exit status of a child, it can pass a 0 address to wait.

退出系统调用使调用进程停止执行，并释放资源，如内存和打开的文件。exit需要一个整数状态参数，传统上0表示成功，1表示失败。wait系统调用返回当前进程的一个已退出（或被杀死）的子进程的PID，并将该子进程的退出状态复制到传递给wait的地址；如果调用者的子进程都没有退出，则wait等待一个子进程退出。如果调用者没有子女，wait立即返回-1。如果父代不关心子代的退出状态，可以传递一个0地址给wait。

In the example, the output lines might come out in either order, depending on whether the parent or child gets to its printf call first. After the child exits, the parent’s wait returns, causing the parent to print parent: `child 1234 is done`

在这个例子中，输出行可能以任何一种顺序出来，这取决于父程序还是子程序先到它的printf调用。在子程序退出后，父程序的wait返回，导致父程序打印父程序：`child 1234 is done`


```
parent: child=1234
child: exiting
```

Although the child has the same memory contents as the parent initially, the parent and child are executing with different memory and different registers: changing a variable in one does not affect the other. For example, when the return value of wait is stored into pid in the parent process, it doesn’t change the variable pid in the child. The value of pid in the child will still be zero.

虽然子进程最初的内存内容与父进程相同，但父进程和子进程的执行内存和寄存器不同：改变其中一个进程的变量不会影响另一个进程。例如，当wait的返回值被存储到父进程的pid中时，并不会改变子进程中的变量pid。子进程中的pid值仍然为零。

The exec system call replaces the calling process’s memory with a new memory image loaded from a file stored in the file system. The file must have a particular format, which specifies which part of the file holds instructions, which part is data, at which instruction to start, etc. xv6 uses the ELF format, which Chapter 3 discusses in more detail. When exec succeeds, it does not return to the calling program; instead, the instructions loaded from the file start executing at the entry point declared in the ELF header. Exec takes two arguments: the name of the file containing the executable and an array of string arguments. For example:

exec系统调用用从文件系统中存储的文件加载的新内存映像替换调用进程的内存。该文件必须有特定的格式，它规定了文件中哪部分存放指令，哪部分是数据，在哪条指令启动等，xv6使用ELF格式，第3章将详细讨论。当exec成功时，它并不返回到调用程序；相反，从文件中加载的指令在ELF头声明的入口点开始执行。exec需要两个参数：包含可执行文件的文件名和一个字符串参数数组。例如

```cpp
char *argv[3];
argv[0] = "echo";
argv[1] = "hello";
argv[2] = 0;
exec("/bin/echo", argv);
printf("exec error\n");
```

This fragment replaces the calling program with an instance of the program /bin/echo running with the argument list echo hello. Most programs ignore the first element of the argument array, which is conventionally the name of the program.

这个片段将调用的程序替换为运行参数列表echo hello的程序/bin/echo的实例。大多数程序都会忽略参数数组的第一个元素，也就是传统的程序名称。

The xv6 shell uses the above calls to run programs on behalf of users. The main structure of the shell is simple; see main (user/sh.c:145). The main loop reads a line of input from the user with getcmd. Then it calls fork, which creates a copy of the shell process. The parent calls wait, while the child runs the command. For example, if the user had typed “echo hello” to the shell, runcmd would have been called with “echo hello” as the argument. runcmd (user/sh.c:58) runs the actual command. For “echo hello”, it would call exec (user/sh.c:78). If exec succeeds then the child will execute instructions from echo instead of runcmd. At some point echo will call exit, which will cause the parent to return from wait in main (user/sh.c:145).

xv6 shell使用上述调用代表用户运行程序。shell的主要结构很简单，参见main(user/sh.c:145)。主循环用getcmd读取用户的一行输入，然后调用fork，创建shell的副本。然后调用fork，创建一个shell进程的副本。父循环调用wait，而子循环则运行命令。例如，如果用户向shell输入了 "echo hello"，那么runcmd就会被调用，参数是 "echo hello"，runcmd(user/sh.c:58)运行实际的命令。对于 "echo hello"，它会调用exec (user/sh.c:78)。如果exec成功，那么子程序将执行来自echo的指令，而不是runcmd。在某些时候，echo会调用exit，这将导致父程序从main(user/sh.c:145)中的wait返回。

You might wonder why fork and exec are not combined in a single call; we will see later that the shell exploits the separation in its implementation of I/O redirection. To avoid the wastefulness of creating a duplicate process and then immediately replacing it (with exec), operating kernels optimize the implementation of fork for this use case by using virtual memory techniques such as copy-on-write (see Section 4.6).

你可能会问为什么fork和exec不在一个调用中合并；我们稍后会看到shell在实现I/O重定向时利用了这种分离。为了避免创建一个重复的进程，然后立即替换它（用exec）的浪费，操作内核通过使用虚拟内存技术（如copy-on-write）来优化这种用例的fork实现（见4.6节）。

Xv6 allocates most user-space memory implicitly: fork allocates the memory required for the child’s copy of the parent’s memory, and exec allocates enough memory to hold the executable file. A process that needs more memory at run-time (perhaps for malloc) can call sbrk(n) to grow its data memory by n bytes; sbrk returns the location of the new memory.

Xv6隐式分配大部分用户空间内存：fork分配子程序拷贝父程序内存所需的内存，exec分配足够的内存来容纳可执行文件。一个进程如果在运行时需要更多的内存（可能是为了malloc），可以调用sbrk(n)将其数据内存增长n个字节；sbrk返回新内存的位置。

## 1.2 I/O and File descriptors

A file descriptor is a small integer representing a kernel-managed object that a process may read from or write to. A process may obtain a file descriptor by opening a file, directory, or device, or by creating a pipe, or by duplicating an existing descriptor. For simplicity we’ll often refer to the object a file descriptor refers to as a “file”; the file descriptor interface abstracts away the differences between files, pipes, and devices, making them all look like streams of bytes. We’ll refer to input and output as I/O.

文件描述符是一个小的整数，代表一个进程可以读取或写入的内核管理对象。一个进程可以通过打开一个文件、目录或设备，或通过创建一个管道，或通过复制一个现有的描述符来获得一个文件描述符。为了简单起见，我们经常把文件描述符所指的对象称为 "文件"；文件描述符接口抽象掉了文件、管道和设备之间的差异，使它们看起来都像字节流。我们会把输入和输出称为I/O。

Internally, the xv6 kernel uses the file descriptor as an index into a per-process table, so that every process has a private space of file descriptors starting at zero. By convention, a process reads from file descriptor 0 (standard input), writes output to file descriptor 1 (standard output), and writes error messages to file descriptor 2 (standard error). As we will see, the shell exploits the convention to implement I/O redirection and pipelines. The shell ensures that it always has three file descriptors open (user/sh.c:151), which are by default file descriptors for the console.

在内部，xv6内核使用文件描述符作为每个进程表的索引，因此每个进程都有一个从0开始的文件描述符私有空间。按照惯例，一个进程从文件描述符0(标准输入)读取数据，向文件描述符1(标准输出)写入输出，向文件描述符2(标准错误)写入错误信息。正如我们将看到的，shell利用了 惯例来实现I/O重定向和管道。shell确保它始终有三个文件描述符打开（user/sh.c:151），这些文件描述符默认是控制台的文件描述符。

The read and write system calls read bytes from and write bytes to open files named by file descriptors. The call read(fd, buf, n) reads at most n bytes from the file descriptor fd, copies them into buf, and returns the number of bytes read. Each file descriptor that refers to a file has an offset associated with it. Read reads data from the current file offset and then advances that offset by the number of bytes read: a subsequent read will return the bytes following the ones returned by the first read. When there are no more bytes to read, read returns zero to indicate the end of the file.

读写系统调用从文件描述符命名的文件中读取字节，并向打开的文件写入字节。调用read(fd, buf, n)从文件描述符fd中最多读取n个字节，将它们复制到buf中，并返回读取的字节数。每个引用文件的文件描述符都有一个与之相关的偏移量。读取从当前文件偏移量中读取数据，然后按读取的字节数推进偏移量：随后的读取将返回第一次读取所返回的字节之后的字节。当没有更多的字节可读时，读返回零，表示文件的结束。

The call write(fd, buf, n) writes n bytes from buf to the file descriptor fd and returns the number of bytes written. Fewer than n bytes are written only when an error occurs. Like read, write writes data at the current file offset and then advances that offset by the number of bytes written: each write picks up where the previous one left off.

调用write(fd, buf, n)将buf中的n个字节写入文件描述符fd，并返回写入的字节数。少于n个字节的数据只有在发生错误时才会写入。和read一样，write在当前文件偏移量处写入数据，然后按写入的字节数将偏移量向前推进：每次写入都从上一次写入的地方开始。

The following program fragment (which forms the essence of the program cat) copies data from its standard input to its standard output. If an error occurs, it writes a message to the standard error.

下面的程序片段（它构成了程序cat的本质）将数据从标准输入复制到标准输出。如果发生错误，它就会向标准错误写入一条信息。

```cpp
char buf[512];
int n;
for(;;){
  n = read(0, buf, sizeof buf);
  if(n == 0)
    break;
  if(n < 0){
    fprintf((FILE*)2, "read error\n");
    exit(1);
  }
  if(write(1, buf, n) != n) {
    fprintf((FILE*)2, "write error\n");
    exit(1);
  }
}
```

The important thing to note in the code fragment is that cat doesn’t know whether it is reading from a file, console, or a pipe. Similarly cat doesn’t know whether it is printing to a console, a file, or whatever. The use of file descriptors and the convention that file descriptor 0 is input and file descriptor 1 is output allows a simple implementation of cat.

在代码片段中需要注意的是，cat不知道它是在从文件、控制台还是管道中读取。同样，cat也不知道它是打印到控制台、文件还是其他什么地方。使用文件描述符和文件描述符 0 是输入，文件描述符 1 是输出的约定，使得 cat 的实现非常简单。

The close system call releases a file descriptor, making it free for reuse by a future open, pipe, or dup system call (see below). A newly allocated file descriptor is always the lowestnumbered unused descriptor of the current process.

关闭系统调用会释放一个文件描述符，使它可以被未来的open、pipe或dup系统调用所自由重用（见下文）。新分配的文件描述符总是当前进程中编号最小的未使用描述符。

File descriptors and fork interact to make I/O redirection easy to implement. Fork copies the parent’s file descriptor table along with its memory, so that the child starts with exactly the same open files as the parent. The system call exec replaces the calling process’s memory but preserves its file table. This behavior allows the shell to implement I/O redirection by forking, reopening chosen file descriptors in the child, and then calling exec to run the new program. Here is a simplified version of the code a shell runs for the command `cat < input.txt`:

文件描述符和fork相互作用，使I/O重定向易于实现。Fork将父进程的文件描述符表和它的内存一起复制，这样子进程开始时打开的文件和父进程完全一样。系统调用exec替换了调用进程的内存，但保留了它的文件表。这种行为允许shell通过分叉实现I/O重定向，在子进程中重新打开选定的文件描述符，然后调用exec运行新的程序。下面是shell运行`cat < input.txt`命令的简化版代码。

```cpp
char *argv[2];
argv[0] = "cat";
argv[1] = 0;
if(fork() == 0) {
  close(0);
  open("input.txt", O_RDONLY);
  exec("cat", argv);
}
```

After the child closes file descriptor 0, open is guaranteed to use that file descriptor for the newly opened input.txt: 0 will be the smallest available file descriptor. Cat then executes with file descriptor 0 (standard input) referring to input.txt. The parent process’s file descriptors are not changed by this sequence, since it modifies only the child’s descriptors.

在子程序关闭文件描述符0后，open保证对新打开的input.txt使用该文件描述符。0将是最小的可用文件描述符。然后Cat执行时，文件描述符0（标准输入）指的是input.txt。这个序列不会改变父进程的文件描述符，因为它只修改子进程的描述符。

The code for I/O redirection in the xv6 shell works in exactly this way (user/sh.c:82). Recall that at this point in the code the shell has already forked the child shell and that runcmd will call exec to load the new program.

xv6 shell中I/O重定向的代码完全是这样工作的（user/sh.c:82）。回想一下，在代码的这一点上，shell已经分叉了子shell，runcmd将调用exec来加载新的程序。

The second argument to open consists of a set of flags, expressed as bits, that control what open does. The possible values are defined in the file control (fcntl) header (kernel/fcntl.h:1-5): O_RDONLY, O_WRONLY, O_RDWR, O_CREATE, and O_TRUNC, which instruct open to open the file for reading, or for writing, or for both reading and writing, to create the file if it doesn’t exist, and to truncate the file to zero length.

open的第二个参数由一组用位表示的标志组成，用来控制open的工作。可能的值在文件控制(fcntl)头(kernel/fcntl.h:1-5)中定义。O_RDONLY、O_WRONLY、O_RDWR、O_CREATE和O_TRUNC，它们指示open打开文件进行读、写、或同时进行读和写，如果文件不存在，则创建文件，并将文件截断为零长度。

Now it should be clear why it is helpful that fork and exec are separate calls: between the two, the shell has a chance to redirect the child’s I/O without disturbing the I/O setup of the main shell. One could instead imagine a hypothetical combined forkexec system call, but the options for doing I/O redirection with such a call seem awkward. The shell could modify its own I/O setup before calling forkexec (and then un-do those modifications); or forkexec could take instructions for I/O redirection as arguments; or (least attractively) every program like cat could be taught to do its own I/O redirection.

现在应该清楚为什么fork和exec是分开调用的：在这两个调用之间，shell有机会重定向子系统的I/O，而不干扰主shell的I/O设置。我们可以想象一个假想的结合forkexec的系统调用，但用这种调用来做I/O重定向的选择似乎很尴尬。shell可以在调用forkexec之前修改自己的I/O设置（然后取消这些修改）；或者forkexec可以将I/O重定向的指令作为参数；或者（最不吸引人的是）每个程序（比如cat）都可以被教导做自己的I/O重定向。

Although fork copies the file descriptor table, each underlying file offset is shared between parent and child. Consider this example:

虽然fork复制了文件描述符表，但每个底层文件的偏移量是父子共享的。考虑这个例子。

```cpp
if(fork() == 0) {
  write(1, "hello ", 6);
  exit(0);
} else {
  wait(0);
  write(1, "world\n", 6);
}
```

At the end of this fragment, the file attached to file descriptor 1 will contain the data hello world. The write in the parent (which, thanks to wait, runs only after the child is done) picks up where the child’s write left off. This behavior helps produce sequential output from sequences of shell commands, like `(echo hello; echo world) >output.txt`.

在这个片段的最后，文件描述符1所附的文件将包含数据hello world。父文件中的写（由于有了wait，只有在子文件完成后才会运行）会从子文件的写结束的地方开始。这种行为有助于从shell命令的序列中产生连续的输出，比如`(echo hello; echo world) >output.txt`。

The dup system call duplicates an existing file descriptor, returning a new one that refers to the same underlying I/O object. Both file descriptors share an offset, just as the file descriptors duplicated by fork do. This is another way to write hello world into a file:

dup系统调用复制一个现有的文件描述符，返回一个新的描述符，它指向同一个底层I/O对象。两个文件描述符共享一个偏移量，就像被fork复制的文件描述符一样。这是将hello world写入文件的另一种方式。

```cpp
fd = dup(1);
write(1, "hello ", 6);
write(fd, "world\n", 6);
```

Two file descriptors share an offset if they were derived from the same original file descriptor by a sequence of fork and dup calls. Otherwise file descriptors do not share offsets, even if they resulted from open calls for the same file. Dup allows shells to implement commands like this:

如果两个文件描述符是通过一系列的fork和dup调用从同一个原始文件描述符衍生出来的，那么这两个文件描述符共享一个偏移量。否则，文件描述符不共享偏移量，即使它们是由同一文件的打开调用产生的。Dup允许shell实现这样的命令。

`ls existing-file non-existing-file > tmp1 2>&1`. The 2>&1 tells the shell to give the command a file descriptor 2 that is a duplicate of descriptor 1. Both the name of the existing file and the error message for the non-existing file will show up in the file tmp1. The xv6 shell doesn’t support I/O redirection for the error file descriptor, but now you know how to implement it.

`ls existing-file non-existing-file > tmp1 2>&1`。2>&1告诉shell给命令一个与描述符1重复的文件描述符2。现有文件的名称和不存在文件的错误信息都会显示在文件tmp1中。xv6 shell不支持错误文件描述符的I/O重定向，但现在你知道如何实现它了。

File descriptors are a powerful abstraction, because they hide the details of what they are connected to: a process writing to file descriptor 1 may be writing to a file, to a device like the console, or to a pipe.

文件描述符是一个强大的抽象，因为它们隐藏了它们所连接的细节：一个向文件描述符1写入的进程可能是在向一个文件、控制台等设备或向一个管道写入。

## 1.3 Pipes

A pipe is a small kernel buffer exposed to processes as a pair of file descriptors, one for reading and one for writing. Writing data to one end of the pipe makes that data available for reading from the other end of the pipe. Pipes provide a way for processes to communicate.

The following example code runs the program wc with standard input connected to the read end of a pipe.

管道是一个小的内核缓冲区，作为一对文件描述符暴露给进程，一个用于读，一个用于写。将数据写入管道的一端就可以从管道的另一端读取数据。管道为进程提供了一种通信方式。

下面的示例代码运行程序wc，标准输入连接到管道的读端。

```cpp
int p[2];
char *argv[2];
argv[0] = "wc";
argv[1] = 0;
pipe(p);
if(fork() == 0) {
  close(0);
  dup(p[0]); // 複製 p[0] 到 fd:0
  close(p[0]);
  close(p[1]);
  exec("/bin/wc", argv); // child: 這樣 wc 會由 stdin (p[0], fd:0) 讀入，於是讀到 hello world
} else {
  close(p[0]);
  write(p[1], "hello world\n", 12); // parent: 寫入 hello world 到 p[1]
  close(p[1]);
}
```

The program calls pipe, which creates a new pipe and records the read and write file descriptors in the array p. After fork, both parent and child have file descriptors referring to the pipe. The child calls close and dup to make file descriptor zero refer to the read end of the pipe, closes the file descriptors in p, and calls exec to run wc. When wc reads from its standard input, it reads from the pipe. The parent closes the read side of the pipe, writes to the pipe, and then closes the write side.

程序调用pipe，创建一个新的管道，并将读写文件描述符记录在数组p中，经过fork后，父代和子代的文件描述符都指向管道。子程序调用close和dup使文件描述符0引用到管道的读端，关闭p中的文件描述符，并调用exec运行wc。当wc从其标准输入端读取时，它从管道讀。母体关闭管道的读端，向管道写入，然后关闭写端。

If no data is available, a read on a pipe waits for either data to be written or for all file descriptors referring to the write end to be closed; in the latter case, read will return 0, just as if the end of a data file had been reached. The fact that read blocks until it is impossible for new data to arrive is one reason that it’s important for the child to close the write end of the pipe before executing wc above: if one of wc ’s file descriptors referred to the write end of the pipe, wc would never see end-of-file.

如果没有数据，管道上的读会等待数据被写入或等待所有引用写端的文件描述符被关闭；在后一种情况下，读将返回0，就像已经到达数据文件的末端一样。事实上，读会阻塞直到不可能有新的数据到达，这也是一个原因，即在执行 上面的wc：如果wc的文件描述符之一指向管道的写端，wc将永远不会看到文件结束。

The xv6 shell implements pipelines such as `grep fork sh.c | wc -l` in a manner similar to the above code (user/sh.c:100). The child process creates a pipe to connect the left end of the pipeline with the right end. Then it calls fork and runcmd for the left end of the pipeline and fork and runcmd for the right end, and waits for both to finish. The right end of the pipeline may be a command that itself includes a pipe (e.g., a | b | c), which itself forks two new child processes (one for b and one for c). Thus, the shell may create a tree of processes. The leaves of this tree are commands and the interior nodes are processes that wait until the left and right children complete.

xv6的shell实现管道，如 `grep fork sh.c | wc -l` 的方式类似于上面的代码（user/sh.c:100）。子进程创建一个管道来连接管道的左端和右端。然后，它为管道左端调用fork和runcmd，为右端调用fork和runcmd，并等待两者的完成。管道的右端可以是一个命令，它本身包括一个管道（例如，a | b | c），它本身会分叉两个新的子进程（一个是b，一个是c）。因此，shell可以创建一棵进程树。这棵树的叶子是命令，内部节点是等待左右子进程完成的进程。

In principle, one could have the interior nodes run the left end of a pipeline, but doing so correctly would complicate the implementation. Consider making just the following modification: change sh.c to not fork for p->left and run runcmd(p->left) in the interior process. Then, for example, echo hi | wc won’t produce output, because when echo hi exits in runcmd, the interior process exits and never calls fork to run the right end of the pipe. This incorrect behavior could be fixed by not calling exit in runcmd for interior processes, but this fix complicates the code: now runcmd needs to know if it a interior process or not. Complications also arise when not forking for runcmd(p->right). For example, with just that modification, sleep 10 | echo hi will immediately print “hi” instead of after 10 seconds, because echo runs immediately and exits, not waiting for sleep to finish. Since the goal of the sh.c is to be as simple as possible, it doesn’t try to avoid creating interior processes.

原则上，我们可以让内部节点运行管道的左端，但正确地这样做会使实现变得复杂。考虑只做以下修改：将sh.c改为不为p->left分叉，在内部进程中运行runcmd(p->left)。然后，例如，echo hi | wc不会产生输出，因为当echo hi在runcmd中退出时，内部进程会退出，并且从未调用fork来运行管道的右端。这种不正确的行为可以通过不在runcmd中对内部进程调用exit来修正，但是这种修正使代码变得复杂：现在runcmd需要知道它是否是内部进程。当不对runcmd(p->right)进行分叉时，也会产生复杂的情况。例如，仅仅是这样的修改，sleep 10 | echo hi会立即打印 "hi"，而不是在10秒后，因为echo会立即运行并退出，而不是等待sleep结束。由于sh.c的目标是要让它像 尽可能的简单，它并不试图避免创建内部流程。

Pipes may seem no more powerful than temporary files: the pipeline `echo hello world | wc` could be implemented without pipes as `echo hello world >/tmp/xyz; wc </tmp/xyz` Pipes have at least four advantages over temporary files in this situation. First, pipes automatically clean themselves up; with the file redirection, a shell would have to be careful to remove /tmp/xyz when done. Second, pipes can pass arbitrarily long streams of data, while file redirection requires enough free space on disk to store all the data. Third, pipes allow for parallel execution of pipeline stages, while the file approach requires the first program to finish before the second starts. Fourth, if you are implementing inter-process communication, pipes’ blocking reads and writes are more efficient than the non-blocking semantics of files.

管道似乎并不比临时文件强大：管道 `echo hello world | wc`可以不用管道实现为 `echo hello world >/tmp/xyz; wc </tmp/xyz ` 在这种情况下，管道比临时文件至少有四个优势。首先，管道会自动清理自己；如果使用文件重定向，shell在完成后必须小心翼翼地删除/tmp/xyz。第二，管道可以传递任意长的数据流，而文件重定向则需要磁盘上有足够的空闲空间来存储所有数据。第三，管道可以实现管道阶段的并行执行，而文件方式则需要在第二个程序开始之前完成第一个程序。第四，如果你要实现进程间的通信，管道的阻塞读写比文件的非阻塞语义更有效率。

## 1.4 File system

The xv6 file system provides data files, which contain uninterpreted byte arrays, and directories, which contain named references to data files and other directories. The directories form a tree, starting at a special directory called the root. A path like /a/b/c refers to the file or directory named c inside the directory named b inside the directory named a in the root directory /. Paths that don’t begin with / are evaluated relative to the calling process’s current directory, which can be changed with the chdir system call. Both these code fragments open the same file (assuming all the directories involved exist):

xv6 文件系统提供了数据文件（包含未解释的字节数组）和目录（包含对数据文件和其他目录的命名引用）。这些目录形成一棵树，从一个称为根目录的特殊目录开始。像/a/b/c这样的路径指的是根目录/中名为b的目录内名为c的文件或名为c的目录，不以/开头的路径是相对于调用进程的当前目录进行评估的，可以通过chdir系统调用来改变。这两个代码片段都会打开同一个文件（假设所有涉及的目录都存在）。

```cpp
chdir("/a");
chdir("b");
open("c", O_RDONLY);
open("/a/b/c", O_RDONLY);
```

The first fragment changes the process’s current directory to /a/b; the second neither refers to nor changes the process’s current directory.

第一个片段将进程的当前目录改为/a/b；第二个片段既不引用也不改变进程的当前目录。

There are system calls to create new files and directories: mkdir creates a new directory, open with the O_CREATE flag creates a new data file, and mknod creates a new device file. This example illustrates all three:

有一些系统调用来创建新的文件和目录：mkdir创建一个新的目录，用O_CREATE标志打开创建一个新的数据文件，以及mknod创建一个新的设备文件。这个例子说明了这三种方法。

```cpp
mkdir("/dir");
fd = open("/dir/file", O_CREATE|O_WRONLY);
close(fd);
mknod("/console", 1, 1);
```

Mknod creates a special file that refers to a device. Associated with a device file are the major and minor device numbers (the two arguments to mknod), which uniquely identify a kernel device.

Mknod创建了一个特殊的文件，它指的是一个设备。与设备文件相关联的是主要设备号和次要设备号(mknod的两个参数)，它们唯一地标识一个内核设备。

When a process later opens a device file, the kernel diverts read and write system calls to the kernel device implementation instead of passing them to the file system.

当一个进程后来打开设备文件时，内核会将系统的读写调用分流给内核设备实现，而不是将它们传递给文件系统。

A file’s name is distinct from the file itself; the same underlying file, called an inode, can have multiple names, called links. Each link consists of an entry in a directory; the entry contains a file name and a reference to an inode. An inode holds metadata about a file, including its type (file or directory or device), its length, the location of the file’s content on disk, and the number of links to a file.

一个文件的名称与文件本身是不同的；同一个基础文件，称为inode，可以有多个名称，称为链接。每个链接由目录中的一个条目组成；该条目包含一个文件名和对inode的引用。一个inode保存着一个文件的元数据，包括它的类型(文件或目录或设备)、它的长度、文件内容在磁盘上的位置以及到文件的链接数量。

The fstat system call retrieves information from the inode that a file descriptor refers to. It fills in a struct stat, defined in stat.h (kernel/stat.h) as:

fstat系统调用从文件描述符引用的inode中检索信息。它在 stat.h (kernel/stat.h)中定义的 stat 结构中填入。

```cpp
#define T_DIR 1 // Directory
#define T_FILE 2 // File
#define T_DEVICE 3 // Device

struct stat {
  int dev; // File system’s disk device
  uint ino; // Inode number
  short type; // Type of file
  short nlink; // Number of links to file
  uint64 size; // Size of file in bytes
};
```

The link system call creates another file system name referring to the same inode as an existing file. This fragment creates a new file named both a and b.

链接系统调用创建了另一个文件系统名，引用同一个inode作为现有文件。这个片段创建了一个同时命名为a和b的新文件。

```cpp
open("a", O_CREATE|O_WRONLY);
link("a", "b");
```

Reading from or writing to a is the same as reading from or writing to b. Each inode is identified by a unique inode number. After the code sequence above, it is possible to determine that a and b refer to the same underlying contents by inspecting the result of fstat: both will return the same inode number (ino), and the nlink count will be set to 2.

从a读入或写入a与从b读入或写入b是一样的，每个inode都有一个唯一的inode号来标识。经过上面的代码序列，可以通过检查fstat的结果来确定a和b指的是同一个底层内容：两者将返回相同的inode号（ino），并且nlink计数将被设置为2。

The unlink system call removes a name from the file system. The file’s inode and the disk space holding its content are only freed when the file’s link count is zero and no file descriptors refer to it. Thus adding

取消链接系统调用从文件系统中删除一个名字。只有当文件的链接数为零且没有文件描述符引用它时，文件的inode和存放其内容的磁盘空间才会被释放。因此，在文件系统中添加

```cpp
unlink("a");
```

to the last code sequence leaves the inode and file content accessible as b. Furthermore,

到最后一个代码序列，使inode和文件内容以b的形式被访问，此外,

```cpp
fd = open("/tmp/xyz", O_CREATE|O_RDWR);
unlink("/tmp/xyz");
```

is an idiomatic way to create a temporary inode with no name that will be cleaned up when the process closes fd or exits.

是一种习惯性的方法，用来创建一个没有名字的临时inode，当进程关闭fd或退出时，这个inode会被清理掉。

Unix provides file utilities callable from the shell as user-level programs, for example mkdir, ln, and rm. This design allows anyone to extend the command-line interface by adding new userlevel programs. In hindsight this plan seems obvious, but other systems designed at the time of Unix often built such commands into the shell (and built the shell into the kernel).

Unix提供了可从shell调用的用户级文件实用程序，例如mkdir、ln和rm。这种设计允许任何人通过添加新的用户级程序来扩展命令行界面。事后看来，这个计划似乎是显而易见的，但在Unix时期设计的其他系统通常将这类命令内置到shell中（并将shell内置到内核中）。

One exception is cd, which is built into the shell (user/sh.c:160). cd must change the current working directory of the shell itself. If cd were run as a regular command, then the shell would fork a child process, the child process would run cd, and cd would change the child ’s working directory. The parent’s (i.e., the shell’s) working directory would not change.

一个例外是cd，它是内置在shell中的(user/sh.c:160)，cd必须改变shell本身的当前工作目录。如果cd是作为常规命令运行，那么shell将分叉一个子进程，子进程将运行cd，cd将改变子进程的工作目录。父进程（即shell）的工作目录不会改变。

## 1.5 Real world

Unix’s combination of “standard” file descriptors, pipes, and convenient shell syntax for operations on them was a major advance in writing general-purpose reusable programs. The idea sparked a culture of “software tools” that was responsible for much of Unix’s power and popularity, and the shell was the first so-called “scripting language.” The Unix system call interface persists today in systems like BSD, Linux, and Mac OS X.

Unix将 "标准 "文件描述符、管道和方便的shell语法结合起来对它们进行操作，是编写通用可重用程序的一大进步。这个想法引发了一种 "软件工具 "文化，Unix的强大和流行在很大程度上要归功于这种文化，shell是第一种所谓的 "脚本语言"。今天，Unix系统调用接口在BSD、Linux和Mac OS X等系统中依然存在。

The Unix system call interface has been standardized through the Portable Operating System Interface (POSIX) standard. Xv6 is not POSIX compliant: it is missing many system calls (including basic ones such as lseek), and many of the system calls it does provide differ from the standard. Our main goals for xv6 are simplicity and clarity while providing a simple UNIX-like system-call interface. Several people have extended xv6 with a few more system calls and a simple C library in order to run basic Unix programs. Modern kernels, however, provide many more system calls, and many more kinds of kernel services, than xv6. For example, they support networking, windowing systems, user-level threads, drivers for many devices, and so on. Modern kernels evolve continuously and rapidly, and offer many features beyond POSIX.

Unix系统调用接口已经通过便携式操作系统接口(POSIX)标准进行了标准化。Xv6 并不符合 POSIX 标准：它缺少许多系统调用（包括基本的系统调用，如 lseek），而且它提供的许多系统调用与标准不同。我们对xv6的主要目标是简单明了，同时提供一个简单的类似UNIX的系统调用接口。一些人已经用一些更多的系统调用和一个简单的C库扩展了xv6，以便运行基本的Unix程序。然而，现代内核比xv6提供了更多的系统调用和更多种类的内核服务。例如，它们支持网络、窗口系统、用户级线程、许多设备的驱动程序等等。现代内核不断快速发展，并提供了许多超越POSIX的功能。

Unix unified access to multiple types of resources (files, directories, and devices) with a single set of file-name and file-descriptor interfaces. This idea can be extended to more kinds of resources; a good example is Plan 9 [13], which applied the “resources are files” concept to networks, graphics, and more. However, most Unix-derived operating systems have not followed this route.

Unix用一套文件名和文件描述符接口统一了对多种类型资源（文件、目录和设备）的访问。这个思想可以扩展到更多种类的资源；一个很好的例子是Plan 9[13]，它把 "资源就是文件 "的概念应用到网络、图形等方面。然而，大多数Unix衍生的操作系统都没有遵循这一路线。

The file system and file descriptors have been powerful abstractions. Even so, there are other models for operating system interfaces. Multics, a predecessor of Unix, abstracted file storage in a way that made it look like memory, producing a very different flavor of interface. The complexity of the Multics design had a direct influence on the designers of Unix, who tried to build something simpler.

文件系统和文件描述符已经是强大的抽象。即便如此，操作系统接口还有其他模式。Multics是Unix的前身，它以一种使文件存储看起来像内存的方式抽象了文件存储，产生了一种截然不同的界面风味。Multics设计的复杂性直接影响了Unix的设计者，他们试图建立一些更简单的东西。

Xv6 does not provide a notion of users or of protecting one user from another; in Unix terms, all xv6 processes run as root.

Xv6没有提供用户的概念，也没有提供保护一个用户与另一个用户的概念；用Unix的术语来说，所有的xv6进程都以root身份运行。

This book examines how xv6 implements its Unix-like interface, but the ideas and concepts apply to more than just Unix. Any operating system must multiplex processes onto the underlying hardware, isolate processes from each other, and provide mechanisms for controlled inter-process communication. After studying xv6, you should be able to look at other, more complex operating systems and see the concepts underlying xv6 in those systems as well.

本书研究的是xv6如何实现其类似Unix的接口，但其思想和概念不仅仅适用于Unix。任何操作系统都必须将进程复用到底层硬件上，将进程相互隔离，并提供受控进程间通信的机制。在学习了xv6之后，您应该能够研究其他更复杂的操作系统，并在这些系统中看到xv6的基本概念。

## 1.6 Exercises

1. Write a program that uses UNIX system calls to “ping-pong” a byte between two processes over a pair of pipes, one for each direction. Measure the program’s performance, in exchanges per second.

1. 编写一个程序，使用UNIX系统调用在两个进程之间通过一对管道（每个方向一个）"乒乓 "一个字节。测量该程序的性能，以每秒交换次数为单位。
