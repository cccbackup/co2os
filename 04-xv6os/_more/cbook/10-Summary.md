# Chapter 10 -- Summary

This text introduced the main ideas in operating systems by studying one operating system, xv6, line by line. Some code lines embody the essence of the main ideas (e.g., context switching, user/kernel boundary, locks, etc.) and each line is important; other code lines provide an illustration of how to implement a particular operating system idea and could easily be done in different ways (e.g., a better algorithm for scheduling, better on-disk data structures to represent files, better logging to allow for concurrent transactions, etc.). All the ideas were illustrated in the context of one particular, very successful system call interface, the Unix interface, but those ideas carry over to the design of other operating systems.

本篇课文通过逐行研究一个操作系统xv6，介绍了操作系统的主要思想。有些代码行体现了主要思想的精髓（如上下文切换、用户/内核边界、锁等），每一行都很重要；另一些代码行则提供了如何实现某个操作系统思想的说明，而且很容易用不同的方法来实现（如更好的调度算法、更好的磁盘数据结构来表示文件、更好的日志记录来实现并发事务等）。所有的想法都是在一个特定的、非常成功的系统调用接口--Unix接口的背景下说明的，但是这些想法也会延续到其他操作系统的设计中。
