# Chapter 6 -- Locking

Most kernels, including xv6, interleave the execution of multiple activities. One source of interleaving is multiprocessor hardware: computers with multiple CPUs executing independently, such as xv6’s RISC-V. These multiple CPUs share physical RAM, and xv6 exploits the sharing to maintain data structures that all CPUs read and write. This sharing raises the possibility of one CPU reading a data structure while another CPU is mid-way through updating it, or even multiple CPUs updating the same data simultaneously; without careful design such parallel access is likely to yield incorrect results or a broken data structure. Even on a uniprocessor, the kernel may switch the CPU among a number of threads, causing their execution to be interleaved. Finally, a device interrupt handler that modifies the same data as some interruptible code could damage the data if the interrupt occurs at just the wrong time. The word concurrency refers to situations in which multiple instruction streams are interleaved, due to multiprocessor parallelism, thread switching, or interrupts.

大多数内核，包括xv6，都会交错执行多个活动。交错执行的一个来源是多处理器硬件：具有多个CPU独立执行的计算机，如xv6的RISC-V。这些多个CPU共享物理RAM，xv6利用共享来维护所有CPU读写的数据结构。这种共享带来了一种可能性，即一个CPU在读取数据结构的同时，另一个CPU正在中途更新数据结构，甚至多个CPU同时更新相同的数据；如果不仔细设计，这种并行访问很可能产生错误的结果或数据结构被破坏。即使在单处理器上，内核也可能在多个线程之间切换CPU，导致它们的执行交错。最后，如果中断发生的时间不对，一个设备中断处理程序可能会修改与一些可中断代码相同的数据，从而破坏数据。并发一词指的是由于多处理器并行、线程切换或中断而导致多个指令流交错的情况。

Kernels are full of concurrently-accessed data. For example, two CPUs could simultaneously call kalloc, thereby concurrently popping from the head of the free list. Kernel designers like to allow for lots of concurrency, since it can yield increased performance though parallelism, and increased responsiveness. However, as a result kernel designers spend a lot of effort convincing themselves of correctness despite such concurrency. There are many ways to arrive at correct code, some easier to reason about than others. Strategies aimed at correctness under concurrency, and abstractions that support them, are called concurrency control techniques.

内核中充满了并发访问的数据。例如，两个CPU可以同时调用kalloc，从而并发地从自由列表的头部弹出。内核设计者喜欢允许大量的并发，因为它可以通过并行来提高性能，提高响应速度。然而，结果是内核设计者花了很多精力说服自己，尽管有这样的并发性，但仍然是正确的。有很多方法可以得出正确的代码，有些方法比其他方法更容易推理。以并发下的正确性为目标的策略，以及支持这些策略的抽象，被称为并发控制技术。

Xv6 uses a number of concurrency control techniques, depending on the situation; many more are possible. This chapter focuses on a widely used technique: the lock. A lock provides mutual exclusion, ensuring that only one CPU at a time can hold the lock. If the programmer associates a lock with each shared data item, and the code always holds the associated lock when using an item, then the item will be used by only one CPU at a time. In this situation, we say that the lock protects the data item. Although locks are an easy-to-understand concurrency control mechanism, the downside of locks is that they can kill performance, because they serialize concurrent operations.

The rest of this chapter explains why xv6 needs locks, how xv6 implements them, and how it uses them.

Xv6根据不同的情况，使用了很多并发控制技术，还有更多的可能。本章重点介绍一种广泛使用的技术：锁。锁提供了相互排斥的功能，确保一次只有一个CPU可以持有锁。如果程序员为每个共享数据项关联一个锁，并且代码在使用某项时总是持有关联的锁，那么该项每次只能由一个CPU使用。在这种情况下，我们说锁保护了数据项。虽然锁是一种易于理解的并发控制机制，但锁的缺点是它们会扼杀性能，因为它们将并发操作序列化。

本章的其余部分将解释为什么xv6需要锁，xv6如何实现锁，以及如何使用锁。

![](../img/Figure6.1.png)

## 6.1 Race conditions

As an example of why we need locks, consider two processes calling wait on two different CPUs. wait frees the child’s memory. Thus on each CPU, the kernel will call kfree to free the children’s pages. The kernel allocator maintains a linked list: kalloc() (kernel/kalloc.c:69) pops a page of memory from a list of free pages, and kfree() (kernel/kalloc.c:47) pushes a page onto the free list. For best performance, we might hope that the kfrees of the two parent processes would execute in parallel without either having to wait for the other, but this would not be correct given xv6’s kfree implementation.

Figure 6.1 illustrates the setting in more detail: the linked list is in memory that is shared by the two CPUs, which manipulate the linked list using load and store instructions. (In reality, the processors have caches, but conceptually multiprocessor systems behave as if there were a single, shared memory.) If there were no concurrent requests, you might implement a list push operation as follows:

作为我们为什么需要锁的一个例子，考虑两个进程在两个不同的CPU上调用wait，wait释放子内存。因此，在每个CPU上，内核都会调用kfree来释放子进程的页面。内核分配器维护了一个链接列表：kalloc() (kernel/kalloc.c:69)从空闲页列表中弹出一个内存页，kfree() (kernel/kalloc.c:47)将一个页推到空闲列表中。为了达到最好的性能，我们可能希望两个父进程的kfrees能够并行执行，而不需要任何一个进程等待另一个进程，但考虑到xv6的kfree实现，这是不正确的。

图6.1更详细地说明了这种设置：链接列表在内存中，由两个CPU共享，它们使用加载和存储指令操作链接列表。(在现实中，处理器有缓存，但在概念上，多处理器系统的行为就像有一个单一的共享内存一样)。如果没有并发请求，你可能会实现如下的列表推送操作。

```cpp
1 struct element {
2 int data;
3 struct element *next;
4 };
5
6 struct element *list = 0;
7
8 void
9 push(int data)
10 {
11   struct element *l;
12
13   l = malloc(sizeof *l);
14   l->data = data;
15   l->next = list;
16   list = l;
17 }
```

![](../img/Figure6.2.png)

This implementation is correct if executed in isolation. However, the code is not correct if more than one copy executes concurrently. If two CPUs execute push at the same time, both might execute line 15 as shown in Fig 6.1, before either executes line 16, which results in an incorrect outcome as illustrated by Figure 6.2. There would then be two list elements with next set to the former value of list. When the two assignments to list happen at line 16, the second one will overwrite the first; the element involved in the first assignment will be lost.

The lost update at line 16 is an example of a race condition. A race condition is a situation in which a memory location is accessed concurrently, and at least one access is a write. A race is often a sign of a bug, either a lost update (if the accesses are writes) or a read of an incompletely-updated data structure. The outcome of a race depends on the exact timing of the two CPUs involved and how their memory operations are ordered by the memory system, which can make race-induced errors difficult to reproduce and debug. For example, adding print statements while debugging push might change the timing of the execution enough to make the race disappear.

The usual way to avoid races is to use a lock. Locks ensure mutual exclusion, so that only one CPU at a time can execute the sensitive lines of push; this makes the scenario above impossible. The correctly locked version of the above code adds just a few lines (highlighted in yellow):

如果单独执行，这个实现是正确的。但是，如果多个副本同时执行，代码就不正确。如果两个CPU同时执行push，那么两个CPU可能都会执行图6.1所示的第15行，然后其中一个才执行第16行，这就会产生一个不正确的结果，如图6.2所示。这样就会出现两个list元素，next设为list的前值。当对list的两次赋值发生在第16行时，第二次赋值将覆盖第一次赋值；第一次赋值中涉及的元素将丢失。

第16行的更新丢失是一个竞赛条件的例子。赛跑条件是指同时访问一个内存位置，并且至少有一次访问是写的情况。赛跑通常是一个错误的标志，要么是丢失的更新（如果访问是写），要么是读取一个不完全更新的数据结构。赛跑的结果取决于所涉及的两个CPU的确切时间，以及它们的内存操作如何被内存系统排序，这可能会使赛跑引起的错误难以重现和调试。例如，在调试push时加入print语句可能会改变执行的时机，足以使竞赛消失。

避免竞赛的通常方法是使用锁。锁可以确保相互排斥，这样一次只能有一个CPU执行push的敏感行；这就使得上面的情况不可能发生。上述代码的正确锁定版本只增加了几行代码（用黄色高亮显示）。

```cpp
6 struct element *list = 0;
7 struct lock listlock;
8
9 void
10 push(int data)
11 {
12   struct element *l;
13   l = malloc(sizeof *l);
14   l->data = data;
15
16   acquire(&listlock);
17   l->next = list;
18   list = l;
19   release(&listlock);
20 }
```


The sequence of instructions between acquire and release is often called a critical section.

The lock is typically said to be protecting list.

When we say that a lock protects data, we really mean that the lock protects some collection of invariants that apply to the data. Invariants are properties of data structures that are maintained across operations. Typically, an operation’s correct behavior depends on the invariants being true when the operation begins. The operation may temporarily violate the invariants but must reestablish them before finishing. For example, in the linked list case, the invariant is that list points at the first element in the list and that each element’s next field points at the next element. The implementation of push violates this invariant temporarily: in line 17, l points to the next list element, but list does not point at l yet (reestablished at line 18). The race condition we examined above happened because a second CPU executed code that depended on the list invariants while they were (temporarily) violated. Proper use of a lock ensures that only one CPU at a time can operate on the data structure in the critical section, so that no CPU will execute a data structure operation when the data structure’s invariants do not hold.

获取和释放之间的指令序列通常被称为关键部分。

锁通常被说成是保护列表。

当我们说锁保护数据的时候，其实是指锁保护了一些适用于数据的不变量的集合。不变量是数据结构的属性，这些属性在不同的操作中被维护。通常情况下，一个操作的正确行为取决于操作开始时的不变量是否为真。操作可能会暂时违反不变量，但必须在结束前重新建立不变量。例如，在链接列表的情况下，不变性是list指向列表中的第一个元素，并且每个元素的下一个字段指向下一个元素。push的实现暂时违反了这个不变性：在第17行，l指向下一个list元素，但list还没有指向l（在第18行重新建立）。我们上面所研究的竞赛条件之所以发生，是因为第二个CPU执行了依赖于列表不变式的代码，而它们被（暂时）违反了。正确使用锁可以确保一次只能有一个CPU对关键部分的数据结构进行操作，因此当数据结构的不变式不成立时，没有CPU会执行数据结构操作。

You can think of a lock as serializing concurrent critical sections so that they run one at a time, and thus preserve invariants (assuming the critical sections are correct in isolation). You can also think of critical sections guarded by the same lock as being atomic with respect to each other, so that each sees only the complete set of changes from earlier critical sections, and never sees partially-completed updates.

Although correct use of locks can make incorrect code correct, locks limit performance. For example, if two processes call kfree concurrently, the locks will serialize the two calls, and we obtain no benefit from running them on different CPUs. We say that multiple processes conflict if they want the same lock at the same time, or that the lock experiences contention. A major challenge in kernel design is to avoid lock contention. Xv6 does little of that, but sophisticated kernels organize data structures and algorithms specifically to avoid lock contention. In the list example, a kernel may maintain a free list per CPU and only touch another CPU’s free list if the CPU’s list is empty and it must steal memory from another CPU. Other use cases may require more complicated designs.

The placement of locks is also important for performance. For example, it would be correct to move acquire earlier in push: it is fine to move the call to acquire up to before line 13. This may reduce performance because then the calls to malloc are also serialized. The section “Using locks” below provides some guidelines for where to insert acquire and release invocations.

你可以把锁看成是把并发的关键部分序列化，使它们一次只运行一个，从而保存不变性（假设关键部分是孤立的）。你也可以认为由同一个锁所保护的关键部分是相互原子的，所以每个关键部分只看到早期关键部分的完整变化，而永远不会看到部分完成的更新。

虽然正确使用锁可以使不正确的代码变得正确，但锁限制了性能。例如，如果两个进程同时调用kfree，锁会将两个调用序列化，我们在不同的CPU上运行它们不会获得任何好处。我们说，如果多个进程同时想要同一个锁，就会发生冲突，或者说锁经历了争夺。内核设计的一个主要挑战是避免锁的争用。Xv6在这方面做得很少，但是复杂的内核会专门组织数据结构和算法来避免锁争用。在列表的例子中，一个内核可能会维护每个CPU的空闲列表，只有当CPU的列表是空的，并且它必须从另一个CPU偷取内存时，才会接触另一个CPU的空闲列表。其他用例可能需要更复杂的设计。

锁的位置对性能也很重要。例如，在push中较早地移动acquisition是正确的：将acquisition的调用移动到第13行之前是可以的。这可能会降低性能，因为这样的话，对malloc的调用也会被序列化。下面的 "使用锁 "一节提供了一些关于在哪里插入获取和释放调用的指南。

## 6.2 Code: Locks

Xv6 has two types of locks: spinlocks and sleep-locks. We’ll start with spinlocks. Xv6 represents a spinlock as a struct spinlock (kernel/spinlock.h:2). The important field in the structure is locked, a word that is zero when the lock is available and non-zero when it is held. Logically, xv6 should acquire a lock by executing code like

Xv6有两种锁：自旋锁和睡眠锁。我们先说说自旋锁。Xv6将自旋锁表示为一个结构spinlock（kernel/spinlock.h:2）。该结构中重要的字段是锁，当锁可用时，这个字为零，当锁被持有时，这个字为非零。从逻辑上讲，xv6应该通过执行类似这样的代码来获取锁。

```cpp
21 void
22 acquire(struct spinlock *lk) // does not work!
23 {
24   for(;;) {
25     if(lk->locked == 0) {
26       lk->locked = 1;
27       break;
28     }
29   }
30 }
```

Unfortunately, this implementation does not guarantee mutual exclusion on a multiprocessor. It could happen that two CPUs simultaneously reach line 25, see that lk->locked is zero, and then both grab the lock by executing line 26. At this point, two different CPUs hold the lock, which violates the mutual exclusion property. What we need is a way to make lines 25 and 26 execute as an atomic (i.e., indivisible) step.

Because locks are widely used, multi-core processors usually provide instructions that implement an atomic version of lines 25 and 26. On the RISC-V this instruction is amoswap r, a.amoswap reads the value at the memory address a, writes the contents of register r to that address, and puts the value it read into r. That is, it swaps the contents of the register and the memory address. It performs this sequence atomically, using special hardware to prevent any other CPU from using the memory address between the read and the write.

Xv6’s acquire (kernel/spinlock.c:22) uses the portable C library call __sync_lock_test_and_set, which boils down to the amoswap instruction; the return value is the old (swapped) contents of lk->locked. The acquire function wraps the swap in a loop, retrying (spinning) until it has acquired the lock. Each iteration swaps one into lk->locked and checks the previous value; if the previous value is zero, then we’ve acquired the lock, and the swap will have set lk->locked to one. If the previous value is one, then some other CPU holds the lock, and the fact that we atomically swapped one into lk->locked didn’t change its value.

不幸的是，这种实现并不能保证多处理器上的相互排斥。可能会出现这样的情况：两个CPU同时到达第25行，看到lk->locked为零，然后都通过执行第26行来抢夺锁。此时，两个不同的CPU持有锁，这就违反了互斥属性。我们需要的是让第25行和第26行作为一个原子（即不可分割）步骤执行的方法。

由于锁被广泛使用，多核处理器通常提供了实现第25和26行原子版的指令。在RISC-V上，这条指令是amoswap r，a.amoswap读取内存地址a处的值，将寄存器r的内容写入该地址，并将其读取的值放入r中，也就是说，它将寄存器的内容和内存地址进行交换。它原子地执行这个序列，使用特殊的硬件来防止任何其他CPU使用读和写之间的内存地址。

Xv6的acquisition(kernel/spinlock.c:22)使用了可移植的C库调用__sync_lock_test_and_set，它归结为amoswap指令；返回值是lk->locked的旧（交换）内容。获取函数在循环中包裹交换，重试（旋转）直到获取了锁。每一次迭代都会将1交换到lk->locked中，并检查之前的值；如果之前的值是0，那么我们已经获得了锁，交换将把lk->locked设置为1。如果之前的值是1，那么其他的CPU持有该锁，我们原子地将1换成lk->locked并没有改变它的值。

Once the lock is acquired, acquire records, for debugging, the CPU that acquired the lock.

The lk->cpu field is protected by the lock and must only be changed while holding the lock.

The function release (kernel/spinlock.c:47) is the opposite of acquire: it clears the lk->cpu field and then releases the lock. Conceptually, the release just requires assigning zero to lk->locked.

The C standard allows compilers to implement an assignment with multiple store instructions, so a C assignment might be non-atomic with respect to concurrent code. Instead, release uses the C library function __sync_lock_release that performs an atomic assignment. This function also boils down to a RISC-V amoswap instruction.

一旦锁被获取，就获取记录，用于调试，获取锁的CPU。

lk->cpu字段受到锁的保护，只有在持有锁的时候才能改变。

函数release(kernel/spinlock.c:47)与acquire相反：它清除lk->cpu字段，然后释放锁。从概念上讲，释放只需要给lk->locked赋零。

C标准允许编译器用多条存储指令来实现赋值，所以C赋值对于并发代码来说可能是非原子性的。相反，release使用C库函数__sync_lock_release执行原子赋值。这个函数也归结为RISC-V的amoswap指令。

## 6.3 Code: Using locks

Xv6 uses locks in many places to avoid race conditions. As described above, kalloc (kernel/kalloc.c:69) and kfree (kernel/kalloc.c:47) form a good example. Try Exercises 1 and 2 to see what happens if those functions omit the locks. You’ll likely find that it’s difficult to trigger incorrect behavior, suggesting that it’s hard to reliably test whether code is free from locking errors and races. It is not unlikely that xv6 has some races.

A hard part about using locks is deciding how many locks to use and which data and invariants each lock should protect. There are a few basic principles. First, any time a variable can be written by one CPU at the same time that another CPU can read or write it, a lock should be used to keep the two operations from overlapping. Second, remember that locks protect invariants: if an invariant involves multiple memory locations, typically all of them need to be protected by a single lock to ensure the invariant is maintained.

The rules above say when locks are necessary but say nothing about when locks are unnecessary, and it is important for efficiency not to lock too much, because locks reduce parallelism.

Xv6在很多地方使用锁来避免竞赛条件。如上所述，kalloc (kernel/kalloc.c:69) 和 kfree (kernel/kalloc.c:47) 就是一个很好的例子。试试练习1和2，看看如果省略这些函数的锁会发生什么。你很可能会发现，很难触发不正确的行为，这说明很难可靠地测试代码是否没有锁定错误和竞赛。xv6有一些竞赛也不是不可能的。

使用锁的一个难点是决定使用多少个锁，以及每个锁应该保护哪些数据和不变量。有几个基本原则。首先，任何时候，当一个CPU可以在另一个CPU读或写变量的同时写入变量时，都应该使用锁来防止这两个操作重叠。第二，记住锁保护不变式：如果一个不变式涉及多个内存位置，通常需要用一个锁保护所有的位置，以确保不变式得到维护。

上面的规则说了什么时候需要锁，但没有说什么时候不需要锁，为了效率，不要锁太多，因为锁会降低并行性。

If parallelism isn’t important, then one could arrange to have only a single thread and not worry about locks. A simple kernel can do this on a multiprocessor by having a single lock that must be acquired on entering the kernel and released on exiting the kernel (though system calls such as pipe reads or wait would pose a problem). Many uniprocessor operating systems have been converted to run on multiprocessors using this approach, sometimes called a “big kernel lock,” but the approach sacrifices parallelism: only one CPU can execute in the kernel at a time. If the kernel does any heavy computation, it would be more efficient to use a larger set of more fine-grained locks, so that the kernel could execute on multiple CPUs simultaneously.

As an example of coarse-grained locking, xv6’s kalloc.c allocator has a single free list protected by a single lock. If multiple processes on different CPUs try to allocate pages at the same time, each will have to wait for its turn by spinning in acquire. Spinning reduces performance, since it’s not useful work. If contention for the lock wasted a significant fraction of CPU time, perhaps performance could be improved by changing the allocator design to have multiple free lists, each with its own lock, to allow truly parallel allocation.

如果并行性并不重要，那么我们可以安排只有一个线程，而不用担心锁的问题。一个简单的内核可以在多处理器上做到这一点，它有一个单一的锁，这个锁必须在进入内核时获得，并在退出内核时释放（尽管诸如管道读取或等待等系统调用会带来问题）。许多单处理器操作系统已经被改造成使用这种方法在多处理器上运行，有时被称为 "大内核锁"，但这种方法牺牲了并行性：内核中一次只能执行一个CPU。如果内核做任何繁重的计算，那么使用一组更大的更细粒度的锁，这样内核可以同时在多个CPU上执行，效率会更高。

作为粗粒度锁的一个例子，xv6的kalloc.c分配器有一个单一的自由列表，由一个锁保护。如果不同CPU上的多个进程试图同时分配页面，每个进程将不得不通过在acquire中旋转来等待轮到自己。旋转会降低性能，因为这不是有用的工作。如果争夺锁浪费了很大一部分CPU时间，也许可以通过改变分配器的设计来提高性能，拥有多个空闲列表，每个列表都有自己的锁，从而实现真正的并行分配。

As an example of fine-grained locking, xv6 has a separate lock for each file, so that processes that manipulate different files can often proceed without waiting for each other’s locks. The file locking scheme could be made even more fine-grained if one wanted to allow processes to simultaneously write different areas of the same file. Ultimately lock granularity decisions need to be driven by performance measurements as well as complexity considerations.

As subsequent chapters explain each part of xv6, they will mention examples of xv6’s use of locks to deal with concurrency. As a preview, Figure 6.3 lists all of the locks in xv6.

作为细粒度锁的一个例子，xv6对每个文件都有一个单独的锁，这样操作不同文件的进程往往可以不等待对方的锁而继续进行。如果想让进程同时写入同一文件的不同区域，文件锁方案可以做得更细。最终，锁的粒度决定需要由性能测量以及复杂性考虑来驱动。

在后续的章节解释xv6的每个部分时，会提到xv6使用锁来处理并发性的例子。作为预览，图6.3列出了xv6中所有的锁

## 6.4 Deadlock and lock ordering

![](../img/Figure6.3.png)

If a code path through the kernel must hold several locks at the same time, it is important that all code paths acquire those locks in the same order. If they don’t, there is a risk of deadlock. Let’s say two code paths in xv6 need locks A and B, but code path 1 acquires locks in the order A then B, and the other path acquires them in the order B then A. Suppose thread T1 executes code path 1 and acquires lock A, and thread T2 executes code path 2 and acquires lock B. Next T1 will try to acquire lock B, and T2 will try to acquire lock A. Both acquires will block indefinitely, because in both cases the other thread holds the needed lock, and won’t release it until its acquire returns. To avoid such deadlocks, all code paths must acquire locks in the same order. The need for a global lock acquisition order means that locks are effectively part of each function’s specification: callers must invoke functions in a way that causes locks to be acquired in the agreed-on order.

如果一个通过内核的代码路径必须同时持有几个锁，那么所有的代码路径以同样的顺序获取这些锁是很重要的。如果它们不这样做，就会有死锁的风险。假设线程T1执行代码路径1并获取锁A，线程T2执行代码路径2并获取锁B，接下来T1会尝试获取锁B，T2会尝试获取锁A，这两次获取都会无限期阻塞，因为在这两种情况下，另一个线程都持有所需的锁，直到其获取返回时才会释放。为了避免这种死锁，所有的代码路径必须以相同的顺序获取锁。对全局锁获取顺序的需求意味着锁实际上是每个函数规范的一部分：调用者必须以导致按约定顺序获取锁的方式调用函数。

Xv6 has many lock-order chains of length two involving per-process locks (the lock in each struct proc) due to the way that sleep works (see Chapter 7). For example, consoleintr (kernel/console.c:138) is the interrupt routine which handles typed characters. When a newline arrives, any process that is waiting for console input should be woken up. To do this, consoleintr holds cons.lock while calling wakeup, which acquires the waiting process’s lock in order to wake it up. In consequence, the global deadlock-avoiding lock order includes the rule that cons.lock must be acquired before any process lock. The file-system code contains xv6’s longest lock chains.

For example, creating a file requires simultaneously holding a lock on the directory, a lock on the new file’s inode, a lock on a disk block buffer, the disk driver’s vdisk_lock, and the calling process’s p->lock. To avoid deadlock, file-system code always acquires locks in the order mentioned in the previous sentence.

由于睡眠的工作方式，Xv6有许多长度为2的锁序链，涉及到每个进程的锁（每个struct proc中的锁）（见第7章）。例如，consoleintr(kernel/console.c:138)是处理类型化字符的中断例程。当一个新行到达时，任何正在等待控制台输入的进程都应该被唤醒。要做到这一点，consoleintr在调用wakeup时持有cons.lock，wakeup获取等待进程的锁以唤醒它。因此，全局避免死锁的锁顺序包括了cons.锁必须在任何进程锁之前获取的规则。文件系统代码包含xv6最长的锁链。

例如，创建一个文件需要同时持有目录上的锁、新文件的inode上的锁、磁盘块缓冲区上的锁、磁盘驱动器的vdisk_lock和调用进程的p->lock。为了避免死锁，文件系统代码总是按照上一句提到的顺序获取锁。

Honoring a global deadlock-avoiding order can be surprisingly difficult. Sometimes the lock order conflicts with logical program structure, e.g., perhaps code module M1 calls module M2, but the lock order requires that a lock in M2 be acquired before a lock in M1. Sometimes the identities of locks aren’t known in advance, perhaps because one lock must be held in order to discover the identity of the lock to be acquired next. This kind of situation arises in the file system as it looks up successive components in a path name, and in the code for wait and exit as they search the table of processes looking for child processes. Finally, the danger of deadlock is often a constraint on how fine-grained one can make a locking scheme, since more locks often means more opportunity for deadlock. The need to avoid deadlock is often a major factor in kernel implementation.

遵守全局避免死锁的顺序可能会出乎意料的困难。有时锁顺序与逻辑程序结构发生冲突，例如，也许代码模块M1调用模块M2，但锁顺序要求在M1的锁之前获得M2的锁。有时锁的身份并不是事先知道的，也许是因为必须持有一个锁才能发现接下来要获取的锁的身份。这种情况出现在文件系统中，因为它在路径名中查找连续的组件，也出现在wait和exit的代码中，因为它们搜索进程表寻找子进程。最后，死锁的危险往往制约着人们对锁方案的细化程度，因为更多的锁往往意味着更多的死锁机会。避免死锁的需求往往是内核实现的一个主要因素。

## 6.5 Locks and interrupt handlers

Some xv6 spinlocks protect data that is used by both threads and interrupt handlers. For example, the clockintr timer interrupt handler might increment ticks (kernel/trap.c:163) at about the same time that a kernel thread reads ticks in sys_sleep (kernel/sysproc.c:64). The lock tickslock serializes the two accesses.

The interaction of spinlocks and interrupts raises a potential danger. Suppose sys_sleep holds tickslock, and its CPU is interrupted by a timer interrupt. clockintr would try to acquire tickslock, see it was held, and wait for it to be released. In this situation, tickslock will never be released: only sys_sleep can release it, but sys_sleep will not continue running until clockintr returns. So the CPU will deadlock, and any code that needs either lock will also freeze.

一些xv6自旋锁保护线程和中断处理程序同时使用的数据。例如，clockintr定时器中断处理程序可能会在内核线程读取sys_sleep (kernel/sysproc.c:64)中的ticks的同时，递增ticks (kernel/trap.c:163)。锁tickslock将这两次访问序列化。

自旋锁和中断的相互作用带来了一个潜在的危险。假设sys_sleep持有tickslock，而它的CPU被一个定时器中断打断，clockintr将尝试获取tickslock，看到它被持有，并等待它被释放。在这种情况下，tickslock永远不会被释放：只有sys_sleep可以释放它，但sys_sleep不会继续运行，直到clockintr返回。所以CPU会死锁，任何需要任一锁的代码也会冻结。

To avoid this situation, if a spinlock is used by an interrupt handler, a CPU must never hold that lock with interrupts enabled. Xv6 is more conservative: when a CPU acquires any lock, xv6 always disables interrupts on that CPU. Interrupts may still occur on other CPUs, so an interrupt’s acquire can wait for a thread to release a spinlock; just not on the same CPU.

xv6 re-enables interrupts when a CPU holds no spinlocks; it must do a little book-keeping to cope with nested critical sections. acquire calls push_off (kernel/spinlock.c:89) and release calls pop_off (kernel/spinlock.c:100) to track the nesting level of locks on the current CPU. When that count reaches zero, pop_off restores the interrupt enable state that existed at the start of the outermost critical section. The intr_off and intr_on functions execute RISC-V instructions to disable and enable interrupts, respectively.

It is important that acquire call push_off strictly before setting lk->locked (kernel/spinlock.c:28). If the two were reversed, there would be a brief window when the lock was held with interrupts enabled, and an unfortunately timed interrupt would deadlock the system. Similarly, it is important that release call pop_off only after releasing the lock (kernel/spinlock.c:66)

为了避免这种情况，如果一个中断处理程序使用了自旋锁，CPU决不能在启用中断的情况下持有该锁。Xv6比较保守：当一个CPU获取任何锁时，xv6总是禁用该CPU上的中断。中断仍然可能发生在其他CPU上，所以一个中断的获取可以等待一个线程释放自旋锁；只是不在同一个CPU上。

当一个CPU没有持有自旋锁时，xv6会重新启用中断；它必须做一些记账工作来应对嵌套的关键部分。acquire调用push_off(kernel/spinlock.c:89)和release调用pop_off(kernel/spinlock.c:100)来跟踪当前CPU上锁的嵌套级别。当该计数达到零时，pop_off会恢复最外层关键部分开始时存在的中断启用状态。intr_off和intr_on函数分别执行RISC-V指令来禁用和启用中断。

重要的是，获取严格地在设置lk->locked之前调用push_off（kernel/spinlock.c:28）。如果二者反过来，那么在启用中断的情况下，会有一个短暂的锁保持窗口，不幸的是，一个定时的中断会使系统死锁。同样，释放锁后才调用pop_off也很重要(kernel/spinlock.c:66)

## 6.6 Instruction and memory ordering

It is natural to think of programs executing in the order in which source code statements appear.

Many compilers and CPUs, however, execute code out of order to achieve higher performance. If an instruction takes many cycles to complete, a CPU may issue the instruction early so that it can overlap with other instructions and avoid CPU stalls. For example, a CPU may notice that in a serial sequence of instructions A and B are not dependent on each other. The CPU may start instruction B first, either because its inputs are ready before A’s inputs, or in order to overlap execution of A and B. A compiler may perform a similar re-ordering by emitting instructions for one statement before the instructions for a statement that precedes it in the source.

Compilers and CPUs follow rules when they re-order to ensure that they don’t change the results of correctly-written serial code. However, the rules do allow re-ordering that changes the results of concurrent code, and can easily lead to incorrect behavior on multiprocessors [2, 3]. The CPU’s ordering rules are called the memory model.

For example, in this code for push, it would be a disaster if the compiler or CPU moved the store corresponding to line 4 to a point after the release on line 6:

人们很自然地认为程序是按照源代码语句出现的顺序来执行的。

然而，许多编译器和CPU为了获得更高的性能，会不按顺序执行代码。如果一条指令需要很多周期才能完成，CPU可能会提前发出该指令，以便与其他指令重叠，避免CPU停顿。例如，CPU可能会注意到在一个串行序列中，指令A和B互不依赖。CPU可能会先启动指令B，这是因为它的输入比A的输入先准备好，或者是为了使A和B的执行重叠。 编译器可能会执行类似的重新排序，在源码中先于前面的一条语句的指令发出一条语句的指令。

编译器和CPU在重新排序时遵循规则，以确保不会改变正确编写的串行代码的结果。然而，规则确实允许改变并发代码的结果的重新排序，并且很容易导致多处理器上的错误行为[2，3]。CPU的排序规则称为内存模型。

例如，在这段推送的代码中，如果编译器或CPU将第4行对应的存储移到第6行释放后的点上，那将是一场灾难

```cpp
1 l = malloc(sizeof *l);
2 l->data = data;
3 acquire(&listlock);
4 l->next = list;
5 list = l;
6 release(&listlock);
```

If such a re-ordering occurred, there would be a window during which another CPU could acquire the lock and observe the updated list, but see an uninitialized list->next.

To tell the hardware and compiler not to perform such re-orderings, xv6 uses __sync_synchronize() in both acquire (kernel/spinlock.c:22) and release (kernel/spinlock.c:47). __sync_synchronize() is a memory barrier: it tells the compiler and CPU to not reorder loads or stores across the barrier.

The barriers in xv6’s acquire and release force order in almost all cases where it matters, since xv6 uses locks around accesses to shared data. Chapter 9 discusses a few exceptions.

如果发生这样的重新排序，就会有一个窗口，在这个窗口中，另一个CPU可以获取锁并观察更新后的列表，但看到的是一个未初始化的list->next。

为了告诉硬件和编译器不要执行这样的重新排序，xv6在获取(kernel/spinlock.c:22)和释放(kernel/spinlock.c:47)中都使用了__sync_synchronize()。__sync_synchronize()是一个内存屏障：它告诉编译器和CPU不要在屏障上重新排序加载或存储。

xv6的获取和释放障碍几乎在所有重要的情况下都会强制排序，因为xv6在访问共享数据的周围使用锁。第9章讨论了一些例外情况。

## 6.7 Sleep locks

Sometimes xv6 needs to hold a lock for a long time. For example, the file system (Chapter 8) keeps a file locked while reading and writing its content on the disk, and these disk operations can take tens of milliseconds. Holding a spinlock that long would lead to waste if another process wanted to acquire it, since the acquiring process would waste CPU for a long time while spinning. Another drawback of spinlocks is that a process cannot yield the CPU while retaining a spinlock; we’d like to do this so that other processes can use the CPU while the process with the lock waits for the disk.

Yielding while holding a spinlock is illegal because it might lead to deadlock if a second thread then tried to acquire the spinlock; since acquire doesn’t yield the CPU, the second thread’s spinning might prevent the first thread from running and releasing the lock. Yielding while holding a lock would also violate the requirement that interrupts must be off while a spinlock is held. Thus we’d like a type of lock that yields the CPU while waiting to acquire, and allows yields (and interrupts) while the lock is held.

有时xv6需要长时间保持一个锁。例如，文件系统（第8章）在磁盘上读写文件内容时，会保持一个文件的锁定，这些磁盘操作可能需要几十毫秒。如果另一个进程想获取一个自旋锁，那么保持那么长的时间会导致浪费，因为获取进程在旋转的同时会浪费CPU很长时间。自旋锁的另一个缺点是，一个进程在保留自旋锁的同时不能屈服CPU；我们希望做到这一点，以便在拥有自旋锁的进程等待磁盘时，其他进程可以使用CPU。

在持有自旋锁时让步是非法的，因为如果第二个线程再试图获取自旋锁，可能会导致死锁；由于获取并不能让步CPU，第二个线程的旋转可能会阻止第一个线程运行和释放锁。在持有锁的同时屈服也会违反在持有自旋锁时中断必须关闭的要求。因此，我们希望有一种锁，在等待获取时让步CPU，在锁被持有时允许让步（和中断）。

Xv6 provides such locks in the form of sleep-locks. acquiresleep (kernel/sleeplock.c:22) yields the CPU while waiting, using techniques that will be explained in Chapter 7. At a high level, a sleep-lock has a locked field that is protected by a spinlock, and acquiresleep ’s call to sleep atomically yields the CPU and releases the spinlock. The result is that other threads can execute while acquiresleep waits.

Because sleep-locks leave interrupts enabled, they cannot be used in interrupt handlers. Because acquiresleep may yield the CPU, sleep-locks cannot be used inside spinlock critical sections (though spinlocks can be used inside sleep-lock critical sections).

Spin-locks are best suited to short critical sections, since waiting for them wastes CPU time; sleep-locks work well for lengthy operations.

Xv6以睡眠锁的形式提供了这样的锁。 acquiresleep (kernel/sleeplock.c:22)在等待的过程中产生CPU，使用的技术将在第7章解释。在高层次上，sleep-lock有一个由spinlock保护的锁定字段，acquiresleep's调用sleep原子性地让渡CPU并释放spinlock。结果就是在acquireleep等待的时候，其他线程可以执行。

因为睡眠锁使中断处于启用状态，所以不能在中断处理程序中使用。因为acquiresleep可能会让出CPU，所以睡眠锁不能在spinlock关键部分内使用（尽管spinlocks可以在睡眠锁关键部分内使用）。

自旋锁最适合于短的关键部分，因为等待它们会浪费CPU时间；睡眠锁对长的操作很有效。

## 6.8 Real world

Programming with locks remains challenging despite years of research into concurrency primitives and parallelism. It is often best to conceal locks within higher-level constructs like synchronized queues, although xv6 does not do this. If you program with locks, it is wise to use a tool that attempts to identify race conditions, because it is easy to miss an invariant that requires a lock.

Most operating systems support POSIX threads (Pthreads), which allow a user process to have several threads running concurrently on different CPUs. Pthreads has support for user-level locks, barriers, etc. Supporting Pthreads requires support from the operating system. For example, it should be the case that if one pthread blocks in a system call, another pthread of the same process should be able to run on that CPU. As another example, if a pthread changes its process’s address space (e.g., maps or unmaps memory), the kernel must arrange that other CPUs that run threads of the same process update their hardware page tables to reflect the change in the address space.

尽管对并发基元和并行进行了多年的研究，但使用锁进行编程仍然具有挑战性。通常最好是将锁隐藏在更高级别的构造中，比如同步队列，尽管xv6没有这样做。如果您使用锁编程，明智的做法是使用一个试图识别竞赛条件的工具，因为很容易错过一个需要锁的不变式。

大多数操作系统都支持POSIX线程（Pthreads），它允许一个用户进程在不同的CPU上有多个线程同时运行。Pthreads对用户级锁、障碍等都有支持。支持Pthreads需要操作系统的支持。例如，应该是如果一个pthread在系统调用中阻塞，同一进程的另一个pthread应该可以在该CPU上运行。又比如，如果一个pthread改变了它的进程的地址空间（比如映射或取消映射内存），内核必须安排运行同一进程线程的其他CPU更新它们的硬件页表以反映地址空间的变化。

It is possible to implement locks without atomic instructions [8], but it is expensive, and most operating systems use atomic instructions.

Locks can be expensive if many CPUs try to acquire the same lock at the same time. If one CPU has a lock cached in its local cache, and another CPU must acquire the lock, then the atomic instruction to update the cache line that holds the lock must move the line from the one CPU’s cache to the other CPU’s cache, and perhaps invalidate any other copies of the cache line. Fetching a cache line from another CPU’s cache can be orders of magnitude more expensive than fetching a line from a local cache.

To avoid the expenses associated with locks, many operating systems use lock-free data structures and algorithms [5, 10]. For example, it is possible to implement a linked list like the one in the beginning of the chapter that requires no locks during list searches, and one atomic instruction to insert an item in a list. Lock-free programming is more complicated, however, than programming locks; for example, one must worry about instruction and memory reordering. Programming with locks is already hard, so xv6 avoids the additional complexity of lock-free programming.

可以在没有原子指令的情况下实现锁[8]，但成本很高，大多数操作系统都使用原子指令。

如果许多CPU试图在同一时间获取同一个锁，那么锁的成本会很高。如果一个CPU的本地缓存中有一个锁，而另一个CPU必须获取该锁，那么更新持有该锁的缓存行的原子指令必须将该行从一个CPU的缓存中移到另一个CPU的缓存中，并且可能使缓存行的任何其他副本无效。从另一个CPU的缓存中获取缓存行的费用可能比从本地缓存中获取行的费用高一个数量级。

为了避免与锁相关的费用，许多操作系统使用无锁数据结构和算法[5，10]。例如，可以实现像本章开头的链接列表，在列表搜索过程中不需要锁，只需要一条原子指令就可以在列表中插入一个项目。不过，无锁编程比有锁编程更复杂，例如，必须担心指令和内存的重新排序问题。用锁编程已经很难了，所以xv6避免了无锁编程的额外复杂性。

## 6.9 Exercises

1- Comment out the calls to acquire and release in kalloc (kernel/kalloc.c:69). This seems like it should cause problems for kernel code that calls kalloc; what symptoms do you expect to see? When you run xv6, do you see these symptoms? How about when running usertests? If you don’t see a problem, why not? See if you can provoke a problem by inserting dummy loops into the critical section of kalloc.

2- Suppose that you instead commented out the locking in kfree (after restoring locking in kalloc). What might now go wrong? Is lack of locks in kfree less harmful than in kalloc?

3- If two CPUs call kalloc at the same time, one will have to wait for the other, which is bad for performance. Modify kalloc.c to have more parallelism, so that simultaneous calls to kalloc from different CPUs can proceed without waiting for each other.

4- Write a parallel program using POSIX threads, which is supported on most operating systems. For example, implement a parallel hash table and measure if the number of puts/gets scales with increasing number of cores.

5- Implement a subset of Pthreads in xv6. That is, implement a user-level thread library so that a user process can have more than 1 thread and arrange that these threads can run in parallel on different CPUs. Come up with a design that correctly handles a thread making a blocking system call and changing its shared address space.

1- 注释掉kalloc中的获取和释放调用(kernel/kalloc.c:69)。这似乎会给调用kalloc的内核代码带来问题；你希望看到什么症状？当你运行xv6时，你会看到这些症状吗？运行usertests的时候呢？如果你没有看到问题，为什么不呢？看看你是否可以通过在kalloc的关键部分插入虚拟循环来引发问题。

2- 假设你把kfree中的锁定注释掉了（在恢复kalloc中的锁定后）。现在可能出了什么问题？kfree中缺少锁是否比kalloc中的危害小？

3- 如果两个CPU同时调用kalloc，其中一个CPU必须等待另一个CPU，这对性能不利。修改kalloc.c，使其具有更多的并行性，这样不同CPU对kalloc的同时调用就可以进行，而不需要等待对方。

4-使用POSIX线程编写并行程序，大多数操作系统都支持POSIX线程。例如，实现一个并行哈希表，并测量put/get的数量是否随着核数的增加而缩放。

5- 在xv6中实现Pthreads的一个子集。即实现用户级线程库，使一个用户进程可以有1个以上的线程，并安排这些线程可以在不同的CPU上并行运行。提出一个设计，正确处理线程进行阻塞系统调用和改变其共享地址空间的问题。