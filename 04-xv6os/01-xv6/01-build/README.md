# xv6-riscv

本專案來源為 MIT 6.828 課程之 github ，網址如下：

* https://github.com/mit-pdos/xv6-riscv

原本該專案是在 Linux 下編譯的，但是我把它改成在 Windows 用 (windows + git bash + RISC-V gcc + qemu) 編譯。

方法是將 [mkfs.c](https://github.com/mit-pdos/xv6-riscv/tree/riscv/mkfs/mkfs.c) 修改為標準 C 語言，就可以編譯成功了。

## 在 Windows 下編譯本專案：

請先下載:

* [git-bash](https://git-scm.com/download/win)
* [FreedomStudio](https://www.sifive.com/software)

解開 FreedomStudio 壓縮檔後，將 windows 的 PATH 加入 `riscv64-unknown-elf-gcc/bin` and `riscv-qemu/bin` 這兩個路徑 (我電腦中的路徑如下)。

```
D:\install\FreedomStudio-2020-06-3-win64\SiFive\riscv64-unknown-elf-gcc-8.3.0-2020.04.1\bin

D:\install\FreedomStudio-2020-06-3-win64\SiFive\riscv-qemu-4.2.0-2020.04.0\bin
```

然後請用 git-bash 編譯建置你的專案。(我用 Visual Studio Code 中的終端機)


## 在 Windows 建置執行

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/co2os/code/04
$ make qemu
qemu-system-riscv64 -machine virt -bios none -kernel kernel/kernel -m 256M -smp 3 -nographic -drive file=fs.img,if=none,format=raw,id=x0 -device virtio-blk-device,drive=x0,bus=virtio-mmio-bus.0

xv6 kernel is booting

hart 2 starting
hart 1 starting
init: starting sh
$ ls
.              1 1 1024
..             1 1 1024
README         2 2 2102
cat            2 3 23944
echo           2 4 22776
forktest       2 5 13112
grep           2 6 27256
init           2 7 23904
kill           2 8 22744
ln             2 9 22728
ls             2 10 26152
mkdir          2 11 22864
rm             2 12 22848
sh             2 13 41800
stressfs       2 14 23776
usertests      2 15 151184
grind          2 16 38008
wc             2 17 25040
zombie         2 18 22272
console        3 19 0
$ 
// 按下 Ctrl-A-C 會進入 QEMU, 若按下 Ctrl-A-X 會直接離開
$ QEMU 4.2.0 monitor - type 'help' for more information
(qemu) quit
```


## Linux 中的建置方法

請按照下列指引去做：

* https://github.com/mit-pdos/xv6-riscv/

## 授權 License

本專案來源如下

* https://github.com/mit-pdos/xv6-riscv 

請遵照下列授權使用之

* https://github.com/mit-pdos/xv6-riscv/blob/riscv/LICENSE

