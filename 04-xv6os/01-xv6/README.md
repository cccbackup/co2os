# xv6 作業系統使用

## 閱讀

* [Chapter 1 -- Operating system interfaces](../book/01n-OsInterface.md)
* [Chapter 2 -- Operating system organization](../book/02n-OsOrganization.md)

## 實作

* [xv6 的建置與執行](./01-build)
* [xv6 的 api 呼叫](./02-api)
* [xv6 的 syscall 系統呼叫](./03-syscall)


## 心得

* [xv6 的整體架構](./xv6overview.md)

