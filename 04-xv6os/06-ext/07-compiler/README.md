# xv6 compiler

## shecc

* https://www.facebook.com/groups/system.software2020/permalink/526772168256987/

jserv: 2020 年為了教學需求，我發展一套精簡的 C 語言編譯器，起初只支援 Arm 32 位元架構 (更精準來說是 ARMv7-A)，最近開始移植到 RISC-V 32 位元架構 (RV32IM 指令集)，由 鄭育丞 同學接手進行。
歡迎關注目前的開發進度。
