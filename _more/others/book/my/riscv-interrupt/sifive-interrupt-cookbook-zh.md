# SiFive Interrupt Cookbook (SiFive 的 RISC-V 中斷手冊)

Version 1.0 © SiFive, Inc.

版本1.0©SiFive, Inc.

## Proprietary Notice (智財權聲明)

Copyright © 2019, SiFive Inc. All rights reserved.

Information in this document is provided as is, with all faults.

SiFive expressly disclaims all warranties, representations, and conditions of any kind, whether express or implied, including, but not limited to, the implied warranties or conditions of merchantability, fitness for a particular purpose and non-infringement.

SiFive does not assume any liability rising out of the application or use of any product or circuit, and specifically disclaims any and all liability, including without limitation indirect, incidental, special, exemplary, or consequential damages.

SiFive reserves the right to make changes without further notice to any products herein.

版权所有©2019保留所有权利。

本文档中的信息是按原样提供的，但都是错误的。

SiFive明确否认任何形式的明示或默示的保证、陈述和条件，包括但不限于关于适销性、适用于特定目的和非侵权的默示保证或条件。

SiFive不承担因使用或使用任何产品或电路而产生的任何责任，并明确否认任何和所有责任，包括但不限于间接、偶然、特殊、惩戒性或间接损害。

本公司保留在不另行通知任何产品的情况下对本产品进行修改的权利。

## Release Information (发布信息)

Version | Date            | Changes
--------|-----------------|----------------
V1.0    | December 5, 2019 | Initial release

版本 | 日期 | 更改
--------|-----------------|----------------
V1.0 | 2019年12月5日 | 首次发布

# Chapter 1 SiFive Interrupt Cookbook

## 1.1 Introduction

Embedded systems rely heavily on handling interrupts which are asynchronous events designed to be managed by the CPU. SiFive core designs include options for a simple timer and software interrupt generator, a fully featured local interrupt controller, and optionally, a global interrupt controller. This document describes the features and configuration details of the available interrupt configurations offered by SiFive.

嵌入式系统严重依赖于处理中断，而中断是被设计为由CPU管理的异步事件。SiFive核心设计包括一个简单的定时器和软件中断生成器、一个功能齐全的本地中断控制器和一个可选的全局中断控制器。本文档描述了SiFive提供的可用中断配置的特性和配置细节。

## 1.1.1 Terminology

## Hardware Threads (HART) in SiFive Designs

As of this writing, all SiFive designed CPUs contain a single HART per core. Future products from SiFive may implement multi-hart designs. For simplicity, HART and CPU may be used interchangeably in this document as it relates to interrupts.

在撰写本文时，所有SiFive设计的cpu每个核都包含一个HART。SiFive 公司未来的产品可能会实现多hart设计。为简单起见，在本文档中，HART和CPU可以互换使用，因为它们与中断有关。

RISC-V Exception, Interrupt, Trap The following terminology comes directly from From The RISC-V Instruction Set Manual Volume I: User-Level ISA Document Version 2.2:

* We use the term exception to refer to an unusual condition occurring at run time associated with an instruction in the current RISC-V thread.
* We use the term trap to refer to the synchronous transfer of control to a trap handler caused by an exceptional condition occurring within a RISC-V thread. Trap handlers usually execute in a more privileged environment.
* We use the term interrupt to refer to an external event that occurs asynchronously to the current RISC-V thread. When an interrupt that must be serviced occurs, some instruction is selected to receive an interrupt exception and subsequently experiences a trap.

> Note : Our use of exception and trap matches that in the IEEE-754 floating-point standard.


以下术语直接来自于RISC-V指令集手册卷I:用户级ISA文档版本2.2:

* 我们使用术语异常来指在运行时发生的与当前RISC-V线程中的一条指令相关的异常情况。
* 我们使用术语trap来指在一个RISC-V线程中发生异常情况而导致的控制同步转移到一个trap处理程序。陷阱处理程序通常在更有特权的环境中执行。
* 我们使用术语中断指的是一个外部事件，异步发生在当前RISC-V线程。当一个必须被服务的中断发生时，一些指令被选择来接收一个中断异常，然后经历一个陷阱。

> 注意:我们对异常和陷阱的使用与IEEE-754浮点标准相匹配。

## Exception Example

The address of the data during a load instruction is not aligned correctly, so upon execution of this load instruction, the CPU will enter an exception handler and a load address misaligned exception code will appear in the mcause register as a result. In the exception handler, software will then need to determine the next course of action, since the misaligned load is not allowed by the design. See The RISC-V Instruction Set Manual Volume II: Privileged Architecture Privileged Architecture Version 1.10 for a detailed description of all available exception codes.

加载指令期间的数据地址没有正确对齐，因此在执行此加载指令时，CPU将输入一个异常处理程序，结果加载地址misaligned异常代码将出现在mcause寄存器中。在异常处理程序中，软件将需要确定下一个行动过程，因为设计不允许未对齐的负载。有关所有可用异常代码的详细描述，请参阅RISC-V指令集手册卷II:特权体系结构特权体系结构版本1.10。

## Trap Example

A particular CPU design contains three privilege modes: Machine, Supervisor, and User. Each privilege mode has its own user registers, control and status registers (CSRs) for trap handling, and stack area dedicated to them. While operating in User mode, a context switch is required to handle an event in Supervisor mode. The software sets up the system for a context switch, and then an ECALL instruction is executed which synchronously switches control to the environmentcall-from-User mode exception handler.

一个特定的CPU设计包含三种特权模式:机器、管理员和用户。每种特权模式都有自己的用户寄存器、用于陷阱处理的控制和状态寄存器(CSRs)以及专用于它们的堆栈区域。在用户模式下操作时，需要上下文切换来处理管理模式下的事件。该软件建立了一个上下文切换系统，然后执行ECALL指令同步地将控制切换到环境调用用户模式的异常处理程序。

## Interrupt Example

A timer interrupt is required to trigger an event in the future, so a CPU writes its own mtimecmp register with a value of mtime + ticks, where ticks is some number of clock cycles in the future.

Since mtime increments continually, it is independent of any instructions being executed by the CPU. At some point later, mtimecmp matches mtime, and the CPU enters an interrupt handler for the timer event.

Since an interrupt may occur anytime, and they are typically not part of the instruction execution sequence, they are asynchronous by nature.

Since an exception occurs as a result of executing an instruction, they are synchronous by nature.

一个定时器中断需要在未来触发一个事件，所以一个CPU写它自己的mtimecmp寄存器，值为mtime + ticks，其中ticks是未来的某个时钟周期数。

由于时时是连续递增的，所以它独立于CPU正在执行的任何指令。在稍后的某个时刻，mtimecmp匹配时时，CPU为定时器事件进入一个中断处理程序。

由于中断可以随时发生，而且中断通常不是指令执行序列的一部分，因此它们本质上是异步的。

由于异常是执行指令的结果，所以它们本质上是同步的。

## 1.2 Local and Global Interrupt Concepts

## 1.2.1 Local Interrupt Controllers

There are two available options on SiFive designs that provide low latency interrupts to the CPU.

First, the Core Local Interrupter (CLINT) offers a compact design with a fixed priority scheme, with preemption support for interrupts from higher privilege levels only. The primary purpose of the CLINT is to serve as a simple CPU interrrupter for software and timer interrupts, since it does not control other local interrupts wired directly to the CPU.

A second option is the Core Local Interrupt Controller (CLIC), which is a fully featured local interrupt controller with configurations that support programmable interrupt levels and priorities.

The CLIC also supports nested interrupts (preemption) within a given privilege level, based on the interrupt level and priority configuration.

Both the CLINT and CLIC integrate registers mtime and mtimecmp to configure timer interrupts, and msip to trigger software interrupts. Additionally, both the CLINT and the CLIC run at the core clock frequency.

在SiFive设计中有两个可用选项，可以为CPU提供低延迟的中断。

首先，核心本地中断器(CLINT)提供了一种紧凑的设计，具有固定优先级方案，仅对来自更高特权级别的中断提供抢占支持。CLINT的主要目的是作为一个简单的CPU间断器为软件和定时器中断，因为它不控制其他本地中断直接连接到CPU。

第二个选择是核心本地中断控制器(CLIC)，这是一个功能齐全的本地中断控制器，配置支持可编程中断级别和优先级。

CLIC还支持基于中断级别和优先级配置的特定特权级别内嵌套中断(抢占)。

CLINT和CLIC集成寄存器时时和mtimecmp来配置计时器中断，msip来触发软件中断。此外，CLINT和CLIC运行在核心时钟频率。

## 1.2.2 Global Interrupt Controller

The global interrupt controller is termed the Platform Local Interrupt Controller (PLIC). The PLIC provides system level flexibility for dispatching interrupts to a single CPU or multiple CPUs in the system. Global interrupts that route through the PLIC arrive at the CPU through a single interrupt connection with a dedicated interrupt ID. Each global interrupt has a programmable priority register available in the PLIC memory map. There is also a system level programmable threshold register which can be used to mask all interrupts below a certain level. The PLIC runs off a different clock than local interrupt controllers, which is typically an integer divided ratio from the core clock.

全局中断控制器称为平台本地中断控制器(PLIC)。PLIC提供了系统级的灵活性，为调度中断到一个单一的CPU或系统中的多个CPU。通过PLIC路由的全局中断通过一个具有专用中断ID的单个中断连接到达CPU。每个全局中断在PLIC内存映射中有一个可编程优先级寄存器。还有一个系统电平可编程门限寄存器，可以用来屏蔽低于某一电平的所有中断。PLIC运行一个不同的时钟比本地中断控制器，这是典型的整数分割比率从核心时钟。

## 1.2.3 Built-in, Predefined Exceptions

RISC-V Instruction Set Architecture describes many different types of system exceptions, however none are defined to have a reserved location in the user defined interrupt vector table. One mode of operation of the local interrupt controller is called direct mode, where it does not use a vector table. In this mode, it’s up to software to determine the source of the exception or interrupt, and act accordingly. There are also variations of vectored mode of operation that will be discussed in the specific sections for the CLINT and CLIC.

指令集架构描述了许多不同类型的系统异常，但是在用户定义的中断向量表中没有一个被定义为保留位置。本地中断控制器的一种操作模式称为直接模式，它不使用向量表。在这种模式下，由软件决定异常或中断的来源，并相应地采取行动。在CLINT和CLIC的具体章节中，还将讨论操作的矢量模式的变化。

## 1.2.4 Interrupt Detection

All interrupts on SiFive designs implement level-high sensitive interrupt triggering. This is not configurable, but some custom implementations may decide to include device specific glue logic to convert interrupt sources from a rising or falling edge into a level high sensitive signal.

所有的中断在SiFive设计实现高电平敏感的中断触发。这是不可配置的，但一些自定义实现可能决定包含设备特定的粘合逻辑，以将中断源从上升或下降沿转换为高灵敏度信号。

# Chapter 2 Interrupt Configuration Registers

There are several Control and Status Registers (CSRs) within the CPU which are used for configuring interrupts. CSRs can only be read or written locally by executing variations of csrr and csrw instructions, and are not visible to other CPUs.

在CPU中有几个用于配置中断的控制和状态寄存器(CSRs)。csr只能通过执行csrr和csrw指令的变体在本地读取或写入，但其他 CPU 無法存取這些状态寄存器(CSRs)。

## 2.1 Interrupt Control and Status Registers (CSRs)

There are interrupt related CSRs contained in the CPU, as well as memory mapped configuration registers in the respective interrupt controllers. Both are used to configure and properly route interrupts to a CPU. Here we will discuss the Machine mode interrupt CSRs for the CPU only. Many Machine mode interrupt CSRs may have Supervisor or User mode equivalents.

Refer to The RISC-V Instruction Set Manual Volume II: Privileged Architecture Privileged Architecture Version 1.10 for the full list.

* mstatus — Status register containing interrupt enables for all privilege modes, previous privilege mode, and other privilege level settings.
* mcause — Status register which indicates whether an exception or interrupt occurred, along with a code to distinguish details of each type.
* mie — Interrupt enable register for local interrupts when using CLINT modes of operation. In CLIC modes, this is hardwired to 0 and interrupt enables are handled using clicintie[i] memory mapped registers.
* mip — Interrupt pending register for local interrupts when using CLINT modes of operation. In CLIC modes, this is hardwired to 0 and pending interrupts are handled using clicintip[i] memory mapped registers.
* mtvec — Machine Trap Vector register which holds the base address of the interrupt vector table, as well as the interrupt mode configuration (direct or vectored) for CLINT and CLIC controllers. All synchronous exceptions also use mtvec as the base address for exception handling in all CLINT and CLIC modes.
* mtvt — Used only in CLIC modes of operation. Contains the base address of the interrupt vector table for selectively vectored interrupt in CLIC direct mode, and for all vectored interrupts in CLIC vectored mode. This register does not exist on designs with a CLINT

有中断相关的csr包含在CPU，以及内存映射配置寄存器在各自的中断控制器。两者都用于配置中断并将其正确路由到CPU。在这里，我们将只讨论CPU的机器模式中断csr。许多机器模式中断csr可能有监督者或用户模式的对等物。

完整列表请参考RISC-V指令集手册卷II:特权体系结构特权体系结构版本1.10。

* mstatus -状态寄存器包含中断，允许所有特权模式，前特权模式，和其他特权级别设置。
* mcause -状态寄存器，指示是否发生异常或中断，连同一个代码来区分每种类型的细节。

当使用CLINT操作模式时，允许为本地中断注册。在CLIC模式中，这是硬连接到0和中断启用被处理使用clicintie[i]内存映射寄存器。

* mip -中断挂起注册本地中断时使用CLINT模式的操作。在CLIC模式中，这是硬连接到0，挂起的中断使用clicintip[i]内存映射寄存器处理。
* mtvec -机器陷阱向量寄存器，它保存中断向量表的基址，以及CLINT和环控制器的中断模式配置(直接或矢量)。在所有CLINT和CLIC模式中，所有同步异常也使用mtvec作为异常处理的基地址。
* mtvt -仅用于CLIC模式的操作。包含CLIC直接模式下选择性向量化中断和CLIC向量化模式下所有向量化中断的中断向量表的基地址。这种寄存器不存在的设计与CLINT

## 2.1.1 Common Registers to CLIC and CLINT

The CLIC introduces new modes of operation that the CLINT does not support. However, both controllers support software and timer interrupts using the same configuration registers.

* msip — Machine mode software interrupt pending register, used to assert a software interrupt for a CPU.
* mtime — Machine mode timer register which runs at a constant frequency. Part of the CLINT and CLIC designs. There is a single mtime register on designs that contain one or more CPUs.
* mtimecmp — Memory mapped machine mode timer compare register, used to trigger an interrupt when mtimecmp is greater than or equal to mtime. There is an mtimecmp dedicated to each CPU.

> Note: Timer interrupts always trap to Machine mode, unless delegated to Supervisor mode using the mideleg register. Similarly, Machine mode exceptions may be delegated to Supervisor mode using the medeleg register. For designs that also implement User mode, there exists sideleg and sedeleg registers to delegate Supervisor interrupts to User mode. Currently none of the SiFive designs support User mode interrupts.

CLIC引入了CLINT不支持的新操作模式。然而，这两个控制器都支持软件和计时器中断使用相同的配置寄存器。

* msip -机器模式软件中断挂起寄存器，用于为CPU断言一个软件中断。
* 时机机模式定时器寄存器，运行在一个恒定的频率。CLINT和CLIC设计的一部分。在包含一个或多个cpu的设计上有一个单一的时时寄存器。
* mtimecmp -内存映射机器模式定时器比较寄存器，用于触发一个中断时，mtimecmp是大于或等于时时。有一个mtimecmp专用于每个CPU。

注意:计时器中断总是陷阱到机器模式，除非委托到管理模式使用mideleg寄存器。类似地，机器模式异常可以使用medeleg寄存器委托给监视模式。对于也实现用户模式的设计，存在sideleg和sedeleg寄存器来将监控中断委托给用户模式。目前，SiFive的设计都不支持用户模式中断。

## 2.1.2 Memory Mapped Interrupt Registers

There are memory mapped interrupt enable, pending, and optionally priority configuration registers based on which interrupt controller is being used. These are referenced in the following sections specific to the CLIC or PLIC. Note that there are no interrupt enable or priority configuration bits in the CLINT. Specific details for custom designs are included in the respective manual, which is part of the design tarball.

有内存映射中断启用，挂起，可选的优先级配置寄存器基于哪个中断控制器正在被使用。这些是在以下章节中引用的特定于CLIC或PLIC。注意，在CLINT中没有中断启用或优先配置位。定制设计的具体细节包含在各自的手册中，这是设计压缩包的一部分。

## 2.1.3 Early Boot: Setup mtvec Register

The mtvec register is required to be setup early in the boot flow, primarily for exception handling. Interrupts are not fully configured early in the boot flow, but exception handling is important to setup as early as possible, in the event an unexpected synchronous event needs to be handled. SiFive provides a portable software API, which also contains early boot code to support mtvec configuration, in a SiFive github repository called freedom-metal.

The example assembly code below shows the setup for the early_trap_vector which is part of the freedom-metal repository available on github.

mtvec寄存器需要在引导流的早期设置，主要用于异常处理。在引导流的早期没有完全配置中断，但是异常处理对于尽早设置非常重要，如果需要处理意外的同步事件。SiFive在名为freedom-metal的SiFive github存储库中提供了一个可移植的软件API，它还包含支持mtvec配置的早期引导代码。

下面的示例汇编代码显示了early_trap_vector的设置，它是github上可用的freedom-metal存储库的一部分。

```s
/* Set up a simple trap vector to catch anything that goes wrong early in
* the boot process. */
la t0, early_trap_vector
csrw mtvec, t0
The startup code also contains the functionality for early_trap_vector, shown below.
/* For sanity's sake we set up an early trap vector that just does nothing. If
* you end up here then there's a bug in the early boot code somewhere. */
.section .text.metal.init.trapvec
.align 2
early_trap_vector:
.cfi_startproc
csrr t0, mcause
csrr t1, mepc
csrr t2, mtval
j early_trap_vector
.cfi_endproc
```

A more sophisticated trap handler may be required later, after the initial bootup is complete. For example, using a C function to handle the trap, which might contain additional functionality based on the type of trap encountered. In this case, the mtvec register can be written directly using C code.

稍后，在初始引导完成之后，可能需要一个更复杂的陷阱处理程序。例如，使用C函数处理陷阱，它可能包含基于所遇到的陷阱类型的附加功能。在这种情况下，可以直接使用C代码编写mtvec寄存器。

```cpp
int mtvec_value = &my_function_handler;
__asm__ volatile ("csrr %0, mtvec" : "=r"(mtvec_value));
```

> Note: It is recommended to disable interrupts globally using mstatus.mie prior to changing mtvec.

> 注意:建议使用mstatus全局禁用中断。在更改mtvec之前的mie。

## 2.1.4 Standard Entry & Exit Behavior for Interrupt Handlers

Whenever an interrupt occurs, hardware will automatically save and restore important registers.

The following steps are complete as an interrupt handler is entered.

* Save pc to mepc
* Save Privilege level to mstatus.mpp
* Save mie to mstatus.mpie
* Set pc to interrupt handler address, based on mode of operation
* Disable interrupts by setting mstatus.mie=0

At this point control is handed over to software where the interrupt processing begins. At the end of the interrupt handler, the mret instruction will do the following.

* Restore mepc to pc
* Restore mstatus.mpp to Priv
* Restore mstatus.mpie to mie

> Note: priv refers to the current privilege level which is not visible while operating at that level. Possible values are Machine=3, Supervisor=1, User=0

There may be additional instructions or functionality contained within the handler, based on the interrupt controller and mode of operation. For example, saving/restoring additional user registers, enabling preemption, and handling global interrupts routed through the PLIC which require a claim/complete step. Subsequent sections for the CLINT, CLIC, and PLIC will describe these options in more detail.

每当中断发生时，硬件将自动保存和恢复重要的寄存器。

输入中断处理程序后，以下步骤就完成了。

* 保存pc到mepc
* 将权限级别保存到mstatus.mpp
* 把mie保存为mstatus.mpie
* 根据操作方式，将pc设置为中断处理器地址
* 通过设置mstatus.mie=0来禁用中断

在这一点上，控制移交给软件，中断处理开始。在中断处理程序的末尾，mret指令将执行以下操作。

* 恢复mepc到pc
* 恢复mstatus mpp 到 Priv
* 恢复mstatus mpie 到 mie

> 注意: priv指的是当前权限级别，在该级别上操作时不可见。可能的值是Machine=3, Supervisor=1, User=0

基于中断控制器和操作模式，处理程序中可能包含额外的指令或功能。例如，保存/恢复额外的用户寄存器，启用抢占，以及处理通过PLIC路由的全局中断(需要索赔/完成步骤)。CLINT、CLIC和PLIC的后续章节将更详细地描述这些选项。

# Chapter 3 SiFive Interrupt Controllers

## 3.1 Core Local Interrupter (CLINT) Overview

The CLINT has a fixed priority scheme which implements Software, Timer, and External interrupts. Software preemption is only available between privilege levels using the CLINT. For example, while in Supervisor mode, a Machine mode interrupt will immediately take priority and preempt Supervisor mode operation. Preemption within a privilege level is not supported with the CLINT. The interrupt ID represents the fixed priority value of each interrupt, and is not configurable. There are two different CLINT modes of operation, direct mode and vectored mode.

To configure CLINT modes, write mtvec.mode field, which is bit[0] of mtvec CSR. For direct mode, mtvec.mode=0, and for vectored mode mtvec.mode=1. Direct mode is the default reset value. The mtvec.base holds the base address for interrupts and exceptions, and must be a minimum 4-byte aligned value in direct mode, and minimum 64-byte aligned value in vectored mode.

CLINT有一个固定优先级方案，它实现了软件、定时器和外部中断。软件抢占只能在特权级别之间使用CLINT。例如，当处于主管模式时，机器模式中断将立即优先并抢占主管模式的操作。CLINT不支持特权级别内的抢占。中断ID表示每个中断的固定优先级值，并且是不可配置的。有两种不同的运作模式，直接模式和矢量模式。

要配置CLINT模式，写入mtvec.mode field, mtvec CSR的[0]位。对于直接模式，mtvec.mode=0，对于矢量模式mtvec.mode=1。直接模式是默认的重置值。mtvec.base保存中断和异常的基地址，在直接模式下必须是最小的4字节对齐值，在矢量模式下必须是最小的64字节对齐值。

## 3.1.1 Example Interrupt Handler

The example below shows an assembly interrupt handler which pushes all registers onto the stack, calls the handler function, then pops all the registers off the stack.

下面的示例显示了一个程序集中断处理程序，它将所有寄存器压入堆栈，调用处理程序函数，然后将所有寄存器弹出堆栈。

```s
.align 2
.global handler_table_entry
handler_table_entry:
addi sp, sp, -32*REGBYTES
STORE x1, 1*REGBYTES(sp)
STORE x2, 2*REGBYTES(sp)
...
STORE x30, 30*REGBYTES(sp)
STORE x31, 31*REGBYTES(sp)
//---- call C Code Handler ----
call software_handler
//---- end of C Code Handler ----
LOAD x1, 1*REGBYTES(sp)
LOAD x2, 2*REGBYTES(sp)
...
LOAD x30, 30*REGBYTES(sp)
LOAD x31, 31*REGBYTES(sp)
addi sp, sp, 32*REGBYTES
mret

```

For direct mode, all interrupts and exceptions would use handler_table_entry as the starting point of execution, which can be configured by writing the mtvec register with the address of the function.

The overhead of pushing and popping all registers is not usually required, and more efficient methods will be detailed in subsequent sections that introduce GCC compiler attributes specific to interrupt handler functions.

对于直接模式，所有中断和异常都将使用handler_table_entry作为执行的起始点，这可以通过使用函数的地址编写mtvec寄存器来配置。

推入和取出所有寄存器的开销通常不是必需的，在后面介绍用于中断处理程序函数的GCC编译器属性的部分将详细介绍更有效的方法。


## 3.1.2 CLINT Direct Mode

Direct mode means all interrupts and exceptions trap to the same handler, and there is no vector table implemented. It is software’s responsibility to execute code to figure out which interrupt occurred. The software handler in direct mode should first read mcause.interrupt to determine if an interrupt or exception occurred, then decide what to do based on mcause.code value which contains the respective interrupt or exception code.

直接模式意味着所有的中断和异常都被捕获到同一个处理程序，并且没有实现向量表。软件的职责是执行代码来确定哪个中断发生了。直接模式下的软件处理程序应该首先读取mcause.interrupt，以确定是否发生了中断或异常，然后根据mcause决定要做什么。包含各自中断或异常代码的代码值。

## 3.1.3 Example Handler for CLINT Direct Mode

```cpp
#define MCAUSE_INT_MASK 0x80000000 // [31]=1 interrupt, else exception
#define MCAUSE_CODE_MASK 0x7FFFFFFF // low bits show code
void software_handler()
{
  unsigned long mcause_value = read_csr(mcause);
  if (mcause_value & MCAUSE_INT_MASK) {
    // Branch to interrupt handler here
    // Index into 32-bit array containing addresses of functions
    async_handler[(mcause_value & MCAUSE_CODE_MASK)]();
  } else {
    // Branch to exception handler
    sync_handler[(mcause_value & MCAUSE_CODE_MASK)]();
  }
}
```

Software would first need to create and populate the async_handler and sync_handler tables used above, using the interrupt and exception functions designed to support their specific event, as described in the following table.

软件首先需要创建并填充上面使用的async_handler和sync_handler表，使用设计来支持它们的特定事件的中断和异常函数，如下表所述。

![](img/Table1-McauseRegister.png)

The interrupt and exception categorizations listed in this table are standard to all CPU designs implementing RISC-V instruction set architecture.

In summary, CLINT direct mode requires software to setup the following:

1. The primary entry point for interrupt and exceptions, shown in handler_table_entry, the base address of which is assigned to mtvec.base
2. A software handler to determine whether the event is an interrupt or exception, which also contains code to jump to the appropriate interrupt function or exception handler
3. The actual interrupt or exception function, where the address of each function is written to the asynch_handler or the sync_handler arrays, respectively

在这个表中列出的中断和异常分类是所有实现RISC-V指令集架构的CPU设计的标准。

总之，CLINT直接模式需要软件设置以下:

1. 中断和异常的主要入口点，如handler_table_entry所示，它的基址被分配给 mtvm.base
2. 确定事件是中断还是异常的软件处理程序，也包含跳转到适当的中断函数或异常处理程序的代码
3. 实际的中断或异常函数，其中每个函数的地址分别写入asynch_handler或sync_handler数组

## 3.1.4 CLINT Vectored Mode

Vectored mode introduces a method to create a vector table that hardware uses for lower interrupt handling latency. When an interrupt occurs in vectored mode, the pc will get assigned by the hardware to the address of the vector table index corresponding to the interrupt ID. From the vector table index, a subsequent jump will occur from there to service the interrupt. Recall that the vector table contains an opcode that is a jump instruction to a specific location.

矢量模式引入了一种创建矢量表的方法，硬件使用它来降低中断处理延迟。当在矢量模式下发生中断时，pc将由硬件分配到与中断ID相对应的矢量表索引地址。从矢量表索引开始，一个后续跳转将从那里开始服务中断。回想一下，vector表包含一个操作码，它是一个到特定位置的跳转指令。

![](img/Figure1-ClintVectorTable.png)

The interrupt handler offset is calculated by mtvec.base + (mtvec.code * 4). For example, software interrupts with ID of 3 would trap to offset mtvec.base + 0xC. On a different note, all exceptions still trap to the base address defined in mtvec, as there is no vector table for exception handling.

中断处理程序偏移量由mtvec计算 mtvec.base + (mtvec.code * 4)。例如，ID为3的软件中断会trap 到 mtvec.base + 0xC。另一方面，由于没有用于异常处理的向量表，所以所有异常仍然陷在mtvec中定义的基址上。

```cpp
#define MCAUSE_INT_MASK 0x80000000 // [31]=1 interrupt, else exception
#define MCAUSE_CODE_MASK 0x7FFFFFFF // low bits show code
void software_handler()
{
  // Vectored interrupts will jump directly to their vector table offset,
  // and will not enter software_handler() here.
  // read mcause for exception handling
  unsigned long mcause_value = read_csr(mcause);
  // Branch to exception handler
  sync_handler[(mcause_value & MCAUSE_CODE_MASK)]();
}
```

CLINT vectored mode does not require the same software overhead shown previously in the software_handler function for interrupt handling. In this mode when an interrupt occurs, program execution jumps directly to the vector table offset for the corresponding interrupt.

CLINT矢量模式不需要与前面在software_handler函数中显示的中断处理相同的软件开销。在这种模式下，当中断发生时，程序执行直接跳转到对应中断的向量表偏移量。

![](img/Figure2-ClintVectorTableOffset.png)

## 3.1.5 CLINT Interrupt Levels, Priorities, and Preemption

For CPU designs that utilize Machine mode only, the CLINT would have the following configuration:

* Software interrupts — Interrupt ID #3.
    * Software interrupts are triggered by writing the memory mapped interrupt pending register msip for a particular CPU. In a multi-CPU system, other CPUs are able to write msip to trigger a software interrupt on any other CPU in the system. This allows for efficient inter-processor communication.
* Timer interrupt — Interrupt ID #7.
    * Timer interrupts are triggered when the memory mapped register mtime is greater than or equal to the global timebase register mtimecmp, and both registers are part of the CLINT and CLIC memory map. In a multi-CPU system, mtimecmp can be written by other CPUs to setup timer interrupts.
* External interrupts — Interrupt ID #11.
    * Global interrupts are usually first routed to the PLIC, then into the CPU using External interrupt ID #11. For systems that do not implement a PLIC, this interrupt can optionally be disabled by tying it to logic 0.
* Local Interrupts are Interrupt ID #16 and higher.
    * Local Interrupts may connect directly to an interrupt source, and do not need to be routed through the PLIC. Specifically to the CLINT, they all have fixed interrupt priority based on their interrupt ID. The maximum number of interrupts for 32-bit designs is 16, while 64-bit designs can have up to 48. This is calculated using the formula XLEN-16.

对于CPU设计，只包含机器模式的 CLINT 将有以下配置:

* 软件中断-中断ID #3。
* 软件中断是通过为一个特定的CPU写内存映射中断挂起寄存器msip来触发的。在多CPU系统中，其他CPU能够编写msip来触发系统中任何其他CPU上的软件中断。这允许高效的处理器间通信。
* 计时器中断-中断ID #7。
* 当内存映射寄存器时时大于或等于全局时基寄存器mtimecmp时触发计时器中断，并且两个寄存器都是CLINT和CLIC内存映射的一部分。在多cpu系统中，mtimecmp可以被其他cpu写入来设置计时器中断。
* 外部中断-中断ID #11。
* 全局中断通常首先被路由到PLIC，然后使用外部中断ID #11进入CPU。对于没有实现PLIC的系统，这个中断可以选择性地通过将它绑定到逻辑0来禁用。
* 本地中断是中断ID #16或更高。
* 本地中断可以直接连接到中断源，不需要通过PLIC路由。对于CLINT，他们都有固定的中断优先级基于他们的中断ID。32位设计的最大中断数是16，而64位设计可以有多达48。这是用XLEN-16公式计算出来的。

## 3.1.6 System Level Block Diagram using CLINT Only

An example configuration using CLINT with no global interrupt controller is shown below.

下面是一个使用CLINT的配置示例，它没有全局中断控制器。

![](img/Figure3-ClintBlockDiagram.png)

## 3.2 Core Local Interrupt Controller (CLIC) Overview

The CLIC has a more flexible configuration than the CLINT, however the CLINT is a smaller design overall. Some CLIC features include:

* Reverse compatibility with the CLINT for software, timer, and external interrupts, when programmed to use legacy CLINT modes through the mtvec register.
* Introduces new CLIC direct and CLIC vectored modes that offer programmable interrupt levels and priorities, which support preemption from interrupts of higher levels and priorities.
* Retains the mtime and mtimecmp memory mapped timer registers, and msip register for triggering software interrupts.
* Extends the mode field of mtvec by six bits, [5:0], where [1:0] are defined to support two additional modes: CLIC direct and CLIC vectored.
    * To configure CLIC direct mode, write mtvec.mode=0x02, and for CLIC vectored mode, mtvec.mode=0x03.
* Flexibility to implement CLIC without vectored mode for applications that do not require low latency real time interrupt handling.

CLIC有一个更灵活的配置比CLINT，但CLINT是一个较小的整体设计。一些CLIC功能包括:

* 反向兼容CLINT的软件，定时器，和外部中断，当编程使用遗留CLINT模式通过mtvec寄存器。
* 引入新的CLIC直接和CLIC矢量模式，提供可编程中断级别和优先级，支持从更高级别和优先级的中断抢占。
* 保留时时和mtimecmp内存映射计时器寄存器，和触发软件中断的msip寄存器。
* 扩展mtvec的模式域6位[5:0]，其中[1:0]被定义为支持另外两种模式:CLIC直接模式和CLIC矢量模式。
* 要配置CLIC直接模式，请写入mtvec.mode=0x02，对于CLIC形矢量模式，mtvec.mode=0x03。
* 灵活性实现的CLIC没有矢量模式的应用，不需要低延迟的实时中断处理。

## 3.2.1 CLIC Direct Mode

CLIC direct mode operates in a similar fashion to CLINT direct mode, however it introduces a feature called selective vectoring. Selective vectoring allows each interrupt to be configured for CLIC hardware vectored operation, while all other interrupts use CLIC direct mode. The clicintcfg[i].SHV field is used to configure selective vectoring. CLIC direct modes uses mtvec as the base address for exception and interrupt handling, but introduces mtvt as the base address for interrupts configured for selective hardware vectoring. 

CLIC直接模式的运作方式与CLINT直接模式类似，但它引入了一个功能称为选择性矢量。选择性矢量允许每个中断配置为 CLIC 硬件矢量操作，而所有其他中断使用CLIC直接模式。clicintcfg[i]。SHV字段用于配置选择性矢量。CLIC直接模式使用mtvec作为异常和中断处理的基址，但引入mtvt作为为选择性硬件矢量配置的中断的基址。

## 3.2.2 CLIC Vectored Mode

CLIC vectored mode has a similar concept to CLINT vectored mode, where an interrupt vector table is used for specific interrupts. However, in CLIC vectored mode, the handler table contains the address of the interrupt handler instead of an opcode containing a jump instruction. When an interrupt occurs in CLIC vectored mode, the address of the handler entry from the vector table is loaded and then jumped to in hardware. CLIC vectored mode uses mtvec exclusively for exception handling, since mtvt is used to define the base address for all vectored interrupts.

It should be noted that access to mtvt may need to be done directly using the CSR number (0x307) instead of the mtvt keyword if it is not supported in the toolchain. For example:

CLIC矢量模式与CLINT矢量模式有类似的概念，其中中断向量表用于特定的中断。然而，在CLIC矢量模式中，处理程序表包含中断处理程序的地址，而不是包含跳转指令的操作码。当在CLIC矢量模式下发生中断时，从矢量表中加载处理器条目的地址，然后跳转到硬件中。CLIC矢量模式使用mtvec专门用于异常处理，因为mtvt用于定义所有矢量中断的基址。

应该注意的是，如果工具链不支持mtvt，那么可能需要直接使用CSR编号(0x307)而不是mtvt关键字来访问mtvt。例如:

```cpp
int mtvt_read_value;
__asm__ volatile ("csrr %0, 0x307" : "=r"(mtvt_read_value));
```

![](img/Figure4-ClicVectorTableOffset.png)

## 3.2.3 CLIC Interrupt Levels, Priorities, and Preemption

The CLIC allows programmable interrupt levels and priorities for all supported interrupts. The interrupt level is the first step to determine which interrupt gets serviced first, whereas the priority is used to break the tie in the event two interrupts of the same level are received by the CPU at the same time. The CLIC can support up to 1024 interrupts, where 0 through 16 are reserved for software, timer, external, and CLIC software interrupt for all privilege modes. The CLIC software interrupt (ID #12) serves a similar function as the legacy machine software interrupt, except its typical use interrupting software threads. This leaves a total of 1008 available external interrupts for custom use. Note that interrupt ID #12 is likely a future addition to the standard RISC-V Machine Cause (mcause) Register Table, referenced previously.

CLIC允许可编程中断级别和优先级为所有支持的中断。中断级别是决定哪个中断首先得到服务的第一步，而优先级则用于在CPU同时接收到两个相同级别的中断时断开连接。CLIC可以支持多达1024个中断，其中0到16个为软件、定时器、外部和CLIC软件中断保留所有特权模式。CLIC软件中断(ID #12)的功能与传统机器软件中断类似，除了它典型的使用中断软件线程。这样总共剩下1008个可用的外部中断供自定义使用。注意，中断ID #12很可能是将来添加到标准RISC-V机器原因(mcause)寄存器表(前面引用过)中的。

## 3.2.4 Software Attributes for Interrupts

To help with efficiency of save and restore context, interrupt attributes can be applied to functions used for interrupt handling.

为了提高保存和恢复上下文的效率，可以将中断属性应用于用于中断处理的函数。

```cpp
void __attribute__ ((interrupt))
  software_handler (void) {
    // handler code
  }`
```

This attribute will save and restore additional registers that are used within the handler, and add an mret instruction at the end of the handler.

The functionality can be demonstrated by comparing the list output of functions with and without the attribute applied.

这个属性将保存和恢复处理程序中使用的其他寄存器，并在处理程序的末尾添加一个mret指令。

可以通过比较有和没有应用该属性的函数的列表输出来演示该功能。

![](img/Figure5-CAttributeCompare.png)

## Enabling Preemption in CLIC Modes

In order for an interrupt of a higher level to preempt an active interrupt of a lower level, mstatus.mie needs to be enabled (non zero) within the handler, since it is disabled by hardware automatically upon entry. Prior to re-enabling interrupts through mstatus.mie, first mepc and mcause must be saved, and subsequently restored before mret is executed at the end of the handler. There is a CLIC specific interrupt attribute that will do these steps automatically.

为了让一个较高级别的中断抢占一个较低级别的活动中断，mstatus。mie需要在处理程序中启用(非零)，因为硬件在进入时自动禁用它。在通过mstatus重新启用中断之前。首先必须保存mie、mepc和mcause，然后在处理程序的末尾执行mret之前还原。有一个特定的CLIC中断属性会自动执行这些步骤。

```cpp
void __attribute__ ((interrupt("SiFive-CLIC-preemptible")))
  software_handler (void) {
    // handler code
  }
```

> Note: Using the SiFive-CLIC-preemptible attribute requires the addition of the -fomit-framepointer compiler flag.

The functionality of this CLIC specific attribute can be demonstrated by comparing the list output of functions with and without the attribute applied.

> 注意:使用SiFive-CLIC-preemptible属性需要添加-fomit-framepointer编译器标志。

这个特定的CLIC属性的功能可以通过比较应用该属性和不应用该属性的函数的列表输出来演示。

![](img/Figure6-ClicAttributeCompare.png)

This attribute applies to vectored interrupts. To support preemption for non-vectored interrupts, refer to the CLIC spec example here. Also, refer to the CLIC section on how to manage interrupt stacks across privilege modes here.

此属性适用于矢量中断。要支持非矢量中断的抢占，请参考这里的CLIC规范示例。另外，请参阅关于如何跨特权模式管理中断堆栈的CLIC部分。

## 3.2.5 Details for CLIC Modes of Operation

In CLIC modes of operation, both mie (machine interrupt enable) and mip (machine interrupt pending) registers are hard wired to zero, and their functionality moves to clicintie[i] and clicintip[i] registers. Additionally, mideleg (interrupt delegation register), which can direct interrupts to be handled in different privilege levels, is not available. Instead, the mode field in the clicintcfg[i] register is used to determine which privilege mode the interrupt is taken.

> Note: The mideleg register is not implemented on designs which implement only Machine mode.

在CLIC操作模式中，mie(机器中断启用)和mip(机器中断挂起)寄存器都被硬连接到0，并且它们的功能移动到clicintie[i]和clicintip[i]寄存器。此外，mideleg(中断委托寄存器)也不可用，它可以将中断直接在不同的特权级别上处理。相反，clicintcfg[i]寄存器中的模式字段用于确定中断采取哪种特权模式。

注意:mideleg寄存器不是在只实现机器模式的设计上实现的。

## New Registers within the CLIC

CLIC modes of operation introduce new registers compared to designs that implement a CLINT.

* cliccfg - Memory mapped CLIC configuration register
    * Determines the number of levels and priorities set by clicintcfg[i]. Also contains selective hardware vector configuration, which allows direct mode or vectored mode on a per-interrupt basis
* clicintcfg[i] - Memory mapped CLIC interrupt configuration register
    * Sets the pre-emption level and priority of a given interrupt
* clicintie[i] - Memory mapped CLIC interrupt enable register
* clicintip[i] - Memory mapped CLIC interrupt pending register
* mtvt - CSR which holds the Machine Trap Vector Table base address for CLIC vectored interrupts
    * Write Always, Read Legal (WARL) register allows for relocatable vector tables, where mtvt.base requires a minimum 64-byte alignment. At a minimum, mtvt.base has a 64-byte alignment requirement, but can increase depending on the total number of CLIC interrupts implemented. Refer to the CLIC specification for more details.
* mnxti - CSR containing the Machine Next Interrupt Handler Address and Interrupt-Enable
    * Used by software in CLIC direct mode to service the next interrupt of equal or greater level before returning to a lower level context
    * A read to this CSR returns the address of an entry in the vector table (mtvt)
    * Can simultaneously be written to, which affects mstatus - this is used to disable interrupts on the same cycle as the read
    * Returns 0 if no higher level interrupt than the saved context, or if a HART is not in CLIC mode, or if the next highest ranked interrupt is selectively hardware vectored
* mintstatus - Read only CSR which holds the Active Machine Mode Interrupt Level
    * Read only register containing current interrupt level for the respective privilege mode

CLIC模式的操作引入新的寄存器相比，实现CLINT的设计。

* cliccfg -内存映射CLIC配置寄存器
    * 确定由clicintcfg[i]设置的级别和优先级的数量。也包含选择性的硬件矢量配置，它允许在每个中断的基础上直接模式或矢量模式
* clicintcfg[i] -内存映射CLIC中断配置寄存器
    * 设置给定中断的优先级和优先级
* clicintie[i] -内存映射的CLIC中断启用寄存器
* clicintip[i] -内存映射的CLIC中断挂起寄存器
* mtvt - CSR，它为CLIC矢量中断保存机器陷阱向量表基地址
    * 始终写，读合法(WARL)寄存器允许重定位向量表，其中mtvt。base要求至少64字节对齐。至少mtvt。base有一个64字节的对齐要求，但是可以根据实现的CLIC中断的总数增加。更多细节请参考CLIC规范。
* mnxti - CSR，包含机器下一个中断处理程序地址和中断启用
    * 在CLIC直接模式下使用的软件，在返回到低级别上下文之前，为同等或更高级别的下一个中断提供服务
    * 对这个CSR的读取返回向量表(mtvt)中的一个条目的地址
    * 可以同时被写入，这影响mstatus -这是用来禁用中断在相同的周期读
    * 如果没有比保存的上下文更高级别的中断，或者HART没有处于CLIC模式，或者下一个最高级别的中断是选择性的硬件矢量，则返回0
* mintstatus -只读CSR，保持主动机器模式中断级别
    * 只读寄存器，包含各自特权模式的当前中断级别

## 3.2.6 Changes to CSRs in CLIC Mode

* mstatus
    * The mstatus.mpp and mstatus.mpie are accessible via fields in the mcause register
* mie and mip
    * mie and mip are hardwired to zero and replaced with memory mapped clicintie[i] and clicintip[i] registers
* mtvec
    * Additional modes which enable CLIC mode of operation
* mcause
    * Stores previous privilege mode, and previous interrupt enable

----

* mstatus
    * mstatus。mpp和mstatus。可以通过mcause寄存器中的字段访问mpie
* mie和mip
    * mie和mip被硬连接到零，并用内存映射的clicintie[i]和clicintip[i]寄存器替换
* mtvec
    * 启用CLIC操作模式的附加模式
* mcause
    * 存储以前的特权模式，和以前的中断启用

## 3.2.7 System Level Block Diagram using CLIC only

An example configuration using a CLIC with no global interrupt controller is shown below.

下面是一个使用没有全局中断控制器的环的配置示例。

![](img/Figure7-ClicBlockDiagram.png)

> Note: In CLIC modes of operation all local interrupts have programmable levels and priorities, which is not determined by the Interrupt ID.

> 注意:在运行的CLIC模式中，所有的本地中断都有可编程的级别和优先级，这不是由中断ID决定的。

The external interrupt connection may not be needed for designs that do not require global interrupts routed through the PLIC, and may be disabled by tying it to logic zero. In legacy CLINT mode of operation, the software, timer, and external interrupt lines are wired directly to the CPU.

These lines are not used when CLIC modes are selected.

外部中断连接在不需要通过PLIC路由全局中断的设计中可能是不需要的，并且可能通过将其绑定到逻辑零而被禁用。在传统的CLINT模式的操作，软件，定时器，和外部中断线直接连线到CPU。

这些线不使用时，CLIC模式被选择。

## 3.3 Platform Level Interrupt Controller (PLIC) Overview

The PLIC is used to manage all global interrupts and route them to one or many CPUs in the system. It is possible for the PLIC to route a single interrupt source to multiple CPUs. Upon entry to the PLIC handler, a CPU reads the claim register to acquire the interrupt ID. A successful claim will atomically clear the pending bit in the PLIC interrupt pending register, signaling to the system that the interrupt is being serviced. During the PLIC interrupt handling process, the pending flag at the interrupt source should also be cleared, if necessary. It is legal for a CPU to attempt to claim an interrupt even when the pending bit is not set. This may happen, for example, when a global interrupt is routed to multiple CPUs, and one CPU has already claimed the interrupt just prior to the claim attempt on another CPU. Before exiting the PLIC handler with MRET instruction (or SRET/URET), the claim/complete register is written back with the non zero claim/complete value obtained upon handler entry. Interrupts routed through the PLIC incur an additional three cycle penalty compared to local interrupts. Cycles in this case are measured at the PLIC frequency, which is typically an integer divided value from the CPU and local interrupt controller frequency.

PLIC用于管理所有全局中断，并将它们路由到系统中的一个或多个cpu。PLIC将一个中断源路由到多个cpu是可能的。在进入PLIC处理器时，CPU读取请求寄存器来获取中断ID。一个成功的请求将自动清除PLIC中断等待寄存器中的挂起位，向系统发出中断正在被服务的信号。在PLIC中断处理过程中，中断源的挂起标志也应该被清除，如果必要的话。是法律的一个CPU试图声称一个中断即使等待没有设置。这可能发生,例如,当全球中断路由到多个CPU,和一个CPU已经宣称中断claim之前尝试在另一个CPU。在使用MRET指令(或SRET/URET)退出PLIC处理程序之前，使用在处理程序入口获得的非零claim/complete值回写claim/complete寄存器。与本地中断相比，通过PLIC路由的中断会产生额外的3个周期的损失。在这种情况下，周期被测量在PLIC频率，这是典型的一个整数除以从中央处理器和本地中断控制器频率。

## 3.3.1 PLIC Priorities and Preemption

There are up to 1024 available interrupts routed into the PLIC, which are numbered sequentially 1 through 1024. Each interrupt into the PLIC has a configurable priority, from 1-7, with 7 being the highest priority. A value of 0 means do not interrupt, effectively disabling that interrupt. There is a global threshold register within the PLIC that allows interrupts configured below a certain level to be blocked. For example, if the threshold register contains a value of 5, all PLIC interrupt configured with priorities from 1 through 5 will not be allowed to propagate to the CPU. If global interrupts with the same priority arrive at the same time into the PLIC, priority is given to the lower of the two Interrupt IDs.

有多达1024个可用中断被路由到PLIC中，它们按顺序编号为1到1024。每个中断进入PLIC有一个可配置的优先级，从1-7，与7是最高优先级。值0表示不中断，有效地禁用该中断。在PLIC中有一个全局阈值寄存器，它允许配置在某个级别以下的中断被阻止。例如，如果阈值寄存器包含一个值5，所有优先级从1到5配置的PLIC中断将不被允许传播到CPU。如果具有相同优先级的全局中断同时到达PLIC，则优先级给予两个中断id中较低的那个。

Global interrupts routed through the PLIC are connected to the CPU in slighty different ways depending on the local interrupt selection. If the PLIC is used with the CLINT, then the external interrupt connection routed from the PLIC is tied directly to the CPU. If the PLIC is used with the CLIC, then the external interrupt connection is not used, and the interrupt is routed from the PLIC through the CLIC interface. By definition, the PLIC cannot forward a new interrupt to a HART that has claimed an interrupt but has not yet finished the complete step of the interrupt handler. Thus, the PLIC does not support preemption of global interrupts to an individual HART.

根据本地中断的选择，通过PLIC路由的全局中断以略微不同的方式连接到CPU。如果PLIC与CLINT一起使用，那么从PLIC路由的外部中断连接直接绑定到CPU。如果PLIC与CLIC一起使用，那么外部中断连接就不被使用，并且中断被从PLIC通过CLIC接口路由。根据定义，PLIC不能将一个新的中断转发给HART，因为HART已经请求了中断，但是还没有完成中断处理程序的完整步骤。因此，PLIC不支持对单个HART的全局中断的抢占。

However, since PLIC interrupts arrive at the CPU through the external interrupt connection, preemption may occur from other CLIC local interrupts that are configured with a higher priority than the external interrupt. To support preemption, mstatus.mie needs to be re-enabled within the handler, since it is disabled by hardware upon entry. Interrupt IDs for global interrupts routed through the PLIC are independent of the interrupt IDs for local interrupts. Thus, software may need to implement a specific handler which supports a software lookup table for the global interrupts that are managed by the PLIC, and arrive at the CPU through the external interrupt connection.

然而，由于PLIC中断通过外部中断连接到达CPU，抢占可能发生在其他被配置比外部中断更高优先级的CLIC本地中断。支持优先权，mstatus。mie需要在处理程序中重新启用，因为它在进入时被硬件禁用。通过PLIC路由的全局中断的中断id与本地中断的中断id是独立的。因此，软件可能需要实现一个特定的处理程序，该处理程序支持由PLIC管理并通过外部中断连接到达CPU的全局中断的软件查找表。

> Note: Recall that the CLINT local interrupts priorities are fixed since they are tied to their interrupt ID, while CLIC has programmable levels and priorities.

> 注意:回想一下，CLINT本地中断的优先级是固定的，因为它们被绑定到它们的中断ID上，而CLIC有可编程的级别和优先级。

Additionally, the PLIC handler may check for additional pending global interrupts once the initial claim/complete process has finished, prior to exiting the handler. This method could save additional PLIC save/restore context for global interrupts

此外，一旦初始索赔/完成过程完成，PLIC处理程序可能会在退出处理程序之前检查额外的挂起的全局中断。这个方法可以为全局中断保存额外的PLIC保存/恢复上下文

## 3.3.2 PLIC Handler Example

Since all global interrupts routed through the PLIC are connected to the CPU through the external interrupt, this handler requires the additional claim/complete step to notify the PLIC that the current global interrupt is being serviced.

由于所有通过PLIC路由的全局中断都通过外部中断连接到CPU，所以这个处理程序需要额外的claim/complete步骤来通知PLIC当前的全局中断正在被服务。

```cpp
void machine_external_interrupt()
{
  //get the highest priority pending PLIC interrupt
  uint32_t int_num = plic.claim_comlete;
  //branch to handler
  plic_handler[int_num]();
  //complete interrupt by writing interrupt number back to PLIC
  plic.claim_complete = int_num;
}
```

The machine_external_interrupt function would reside at vector table offset +0x2C when using CLINt vectored mode of operation. This offset changes to +0x58 for 64-bit architectures while using CLIC vectored mode of operation. For non-vectored modes, the async_handler resides at index 11 in the software table. The async_handler was referenced in the CLINT example previously

The plic_handler software table would be populated with functions that support each of the PLIC specific global interrupts.

当使用CLINt向矢量模式的操作时，machine_external_interrupt函数将驻留在向量表offset +0x2C上。对于64位体系结构，当使用CLIC向量操作模式时，这个偏移量将更改为+0x58。对于非矢量模式，async_handler驻留在软件表的索引11处。在前面的CLINT示例中引用了async_handler

plic_handler软件表将填充支持每个特定于PLIC的全局中断的函数。

## 3.3.3 PLIC + CLINT, Machine Mode Interrupts Only

For a multi-CPU system implementing Machine mode only, an example configuration is shown below.

对于仅实现机器模式的多cpu系统，下面显示了一个配置示例。

![](img/Figure8-PlicBlockDiagram.png)

> Note: For systems that implement Supervisor mode, there will be additional Supervisor interrupt connections into each HART. User level interrupts are only available on devices which implement the RISC-V "N" extension.

> 注意: 对于实现管理模式的系统，将会有额外的到每个HART的管理中断连接。用户级中断仅在实现了RISC-V "N" 扩展的设备上可用。

## 3.3.4 PLIC + CLIC, Machine Mode Interrupts Only

Below is a representation of a design implementing multiple cores which requires a PLIC global interrupt controller.

下面是一个实现需要PLIC全局中断控制器的多核设计的表示。

![](img/Figure9-PlicClicBlockDiagram.png)

The external interrupt connection is routed directly to the HART when CLIC operates in legacy CLINT mode, but this connection is not used in CLIC modes of operation.

Local interrupts are routed into the CLIC where the memory mapped registers exist for configurable interrupt levels and priorities. This is different from the connectivity when using the CLINT, where they are connected directly to the HART.

当CLIC在遗留CLINT模式下运行时，外部中断连接被直接路由到HART，但该连接不用于CLIC模式下的操作。

本地中断被路由到具有可配置中断级别和优先级的内存映射寄存器的CLIC中。这与使用CLINT时的连接是不同的，在那里他们直接连接到HART。

# Chapter 4 Additional Code Examples

The steps below show the high level configuration to properly enable an interrupt.

下面的步骤显示了正确启用中断的高级配置。

## 4.1 Pseudo Code to Setup an Interrupt

1. Write mtvec to configure the interrupt mode and the base address for the interrupt vector table, and optionally, mtvt for CLIC modes. The CSR number for mtvt is 0x307.
2. Enable interrupts in memory mapped PLIC or CLIC register space. The CLINT does not contain interrupt enable bits.
3. Write mie to enable the software, timer, and external interrupt enables for each privilege mode
4. Write mstatus to enable interrupts globally for each supported privilege mode

> Note: mie register is disabled in CLIC modes. Use clicintie[i] to enable interrupts in CLIC modes of operation.

1. 编写mtvec来配置中断模式和中断向量表的基址，并可选地为CLIC模式配置mtvt。mtvt的CSR编号是0x307。
2. 在内存映射的PLIC或CLIC寄存器空间中启用中断。CLINT不包含中断启用位。
3. 写mie来启用每个特权模式的软件、定时器和外部中断启用
4. 写mstatus以全局地为每种支持的特权模式启用中断

> 注意:mie寄存器在CLIC模式下是禁用的。使用clicintie[i]在CLIC操作模式中启用中断。

## 4.2 Freedom Metal API

The freedom-metal library is a bare metal C programming environment provided by SiFive that utilizes an application program interface (API) designed to be portable across different CPU architectures. The freedom-e-sdk repo includes freedom-metal to leverage its functionality for software examples and startup code which run on any SiFive design. For a full list of examples, see https://github.com/sifive/freedom-e-sdk/tree/master/software. To reference the freedommetal documentation, see https://sifive.github.io/freedom-metal-docs/.

freedom -metal库是SiFive提供的一种纯金属C编程环境，它利用了一种应用程序编程接口(API)，被设计为可以跨不同的CPU架构移植。freedom -e-sdk回购包括freedom -metal，以利用其软件示例和启动代码的功能，运行在任何SiFive设计上。要获得完整的示例列表，请参见https://github.com/sifive/freedom-e-sdk/tree/master/software。要引用freedommetal文档，请参见https://sifive.github.io/freedom-metal-docs/。

## 4.3 DeviceTree Interrupt Mapping in design.dts file

SiFive’s design environment generates a device tree specification file called design.dts for all supported cores. This file describes the hardware, including the base address for interfaces and peripherals, size of the available interface, and the interrupt connectivity. It is the starting point for auto-generating software components that are necessary for compiling software for new designs. The following auto generated components exist in the freedom-e-sdk/bsp path:

* Header files (.h) used by freedom-metal library for startup code and initialization
* Linker scripts (.lds) used map the software builds to the ports or memory available in the design
* Makefile named `settings.mk` which passes architecture and application binary interface information to the toolchain, and additional details to support output file formatting

To understand how the software components are generated from the design.dts file, refer to the Custom Core Getting Started Guide, available in the Application Notes section at https://www.sifive.com/documentation.

SiFive的设计环境生成一个称为design的设备树规范文件。所有支持核心的dts。这个文件描述了硬件，包括接口和外设的基址、可用接口的大小和中断连接。它是自动生成软件组件的起点，而这些组件对于编译新设计的软件是必要的。freedom-e-sdk/bsp路径中自动生成的组件如下:

* freedom-metal 库用于启动代码和初始化的头文件(.h)
* 链接器脚本(.lds)用于将软件构建映射到设计中可用的端口或内存
* Makefile命名设置`settings.mk`，它将体系结构和应用程序的二进制接口信息传递给工具链，以及支持输出文件格式的其他细节

了解如何从设计中生成软件组件。dts文件，请参考自定义核心入门指南，该指南可在https://www.sifive.com/documentation的应用程序说明部分中获得。

## 4.4 Interrupt components in design.dts - CLIC Example

The interrupt components of an E21 design.dts file are shown below. This design uses a CLIC local interrupt controller, and here we highlight the interrupt specific components described in the design.dts file.

E21设计的中断组件。dts文件如下所示。本设计使用了一个CLIC本地中断控制器，这里我们强调了在设计中描述的中断特定组件。dts文件。

```cpp
L14: cpus {
  #address-cells = <1>;
  #size-cells = <0>;
  L4: cpu@0 {
    clock-frequency = <0>;
    compatible = "sifive,caboose0", "riscv";
    device_type = "cpu";
    hardware-exec-breakpoint-count = <4>;
    reg = <0x0>;
    riscv,isa = "rv32imac";
    riscv,pmpregions = <4>;
    status = "okay";
    timebase-frequency = <1000000>;
    L3: interrupt-controller {
    #interrupt-cells = <1>;
    compatible = "riscv,cpu-intc";
    interrupt-controller;
    };
  };
};
```

The L3 instance above is the interrupt-controller, which is part of the CPU, and is the parent node to interrupt-controller@20000000 below.

上面的L3实例是中断控制器，它是CPU的一部分，是下面的interrupt-controller@20000000的父节点。

```cpp
L1: interrupt-controller@2000000 {
  #interrupt-cells = <1>;
  compatible = "sifive,clic0";
  interrupt-controller;
  interrupts-extended = <&L3 3 &L3 7 &L3 11>;
  reg = <0x2000000 0x1000000>;
  reg-names = "control";
  sifive,numintbits = <4>;
  sifive,numints = <143>;
  sifive,numlevels = <16>;
};
```

The line containing <&L3 3 &L3 7 &L3 11> describe the software, timer, and external interrupt IDs of the local interrupt controller.

包含<&L3 3 &L3 7 &L3 11>的行描述了本地中断控制器的软件、定时器和外部中断id。

```cpp
L9: local-external-interrupts-0 {
  compatible = "sifive,local-external-interrupts0";
  interrupt-parent = <&L1>;
  interrupts = <16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 ... >;
};
```

The local-external-interrupts-0 node L9, above, has L1 as the parent node, and lists the additional local interrupt lines available in the design, starting at 16. The total number of interrupts in this list is defined by the user when designing a core on SiFive.com.

> Note: The compatible string above refers directly to the file freedom-metal/src/drivers/sifive_localexternal-interrupts0.c

上面的local-external-interrupts-0节点L9以L1为父节点，并列出了设计中可用的其他本地中断行，从16开始。这个列表中的中断总数是用户在设计SiFive.com上的核心时定义的。

> 注意:上面兼容的字符串直接引用文件freedom-metal/src/drivers/sifive_localexternal-interrupts0.c

## 4.5 Interrupt components in design.dts - CLINT Example

The interrupt components of an S76 design.dts file are shown below. This design uses a CLINT local interrupt controller, as well as a PLIC global interrupt controller. Here we highlight the interrupt specific components described in the file.

S76设计的中断部件。dts文件如下所示。本设计采用CLINT本地中断控制器和PLIC全局中断控制器。这里我们突出显示文件中描述的特定于中断的组件。

```cpp
L7: cpu@0 {
  clock-frequency = <0>;
  compatible = "sifive,bullet0", "riscv";
  d-cache-block-size = <64>;
  d-cache-sets = <128>;
  d-cache-size = <32768>;
  device_type = "cpu";
  hardware-exec-breakpoint-count = <4>;
  i-cache-block-size = <64>;
  i-cache-sets = <128>;
  i-cache-size = <32768>;
  next-level-cache = <&L10>;
  reg = <0x0>;
  riscv,isa = "rv64imafdc";
  riscv,pmpregions = <8>;
  sifive,dls = <&L6>;
  sifive,itim = <&L5>;
  status = "okay";
  timebase-frequency = <1000000>;
  L4: interrupt-controller {
  #interrupt-cells = <1>;
  compatible = "riscv,cpu-intc";
  interrupt-controller;
  };
};
```

The L4 instance above is the interrupt-controller, which is part of the CPU, and is the parent node to clint@2000000 below.

上面的L4实例是中断控制器，它是CPU的一部分，是下面的clint@2000000的父节点。

```cpp
L2: clint@2000000 {
  compatible = "riscv,clint0";
  interrupts-extended = <&L4 3 &L4 7>;
  reg = <0x2000000 0x10000>;
  reg-names = "control";
};
```

The line containing <&L4 3 &L4 7> describe the software and timer interrupt IDs of the local interrupt controller. Notice the external interrupt ID #11 is not in this list like it was in the CLINT.

包含<&L4 3 &L4 7>的行描述了本地中断控制器的软件和定时器中断id。注意，外部中断ID #11不在这个列表中，就像它在CLINT中一样。

```cpp
L9: global-external-interrupts {
  compatible = "sifive,global-external-interrupts0";
  interrupt-parent = <&L1>;
  interrupts = <1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 ...>;
};
```

The PLIC is represented here by the global-external-interrupts node, but its parent node is interrupt-controller@c000000, below. The total number of interrupts is configured when designing a core on SiFive.com. The interrupts field represents the global interrupts that route into the PLIC controller, which are independent of the interrupt IDs for the local interrupt controller

PLIC在这里由 global-external-interrupts 节点表示，但是它的父节点是下面的interrupt-controller@c000000。中断的总数是在设计SiFive.com上的核心时配置的。interrupts字段表示路由到PLIC控制器的全局中断，它独立于本地中断控制器的中断id

```cpp
L1: interrupt-controller@c000000 {
  #interrupt-cells = <1>;
  compatible = "riscv,plic0";
  interrupt-controller;
  interrupts-extended = <&L4 11>;
  reg = <0xc000000 0x4000000>;
  reg-names = "control";
  riscv,max-priority = <7>;
  riscv,ndev = <127>;
};
```

The interrupt-controller@c000000 node has L4 as the parent node, and this is external interrupt ID #11 into the CPU. This is the connection into the CPU from the PLIC, which routes global interrupts to individual CPUs.

> Note: The compatible string above refers directly to the file freedom-metal/src/drivers/riscv_plic0.c

中断控制器 @c000000 节点有L4作为父节点，这是进入CPU的外部中断ID #11。这是从PLIC到CPU的连接，它将全局中断路由到各个CPU。

> 注意: 上面兼容的字符串直接指向文件 freedom-metal/src/drivers/riscv_plic0.c

# Chapter 5 Privilege Levels

## 5.1 Priorities for Supervisor & Machine Mode Interrupts

The privilege levels allow protection to certain software components that only run, or have access from, higher privilege levels. At a minimum, any RISC-V implementation must implement Machine mode, as this is the highest privilege level.

特权级别允许保护仅运行或从更高特权级别访问的某些软件组件。至少，任何RISC-V实现都必须实现机器模式，因为这是最高的特权级别。

![](img/Table2-RiscvPrivilegeModes.png)

Only designs that include the RISC-V "N" extension allow interrupts in User mode. When Supervisor mode is implemented, additional interrupts are included, as shown below.

* Software Interrupt, Supervisor Mode, Interrupt ID: 1
* Software Interrupt, Machine Mode, Interrupt ID: 3
* Timer Interrupt, Supervisor Mode, Interrupt ID: 5
* Timer Interrupt, Machine Mode, Interrupt ID: 7
* External Interrupt, Supervisor Mode, Interrupt ID: 9
* External Interrupt, Machine Mode, Interrupt ID: 11

A CPU operating in Supervisor mode will trap to Machine mode upon the arrival of a Machine mode interrupt, unless the Machine mode interrupt has been delegated to Supervisor mode through the mideleg register. On the contrary, Supervisor interrupts will not immediately trigger if a CPU is in Machine mode. While operating in Supervisor mode, a CPU does not have visibility to configure Machine mode interrupts.

只有设计包括的RISC-V "N" 扩展允许中断在用户模式。当执行监督模式时，会包含额外的中断，如下所示。

* 软件中断，主管模式，中断ID: 1
* 软件中断，机器模式，中断ID: 3
* 计时器中断，管理模式，中断ID: 5
* 计时器中断，机器模式，中断ID: 7
* 外部中断，主管模式，中断ID: 9
* 外部中断，机器模式，中断ID: 11

除非机器模式中断已通过mideleg寄存器委托给管理模式，否则在管理模式下操作的CPU将在到达机器模式中断时trap到机器模式。相反，如果CPU处于机器模式，监控器中断不会立即触发。在管理模式下操作时，CPU不具有配置机器模式中断的可见性。

## 5.2 Context Switch Overhead

Each privilege level has its own interrupt configuration registers, or a subset of bits in the respective machine mode only registers. Additionally, there is save/restore overhead to support a context switch to a different privilege level. Context switches include the following:

* 31 User registers, x1 - x31 (x0 is hardwired to 0)
* Program Counter (pc)
* 32 Floating point registers
* Floating point Control and Status Register (fcsr)

Prior to initiating a context switch, these registers should be saved on the stack. Likewise, prior to returning from a different privilege level, they are required to be restored from the stack.

A Context switch is initiated through the ECALL instruction which results in a environment-callfrom-x-mode exception, where x is either M, S, or U, depending on the current privilege level.

Return from an ECALL instruction occurs by executing the respective xRET instruction, where again, x is M, S, or U. The xRET instruction can be used in privilege mode x or higher. For more information on context switches, refer to The RISC-V Instruction Set Manual Volume II: Privileged Architecture Privileged Architecture Version 1.10

每个特权级别都有其自己的中断配置寄存器，或位的子集在各自的机器模式仅寄存器。此外，还有保存/恢复开销来支持上下文切换到不同的特权级别。上下文切换包括以下内容:

* 31个用户寄存器，x1 - x31 (x0硬连接到0)
* 程序计数器(pc)
* 32个浮点寄存器
* 浮点控制和状态寄存器(fcsr)

在启动上下文切换之前，这些寄存器应该保存在堆栈上。同样，在从不同的特权级别返回之前，需要从堆栈中恢复它们。

通过ECALL指令启动上下文切换，这会导致 environment-call-from-x-mode 异常，其中x为M、S或U，具体取决于当前特权级别。

通过执行各自的xRET指令从ECALL指令返回，其中x是M、S或U。xRET指令可以在特权模式x或更高的情况下使用。有关上下文切换的更多信息，请参考RISC-V指令集手册卷II:特权体系结构特权体系结构版本1.10
