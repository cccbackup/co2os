# 自製 RISC-V 工具鏈

## 搭配 riscv toolchain

```
root@localhost:~/co/riscv/mini_riscv/01/asm# riscv64-unknown-elf-gcc -march=rv32ima -mabi=ilp32 -Wl,-Ttext=0x0 
-nostdlib -o sum1 sum1.s
/usr/local/lib/gcc/riscv64-unknown-elf/9.2.0/../../../../riscv64-unknown-elf/bin/ld: warning: cannot find entry symbol _start; defaulting to 0000000000000000
root@localhost:~/co/riscv/mini_riscv/01/asm# riscv64-unknown-elf-objdump -d sum1

sum1:     file format elf32-littleriscv


Disassembly of section .text:

00000000 <main>:
   0:   00000093                li      ra,0
   4:   00100113                li      sp,1
   8:   00b00193                li      gp,11

0000000c <.loop>:
   c:   002080b3                add     ra,ra,sp
  10:   00110113                addi    sp,sp,1
  14:   fe314ce3                blt     sp,gp,c <.loop>

root@localhost:~/co/riscv/mini_riscv/01/asm# riscv64-unknown-elf-objcopy -O binary sum1 sum1.bin
root@localhost:~/co/riscv/mini_riscv/01/asm# cd ..
root@localhost:~/co/riscv/mini_riscv/01# ./dasm asm/sum1.bin
asm                  addr:code     T op f3 f7 rd rs1 rs2
---------------------------------------------------------
0000:addi x1,x0,0         00000093 I 13  0 00 01  00  00
0004:addi x2,x0,1         00100113 I 13  0 00 02  00  01
0008:addi x3,x0,11        00B00193 I 13  0 00 03  00  0B
000C:add x1,x1,x2         002080B3 R 33  0 00 01  01  02
0010:addi x2,x2,1         00110113 I 13  0 00 02  02  01
0014:blt x2,x3,-8         FE314CE3 B 63  4 7F 19  02  03
```
