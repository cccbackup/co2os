#include "riscv.hpp"

#define MMAX 32768
uint8_t m[MMAX];

void disasm(uint8_t *m, uint32_t mTop) {
  uint32_t I = 0, f3, f7, op, rd, rs1, rs2;
  int32_t imm;
  char type;
  printf("asm        \taddr:code     T op f3 f7 rd rs1 rs2\n-----------------------------------------------------\n");
  for (int PC = 0; PC<mTop; PC+=4) {
    I = *(uint32_t*)&m[PC];
    op=BITS(I, 0, 6); 
    f3=BITS(I, 12, 14);
    f7=BITS(I, 25, 31);
    rd=BITS(I, 7, 11);
    rs1=BITS(I, 15, 19);
    rs2=BITS(I, 20, 24);
    imm=0;
    type='?';
    switch (op) {
      case 0x33:
        type = 'R';
        switch (f3) {
          case 0: printf("add x%d,x%d,x%d\t", rd, rs1, rs2); break;
          default: break;
        }
        break;
      case 0x13:
        type = 'I';
        imm = BITS(I, 20, 31);
        switch (f3) {
          case 0: printf("addi x%d,x%d,%d\t", rd, rs1, imm); break;
          default: break;
        }
        break;
      case 0x23:
        type = 'S';
        switch (f3) {
          case 2: printf("sw x%d,%d(x%d)\t", rs1, imm, rs2); break;
          default: break;
        }
        break;
      case 0x03:
        type = 'I';
        switch (f3) {
          case 2: printf("lw x%d,%d(x%d)\t", rd, imm, rs1); break;
          default: break;
        }
        break;
      case 0x63:
        type = 'B';
        switch (f3) {
          case 0: printf("beq x%d,x%d,%d\t", rs1, rs2, imm); break;
          default: break;
        }
        break;
    }
    printf("%04X:%08X %c %02X  %01X %02X %02X  %02X  %02X\n", PC, I, type, op, f3, f7, rd, rs1, rs2);
  }
}

// run: ./disasm <file.bin>
int main(int argc, char *argv[]) {
  char *binFileName = argv[1];
  FILE *binFile = fopen(binFileName, "rb");
  uint32_t mTop = fread(m, sizeof(uint8_t), MMAX, binFile);
  fclose(binFile);
  disasm(m, mTop);
  return 0;
}
