#include "riscv.hpp"

Op oNull = { "", '?', 0, 0, 0 };
Op oSll = {"sll", 'R',0x33,0x1,0x0};
Op oSlli = {"slli", 'I',0x13,0x1,0x0};
Op oSrl = {"srl", 'R',0x33,0x5,0x0};
Op oSrli = {"srli", 'I',0x13,0x5,0x0};
Op oSra = {"sra", 'R',0x33,0x5,0x20};
Op oSrai = {"srai", 'I',0x13,0x5,0x20};
Op oAdd = {"add", 'R',0x33,0x0,0x0};
Op oAddi = {"addi", 'I',0x13,0x0,0x0};
Op oSub = {"sub", 'R',0x33,0x0,0x20};
Op oLui = {"lui", 'U',0x37};
Op oAuipc = {"auipc", 'U',0x17};
Op oXor = {"xor", 'R',0x33,0x4};
Op oXori = {"xori", 'I',0x13,0x4};
Op oOr = {"or", 'R',0x33,0x6};
Op oOri = {"ori", 'I',0x13,0x6};
Op oAnd = {"and", 'R',0x33,0x7};
Op oAndi = {"andi", 'I',0x13,0x7};
Op oSlt = {"slt", 'R',0x33,0x2};
Op oSlti = {"slti", 'I',0x13,0x2};
Op oSltu = {"sltu", 'R',0x33,0x3};
Op oSltiu = {"sltiu", 'I',0x13,0x3};
Op oBeq = {"beq", 'B',0x63,0x0};
Op oBne = {"bne", 'B',0x63,0x1};
Op oBlt = {"blt", 'B',0x63,0x4};
Op oBge = {"bge", 'B',0x63,0x5};
Op oBltu = {"bltu", 'B',0x63,0x6};
Op oBgeu = {"bgeu", 'B',0x63,0x7};
Op oJal = {"jal", 'J',0x6F};
Op oJalr = {"jalr", 'I',0x67};
Op oFence = {"fence", 'I',0x0F,0x0};
Op oFenceI = {"fence.i", 'I',0x0F,0x1};
Op oEcall = {"ecall", 'I',0x73};
Op oEbreak = {"ebreak", 'I',0x73};
Op oCsrrw = {"csrrw", 'I',0x73,0x1};
Op oCsrrs = {"csrrs", 'I',0x73,0x2};
Op oCsrrc = {"csrrc", 'I',0x73,0x3};
Op oCsrrwi = {"csrrwi", 'I',0x73,0x5};
Op oCsrrsi = {"csrrsi", 'I',0x73,0x6};
Op oCsrrci = {"csrrci", 'I',0x73,0x7};
Op oLb = {"lb", 'I',0x03,0x0};
Op oLh = {"lh", 'I',0x03,0x1};
Op oLw = {"lw", 'I',0x03,0x2};
Op oLbu = {"lbu", 'I',0x03,0x4};
Op oLhu = {"lhu", 'I',0x03,0x5};
Op oSb = {"sb", 'S',0x23,0x0};
Op oSh = {"sh", 'S',0x23,0x1};
Op oSw = {"sw", 'S',0x23,0x2};

map<string, Op> opMap {
  {"null", oNull}, 
  {"sll", oSll}, 
  {"slli",oSlli}, 
  {"srl", oSrl}, 
  {"srli",oSrli}, 
  {"sra",oSra}, 
  {"srai",oSrai}, 
  {"add", oAdd}, 
  {"addi",oAddi}, 
  {"sub", oSub}, 
  {"lui", oLui}, 
  {"auipc", oAuipc}, 
  {"xor", oXor}, 
  {"xori", oXori}, 
  {"or",  oOr}, 
  {"ori", oOri},
  {"and", oAnd},
  {"andi", oAndi},
  {"slt", oSlt},
  {"slti", oSlti},
  {"sltu", oSltu},
  {"sltiu", oSltiu},
  {"beq", oBeq},
  {"bne", oBne},
  {"blt", oBlt},
  {"bge", oBge},
  {"bltu", oBltu},
  {"bgeu", oBgeu},
  {"jal",  oJal},
  {"jalr", oJalr},
  {"fence", oFence},
  {"fence.i", oFenceI},
  {"ecall", oEcall},
  {"ebreak", oEbreak},
  {"csrrw", oCsrrw},
  {"csrrs", oCsrrs},
  {"csrrc", oCsrrc},
  {"csrrwi", oCsrrwi},
  {"csrrsi", oCsrrsi},
  {"csrrci", oCsrrci},
  {"lb", oLb},
  {"lh", oLh},
  {"lw", oLw},
  {"lbu", oLbu},
  {"lhu", oLhu},
  {"sb", oSb},
  {"sh", oSh},
  {"sw", oSw},
};

map<string, int> rMap {
  // 暫存器 x0-x31
  {"x0",0},{"x1",1},{"x2",2},{"x3",3},{"x4",4},{"x5",5},{"x6",6},{"x7",7},
  {"x8",8}, {"x9",9}, {"x10",10}, {"x11",11},{"x12",12}, {"x13",13}, {"x14",14}, {"x15",15},
  {"x16",16}, {"x17",17}, {"x18",18}, {"x19",19},{"x20",20}, {"x21",21}, {"x22",22}, {"x23",23},
  {"x24",24}, {"x25",25}, {"x26",26}, {"x27",27},{"x28",28}, {"x29",29}, {"x30",30}, {"x31",31},
  // 暫存器別名
  {"zero",0},{"ra",1},{"sp",2},{"gp",3},{"tp",4},{"t0",5},{"t1",6},{"t2",7},
  {"s0",8}, {"fp",8}, {"s1",9}, {"a0",10}, {"a1",11},{"a2",12}, {"a3",13}, {"a4",14}, {"a5",15},
  {"a6",16}, {"a7",17}, {"s2",18}, {"s3",19},{"s4",20}, {"s5",21}, {"s6",22}, {"s7",23},
  {"s8",24}, {"s9",25}, {"s10",26}, {"s11",27},{"t3",28}, {"t4",29}, {"t5",30}, {"t6",31}
};
