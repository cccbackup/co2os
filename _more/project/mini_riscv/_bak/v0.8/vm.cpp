#include "riscv.hpp"

#define MMAX 32768
uint8_t m[MMAX];

void vm(uint8_t *m, uint32_t mTop) {
  pc = 0;
  while (1) {
    ir = bytesToWord(&m[pc]);
    Op *o = decode(ir);
    dumpInstr(ir);
    int isJump = 0;
    if (o==&oSll) x[rd]=(uint32_t)x[rs1]<<x[rs2];
    else if (o==&oSrl) x[rd]=(uint32_t)x[rs1]>>x[rs2];
    else if (o==&oSra) x[rd]=x[rs1]>>x[rs2];
    else if (o==&oAdd) x[rd]=x[rs1]+x[rs2];
    else if (o==&oSub) x[rd]=x[rs1]-x[rs2];
    else if (o==&oXor) x[rd]=x[rs1]^x[rs2];
    else if (o==&oAnd) x[rd]=x[rs1]&x[rs2];
    else if (o==&oSlli) x[rd]=(uint32_t)x[rs1]<<imm;
    else if (o==&oSrli) x[rd]=(uint32_t)x[rs1]>>imm;
    else if (o==&oSrai) x[rd]=x[rs1]>>imm;
    else if (o==&oAddi) x[rd]=x[rs1]+imm;
    else if (o==&oXori) x[rd]=x[rs1]^imm;
    else if (o==&oAndi) x[rd]=x[rs1]&imm;
    else if (o==&oSlt) x[rd] = (x[rs1] < x[rs2]);
    else if (o==&oSlti) x[rd] = (x[rs1] < imm);
    else if (o==&oSltu) x[rd] = ((uint32_t)x[rs1] < (uint32_t)x[rs2]);
    else if (o==&oSltiu) x[rd] = ((uint32_t)x[rs1] < (uint32_t)imm);
    else if (o==&oLb) x[rd]=*(int8_t*)&m[x[rs1]+imm];
    else if (o==&oLbu) x[rd]=*(uint8_t*)&m[x[rs1]+imm];
    else if (o==&oLh) x[rd]=*(int16_t*)&m[x[rs1]+imm];
    else if (o==&oLhu) x[rd]=*(uint16_t*)&m[x[rs1]+imm];
    else if (o==&oLw) x[rd]=*(int32_t*)&m[x[rs1]+imm];
    else if (o==&oSb) *(int8_t*)&m[x[rs1]+imm]=x[rd];
    else if (o==&oSh) *(int16_t*)&m[x[rs1]+imm]=x[rd];
    else if (o==&oSw) *(int32_t*)&m[x[rs1]+imm]=x[rd];
    else if (o==&oLui) x[rd]=imm<<12;
    else if (o==&oAuipc) x[rd]=pc+(imm<<12);
    else {
      isJump = 1;
      if (o==&oJal) { x[rd]=pc; pc+= imm; }
      else if (o==&oJalr) { int32_t t=pc+4; pc=x[rs1]+imm; x[rd] = t; } // 保存返回位址然後跳到 x[rs1]+imm
      else if (o==&oBeq) { if (x[rs1] == x[rs2]) pc+=imm; else pc+=4; } // 疑問：跳躍位址應該是 pc加上 sign-extended 13-bit，而非直接設定。
      else if (o==&oBlt) { if (x[rs1] <  x[rs2]) pc+=imm; else pc+=4; }
      else if (o==&oBne) { if (x[rs1] != x[rs2]) pc+=imm; else pc+=4; }
      else if (o==&oBge) { if (x[rs1] >= x[rs2]) pc+=imm; else pc+=4; }
      else if (o==&oBltu) { if ((uint32_t)x[rs1] <  (uint32_t)x[rs2]) pc += imm; else pc+=4; }
      else if (o==&oBgeu) { if ((uint32_t)x[rs1] >= (uint32_t)x[rs2]) pc += imm; else pc+=4; }
      else { printf("ir=%08x : command not found!\n"); exit(1); }
    }
    printf("  x[%d]=%d\n", rd, x[rd]);
    if (!isJump) pc += 4;
    if (pc >= mTop) break;
  }
}

/*
Op oFence = {"fence", 'I',0x0F,0x0};
Op oFenceI = {"fence.i", 'I',0x0F,0x1};
Op oEcall = {"ecall", 'I',0x73};
Op oEbreak = {"ebreak", 'I',0x73};
Op oCsrrw = {"csrrw", 'I',0x73,0x1};
Op oCsrrs = {"csrrs", 'I',0x73,0x2};
Op oCsrrc = {"csrrc", 'I',0x73,0x3};
Op oCsrrwi = {"csrrwi", 'I',0x73,0x5}; // 格式 I 是把 rs1 改為 uimm
Op oCsrrsi = {"csrrsi", 'I',0x73,0x6};
Op oCsrrci = {"csrrci", 'I',0x73,0x7};
*/
// run: ./disasm <file.bin>
int main(int argc, char *argv[]) {
  char *binFileName = argv[1];
  FILE *binFile = fopen(binFileName, "rb");
  mTop = fread(m, sizeof(uint8_t), MMAX, binFile);
  fclose(binFile);
  vm(m, mTop);
  return 0;
}
