#ifndef __LIB_H__
#define __LIB_H__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stddef.h>
#include <assert.h>
#include <iostream> 
#include <map>

using namespace std;

#define SMAX 100
#define TMAX 10000

#define BITS(x, from, to) ((uint32_t)(x)<<(32-(to+1)))>>(32-(to-from+1)) // 包含 to, 用 BITS(x, 1, 30) 去思考
#define SGN(x, i) (BITS(x,i,i)==0)? x : (-(1<<i)+(int32_t)BITS(x, 0, i-1))+1 // x[i] 為 sign bit，轉為整數。

int mapFind(map<string, int> *iMap, char *name);
void mapDump(map<string, int> *iMap);

#endif