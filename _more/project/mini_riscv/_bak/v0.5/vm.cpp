#include "riscv.hpp"

#define MMAX 32768
uint8_t m[MMAX];

void dumpInstr(uint32_t I) {
  char sasm[SMAX];
  imm = 0;
  uint32_t t;
  switch (type) {
    case 'R': sprintf(sasm, "%s x%d,x%d,x%d", name, rd, rs1, rs2); break;
    case 'I': t=BITS(I, 20, 31); imm = SGN(t, 11); sprintf(sasm, "%s x%d,x%d,%d", name, rd, rs1, imm); break;
    case 'S': t=BITS(I, 25, 31)<<5|BITS(I, 7, 11); imm=SGN(t,11); sprintf(sasm, "%s x%d,%d(x%d)", name, rs1, imm, rs2); break;
    case 'B': t=BITS(I, 31, 31)<<12|BITS(I,7,7)<<11|BITS(I,25,30)<<5|BITS(I,8,11)<<1; imm=SGN(t, 12); sprintf(sasm, "%s x%d,x%d,%d", name, rs1, rs2, imm); break;
    case 'U': imm=(int32_t)(BITS(I, 12, 31)<<12); sprintf(sasm, "%s x%d,%d", name, rd, imm); break;
    case 'J': t=BITS(I, 31, 31)<<20|BITS(I,21,30)<<1|BITS(I,20,20)<<11|BITS(I,12,19)<<12; imm=SGN(t,20); sprintf(sasm, "%s %d(x%d)", name, imm, rd); break;
    default: sprintf(sasm, "Instruction error !");
  }
  printf("%-20s %04X:%08X %c %02X  %01X %02X %02X  %02X  %02X\n", sasm, PC, I, type, op, f3, f7, rd, rs1, rs2);
}

void vm(uint8_t *m, uint32_t mTop) {
  for (PC = 0; PC<mTop; ) {
    I = *(uint32_t*)&m[PC];
    Op *o = decode(I);
    dumpInstr(I);
    PC += 4;
    if (o==&oAdd) R[rd]=R[rs1]+R[rs2];
    else if (o==&oAddi) R[rd]=R[rs1]+imm;
    else if (o==&oBlt) {
      if (R[rs1] < R[rs2]) PC = imm;
    } else {
      printf("command not found!\n");
    }
    printf("R[%d]=%d\n", rd, R[rd]);
  }
}

// run: ./disasm <file.bin>
int main(int argc, char *argv[]) {
  char *binFileName = argv[1];
  FILE *binFile = fopen(binFileName, "rb");
  mTop = fread(m, sizeof(uint8_t), MMAX, binFile);
  fclose(binFile);
  vm(m, mTop);
  return 0;
}
