#ifndef __RISCV_H__
#define __RISCV_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stddef.h>
#include <assert.h>
#include <iostream> 
#include <map>

using namespace std;

#define SMAX 100
#define TMAX 10000

#define BITS(x, from, to) (((x)<<(32-(to+1)))>>(32-((to)-(from)+1))) // 包含 to, 用 BITS(x, 1, 30) 去思考

typedef struct _Op {
  char type;
  uint32_t op, f3, f7;
} Op;

#define ISLL  Op{'R',0x33,0x1,0x0}
#define ISLLI Op{'I',0x13,0x1,0x0}
#define ISRL  Op{'R',0x33,0x5,0x0}
#define ISRLI Op{'I',0x13,0x5,0x0}
#define ISRA  Op{'R',0x33,0x5,0x20}
#define ISRAI Op{'I',0x13,0x5,0x20}
#define IADD  Op{'R',0x33,0x0,0x0}
#define IADDI Op{'I',0x13,0x0,0x0}
#define ISUB  Op{'R',0x33,0x0,0x20}
#define ILUI  Op{'U',0x37}
#define IAUIPC Op{'U',0x17}
#define IXOR  Op{'R',0x33,0x4}
#define IXORI Op{'I',0x13,0x4}
#define IOR   Op{'R',0x33,0x6}
#define IORI  Op{'I',0x13,0x6}
#define IAND  Op{'R',0x33,0x7}
#define IANDI Op{'I',0x13,0x7}
#define ISLT  Op{'R',0x33,0x2}
#define ISLTI Op{'I',0x13,0x2}
#define ISLTU Op{'R',0x33,0x3}
#define ISLTIU Op{'I',0x13,0x3}
#define IBEQ  Op{'B',0x63,0x0}
#define IBNE  Op{'B',0x63,0x1}
#define IBLT  Op{'B',0x63,0x4}
#define IBGE  Op{'B',0x63,0x5}
#define IBLTU Op{'B',0x63,0x6}
#define IBGEU Op{'B',0x63,0x7}
#define IJAL  Op{'J',0x6F}
#define IJALR Op{'I',0x67}
#define IFENCE Op{'I',0x0F,0x0}
#define IFENCE_I Op{'I',0x0F,0x1}
#define IECALL Op{'I',0x73}
#define IEBREAK Op{'I',0x73}
#define ICSRRW Op{'I',0x73,0x1}
#define ICSRRS Op{'I',0x73,0x2}
#define ICSRRC Op{'I',0x73,0x3}
#define ICSRRWI Op{'I',0x73,0x5}
#define ICSRRSI Op{'I',0x73,0x6}
#define ICSRRCI Op{'I',0x73,0x7}
#define ILB   Op{'I',0x03,0x0}
#define ILH   Op{'I',0x03,0x1}
#define ILW   Op{'I',0x03,0x2}
#define ILBU  Op{'I',0x03,0x4}
#define ILHU  Op{'I',0x03,0x5}
#define ISB   Op{'S',0x23,0x0}
#define ISH   Op{'S',0x23,0x1}
#define ISW   Op{'S',0x23,0x2}

extern Op opNull;
extern map<string, Op> opMap;
extern map<string, int> rMap;

#endif


/* 指令型態
  | R, I, S, B, U, J
--|--------------------------------------------
R | f7 rs2 rs1 f3 rd op
I |   i12  rs1 f3 rd op
S | i7 rs2 rs1 f3 i5 op  i7=i[11:5] i5=i[4:0]
B | i7 rs2 rs1 f3 i5 op  i7=i[12|10:5] i5=i[4:1|11]
U |   i20         rd op  i20=i[31:12]
J |   i20         rd op  i20=i[20|10:1|11|19:12]
*/

/* 指令表
U: lui rd,imm`: `rd = imm * 2^12; pc = pc + 4` with `-2^19 <= imm < 2^19`
I: addi rd,rs1,imm`: `rd = rs1 + imm; pc = pc + 4` with `-2^11 <= imm < 2^11`
I: lw?? ld rd,imm(rs1)`: `rd = memory[rs1 + imm]; pc = pc + 4` with `-2^11 <= imm < 2^11`
I: sw?? sd rs2,imm(rs1)`: `memory[rs1 + imm] = rs2; pc = pc + 4` with `-2^11 <= imm < 2^11`
R: add rd,rs1,rs2`: `rd = rs1 + rs2; pc = pc + 4`   op:0110011 f7:0000000 
R: sub rd,rs1,rs2`: `rd = rs1 - rs2; pc = pc + 4`   op:0110011 f7:0100000
R: mul rd,rs1,rs2`: `rd = rs1 * rs2; pc = pc + 4`
R: divu rd,rs1,rs2`: `rd = rs1 / rs2; pc = pc + 4` where `rs1` and `rs2` are unsigned integers.
R: remu rd,rs1,rs2`: `rd = rs1 % rs2; pc = pc + 4` where `rs1` and `rs2` are unsigned integers.
R: sltu rd,rs1,rs2`: `if (rs1 < rs2) { rd = 1 } else { rd = 0 } pc = pc + 4` where `rs1` and `rs2` are unsigned integers.
B: beq rs1,rs2,imm`: `if (rs1 == rs2) { pc = pc + imm } else { pc = pc + 4 }` with `-2^12 <= imm < 2^12` and `imm % 2 == 0`
J: jal rd,imm`: `rd = pc + 4; pc = pc + imm` with `-2^20 <= imm < 2^20` and `imm % 2 == 0`
I: jalr rd,imm(rs1)`: `tmp = ((rs1 + imm) / 2) * 2; rd = pc + 4; pc = tmp` with `-2^11 <= imm < 2^11`
I: ecall`: system call number is in `a7`, parameters are in `a0-a2`, return value is in `a0`.
*/
